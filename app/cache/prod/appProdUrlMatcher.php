<?php

use Symfony\Component\Routing\Exception\MethodNotAllowedException;
use Symfony\Component\Routing\Exception\ResourceNotFoundException;
use Symfony\Component\Routing\RequestContext;

/**
 * appProdUrlMatcher
 *
 * This class has been auto-generated
 * by the Symfony Routing Component.
 */
class appProdUrlMatcher extends Symfony\Bundle\FrameworkBundle\Routing\RedirectableUrlMatcher
{
    /**
     * Constructor.
     */
    public function __construct(RequestContext $context)
    {
        $this->context = $context;
    }

    public function match($pathinfo)
    {
        $allow = array();
        $pathinfo = rawurldecode($pathinfo);
        $context = $this->context;
        $request = $this->request;

        // cicrtg_backend_homepage
        if (0 === strpos($pathinfo, '/hello') && preg_match('#^/hello/(?P<name>[^/]++)$#s', $pathinfo, $matches)) {
            return $this->mergeDefaults(array_replace($matches, array('_route' => 'cicrtg_backend_homepage')), array (  '_controller' => 'cicrtg\\BackendBundle\\Controller\\DefaultController::indexAction',));
        }

        // cicrtg_backend_navigation
        if (0 === strpos($pathinfo, '/nav') && preg_match('#^/nav/(?P<name>[^/]++)$#s', $pathinfo, $matches)) {
            return $this->mergeDefaults(array_replace($matches, array('_route' => 'cicrtg_backend_navigation')), array (  '_controller' => 'cicrtg\\BackendBundle\\Controller\\DefaultController::navAction',));
        }

        // cicrtg_backend_home
        if ($pathinfo === '/admin/home') {
            return array (  '_controller' => 'cicrtg\\BackendBundle\\Controller\\BackendController::indexAction',  '_route' => 'cicrtg_backend_home',);
        }

        // cicrtg_backend_donnee
        if (0 === strpos($pathinfo, '/donnees') && preg_match('#^/donnees/(?P<id>\\d+)$#s', $pathinfo, $matches)) {
            return $this->mergeDefaults(array_replace($matches, array('_route' => 'cicrtg_backend_donnee')), array (  '_controller' => 'cicrtg\\BackendBundle\\Controller\\BackendController::donneesAction',));
        }

        if (0 === strpos($pathinfo, '/admin')) {
            if (0 === strpos($pathinfo, '/admin/agents')) {
                // cicrtg_backend_agent
                if ($pathinfo === '/admin/agents') {
                    return array (  '_controller' => 'cicrtg\\BackendBundle\\Controller\\BackendController::agentsAction',  '_route' => 'cicrtg_backend_agent',);
                }

                // cicrtg_backend_agent_modif
                if (0 === strpos($pathinfo, '/admin/agents/modif') && preg_match('#^/admin/agents/modif/(?P<id>\\d+)$#s', $pathinfo, $matches)) {
                    return $this->mergeDefaults(array_replace($matches, array('_route' => 'cicrtg_backend_agent_modif')), array (  '_controller' => 'cicrtg\\BackendBundle\\Controller\\BackendController::agentmodifAction',));
                }

                // cicrtg_backend_agent_supp
                if (0 === strpos($pathinfo, '/admin/agents/supp') && preg_match('#^/admin/agents/supp/(?P<id>\\d+)$#s', $pathinfo, $matches)) {
                    return $this->mergeDefaults(array_replace($matches, array('_route' => 'cicrtg_backend_agent_supp')), array (  '_controller' => 'cicrtg\\BackendBundle\\Controller\\BackendController::suppagentAction',));
                }

            }

            // cicrtg_backend_user
            if ($pathinfo === '/admin/users') {
                return array (  '_controller' => 'cicrtg\\BackendBundle\\Controller\\BackendController::usersAction',  '_route' => 'cicrtg_backend_user',);
            }

            if (0 === strpos($pathinfo, '/admin/zone')) {
                // cicrtg_backend_zone
                if ($pathinfo === '/admin/zone') {
                    return array (  '_controller' => 'cicrtg\\BackendBundle\\Controller\\BackendController::zoneAction',  '_route' => 'cicrtg_backend_zone',);
                }

                // cicrtg_backend_prefecture_modif
                if (0 === strpos($pathinfo, '/admin/zone/prefecture') && preg_match('#^/admin/zone/prefecture/(?P<id>\\d+)$#s', $pathinfo, $matches)) {
                    return $this->mergeDefaults(array_replace($matches, array('_route' => 'cicrtg_backend_prefecture_modif')), array (  '_controller' => 'cicrtg\\BackendBundle\\Controller\\BackendController::modifprefectureAction',));
                }

                // cicrtg_backend_localite_modif
                if (0 === strpos($pathinfo, '/admin/zone/localite') && preg_match('#^/admin/zone/localite/(?P<id>\\d+)$#s', $pathinfo, $matches)) {
                    return $this->mergeDefaults(array_replace($matches, array('_route' => 'cicrtg_backend_localite_modif')), array (  '_controller' => 'cicrtg\\BackendBundle\\Controller\\BackendController::modiflocaliteAction',));
                }

                // cicrtg_backend_prefecture_supp
                if (0 === strpos($pathinfo, '/admin/zone/prefecture/supp') && preg_match('#^/admin/zone/prefecture/supp/(?P<id>\\d+)$#s', $pathinfo, $matches)) {
                    return $this->mergeDefaults(array_replace($matches, array('_route' => 'cicrtg_backend_prefecture_supp')), array (  '_controller' => 'cicrtg\\BackendBundle\\Controller\\BackendController::supprefectureAction',));
                }

                // cicrtg_backend_localite_supp
                if (0 === strpos($pathinfo, '/admin/zone/localite/supp') && preg_match('#^/admin/zone/localite/supp/(?P<id>\\d+)$#s', $pathinfo, $matches)) {
                    return $this->mergeDefaults(array_replace($matches, array('_route' => 'cicrtg_backend_localite_supp')), array (  '_controller' => 'cicrtg\\BackendBundle\\Controller\\BackendController::suplocaliteAction',));
                }

            }

            // cicrtg_backend_localite
            if ($pathinfo === '/admin/localite') {
                return array (  '_controller' => 'cicrtg\\BackendBundle\\Controller\\BackendController::localiteAction',  '_route' => 'cicrtg_backend_localite',);
            }

            // cicrtg_backend_type
            if ($pathinfo === '/admin/typeDonnees') {
                return array (  '_controller' => 'cicrtg\\BackendBundle\\Controller\\BackendController::typeDonneesAction',  '_route' => 'cicrtg_backend_type',);
            }

            if (0 === strpos($pathinfo, '/admin/pheno')) {
                // cicrtg_backend_pheno_post
                if ($pathinfo === '/admin/pheno/traitement') {
                    return array (  '_controller' => 'cicrtg\\BackendBundle\\Controller\\BackendController::phenoPostAction',  '_route' => 'cicrtg_backend_pheno_post',);
                }

                // cicrtg_backend_pheno_delete
                if (0 === strpos($pathinfo, '/admin/pheno/delete') && preg_match('#^/admin/pheno/delete/(?P<id>\\d+)$#s', $pathinfo, $matches)) {
                    return $this->mergeDefaults(array_replace($matches, array('_route' => 'cicrtg_backend_pheno_delete')), array (  '_controller' => 'cicrtg\\BackendBundle\\Controller\\BackendController::phenoDelAction',));
                }

            }

            if (0 === strpos($pathinfo, '/admin/type')) {
                // cicrtg_backend_type_post
                if ($pathinfo === '/admin/type/traitement') {
                    return array (  '_controller' => 'cicrtg\\BackendBundle\\Controller\\BackendController::typePostAction',  '_route' => 'cicrtg_backend_type_post',);
                }

                // cicrtg_backend_type_delete
                if (0 === strpos($pathinfo, '/admin/type/delete') && preg_match('#^/admin/type/delete/(?P<id>\\d+)$#s', $pathinfo, $matches)) {
                    return $this->mergeDefaults(array_replace($matches, array('_route' => 'cicrtg_backend_type_delete')), array (  '_controller' => 'cicrtg\\BackendBundle\\Controller\\BackendController::typeDelAction',));
                }

            }

        }

        // cicrtg_frontend_homepage
        if (0 === strpos($pathinfo, '/hello') && preg_match('#^/hello/(?P<name>[^/]++)$#s', $pathinfo, $matches)) {
            return $this->mergeDefaults(array_replace($matches, array('_route' => 'cicrtg_frontend_homepage')), array (  '_controller' => 'cicrtg\\FrontendBundle\\Controller\\DefaultController::indexAction',));
        }

        // frontend_homepage
        if (rtrim($pathinfo, '/') === '') {
            if (substr($pathinfo, -1) !== '/') {
                return $this->redirect($pathinfo.'/', 'frontend_homepage');
            }

            return array (  '_controller' => 'cicrtg\\FrontendBundle\\Controller\\FrontendController::indexAction',  '_route' => 'frontend_homepage',);
        }

        if (0 === strpos($pathinfo, '/donnees')) {
            // frontend_donnee
            if ($pathinfo === '/donnees') {
                return array (  '_controller' => 'cicrtg\\FrontendBundle\\Controller\\FrontendController::donneesAction',  '_route' => 'frontend_donnee',);
            }

            // frontend_donnee_zone
            if (0 === strpos($pathinfo, '/donnees/zone') && preg_match('#^/donnees/zone/(?P<id>\\d*)$#s', $pathinfo, $matches)) {
                return $this->mergeDefaults(array_replace($matches, array('_route' => 'frontend_donnee_zone')), array (  '_controller' => 'cicrtg\\FrontendBundle\\Controller\\FrontendController::donneeszoneAction',));
            }

        }

        // frontend_mode
        if ($pathinfo === '/mode_demploi') {
            return array (  '_controller' => 'cicrtg\\FrontendBundle\\Controller\\FrontendController::modeAction',  '_route' => 'frontend_mode',);
        }

        // frontend_plan
        if ($pathinfo === '/plan_site') {
            return array (  '_controller' => 'cicrtg\\FrontendBundle\\Controller\\FrontendController::planAction',  '_route' => 'frontend_plan',);
        }

        // frontend_contact
        if ($pathinfo === '/contact') {
            return array (  '_controller' => 'cicrtg\\FrontendBundle\\Controller\\FrontendController::contactAction',  '_route' => 'frontend_contact',);
        }

        if (0 === strpos($pathinfo, '/log')) {
            if (0 === strpos($pathinfo, '/login')) {
                // login
                if ($pathinfo === '/login') {
                    return array (  '_controller' => 'cicrtg\\UserBundle\\Controller\\SecurityController::loginAction',  '_route' => 'login',);
                }

                // login_check
                if ($pathinfo === '/login_check') {
                    return array('_route' => 'login_check');
                }

            }

            // logout
            if ($pathinfo === '/logout') {
                return array('_route' => 'logout');
            }

        }

        throw 0 < count($allow) ? new MethodNotAllowedException(array_unique($allow)) : new ResourceNotFoundException();
    }
}

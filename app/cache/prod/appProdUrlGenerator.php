<?php

use Symfony\Component\Routing\RequestContext;
use Symfony\Component\Routing\Exception\RouteNotFoundException;
use Psr\Log\LoggerInterface;

/**
 * appProdUrlGenerator
 *
 * This class has been auto-generated
 * by the Symfony Routing Component.
 */
class appProdUrlGenerator extends Symfony\Component\Routing\Generator\UrlGenerator
{
    private static $declaredRoutes = array(
        'cicrtg_backend_homepage' => array (  0 =>   array (    0 => 'name',  ),  1 =>   array (    '_controller' => 'cicrtg\\BackendBundle\\Controller\\DefaultController::indexAction',  ),  2 =>   array (  ),  3 =>   array (    0 =>     array (      0 => 'variable',      1 => '/',      2 => '[^/]++',      3 => 'name',    ),    1 =>     array (      0 => 'text',      1 => '/hello',    ),  ),  4 =>   array (  ),  5 =>   array (  ),),
        'cicrtg_backend_navigation' => array (  0 =>   array (    0 => 'name',  ),  1 =>   array (    '_controller' => 'cicrtg\\BackendBundle\\Controller\\DefaultController::navAction',  ),  2 =>   array (  ),  3 =>   array (    0 =>     array (      0 => 'variable',      1 => '/',      2 => '[^/]++',      3 => 'name',    ),    1 =>     array (      0 => 'text',      1 => '/nav',    ),  ),  4 =>   array (  ),  5 =>   array (  ),),
        'cicrtg_backend_home' => array (  0 =>   array (  ),  1 =>   array (    '_controller' => 'cicrtg\\BackendBundle\\Controller\\BackendController::indexAction',  ),  2 =>   array (  ),  3 =>   array (    0 =>     array (      0 => 'text',      1 => '/admin/home',    ),  ),  4 =>   array (  ),  5 =>   array (  ),),
        'cicrtg_backend_donnee' => array (  0 =>   array (    0 => 'id',  ),  1 =>   array (    '_controller' => 'cicrtg\\BackendBundle\\Controller\\BackendController::donneesAction',  ),  2 =>   array (    'id' => '\\d+',  ),  3 =>   array (    0 =>     array (      0 => 'variable',      1 => '/',      2 => '\\d+',      3 => 'id',    ),    1 =>     array (      0 => 'text',      1 => '/donnees',    ),  ),  4 =>   array (  ),  5 =>   array (  ),),
        'cicrtg_backend_agent' => array (  0 =>   array (  ),  1 =>   array (    '_controller' => 'cicrtg\\BackendBundle\\Controller\\BackendController::agentsAction',  ),  2 =>   array (  ),  3 =>   array (    0 =>     array (      0 => 'text',      1 => '/admin/agents',    ),  ),  4 =>   array (  ),  5 =>   array (  ),),
        'cicrtg_backend_agent_modif' => array (  0 =>   array (    0 => 'id',  ),  1 =>   array (    '_controller' => 'cicrtg\\BackendBundle\\Controller\\BackendController::agentmodifAction',  ),  2 =>   array (    'id' => '\\d+',  ),  3 =>   array (    0 =>     array (      0 => 'variable',      1 => '/',      2 => '\\d+',      3 => 'id',    ),    1 =>     array (      0 => 'text',      1 => '/admin/agents/modif',    ),  ),  4 =>   array (  ),  5 =>   array (  ),),
        'cicrtg_backend_agent_supp' => array (  0 =>   array (    0 => 'id',  ),  1 =>   array (    '_controller' => 'cicrtg\\BackendBundle\\Controller\\BackendController::suppagentAction',  ),  2 =>   array (    'id' => '\\d+',  ),  3 =>   array (    0 =>     array (      0 => 'variable',      1 => '/',      2 => '\\d+',      3 => 'id',    ),    1 =>     array (      0 => 'text',      1 => '/admin/agents/supp',    ),  ),  4 =>   array (  ),  5 =>   array (  ),),
        'cicrtg_backend_user' => array (  0 =>   array (  ),  1 =>   array (    '_controller' => 'cicrtg\\BackendBundle\\Controller\\BackendController::usersAction',  ),  2 =>   array (  ),  3 =>   array (    0 =>     array (      0 => 'text',      1 => '/admin/users',    ),  ),  4 =>   array (  ),  5 =>   array (  ),),
        'cicrtg_backend_zone' => array (  0 =>   array (  ),  1 =>   array (    '_controller' => 'cicrtg\\BackendBundle\\Controller\\BackendController::zoneAction',  ),  2 =>   array (  ),  3 =>   array (    0 =>     array (      0 => 'text',      1 => '/admin/zone',    ),  ),  4 =>   array (  ),  5 =>   array (  ),),
        'cicrtg_backend_prefecture_modif' => array (  0 =>   array (    0 => 'id',  ),  1 =>   array (    '_controller' => 'cicrtg\\BackendBundle\\Controller\\BackendController::modifprefectureAction',  ),  2 =>   array (    'id' => '\\d+',  ),  3 =>   array (    0 =>     array (      0 => 'variable',      1 => '/',      2 => '\\d+',      3 => 'id',    ),    1 =>     array (      0 => 'text',      1 => '/admin/zone/prefecture',    ),  ),  4 =>   array (  ),  5 =>   array (  ),),
        'cicrtg_backend_localite_modif' => array (  0 =>   array (    0 => 'id',  ),  1 =>   array (    '_controller' => 'cicrtg\\BackendBundle\\Controller\\BackendController::modiflocaliteAction',  ),  2 =>   array (    'id' => '\\d+',  ),  3 =>   array (    0 =>     array (      0 => 'variable',      1 => '/',      2 => '\\d+',      3 => 'id',    ),    1 =>     array (      0 => 'text',      1 => '/admin/zone/localite',    ),  ),  4 =>   array (  ),  5 =>   array (  ),),
        'cicrtg_backend_prefecture_supp' => array (  0 =>   array (    0 => 'id',  ),  1 =>   array (    '_controller' => 'cicrtg\\BackendBundle\\Controller\\BackendController::supprefectureAction',  ),  2 =>   array (    'id' => '\\d+',  ),  3 =>   array (    0 =>     array (      0 => 'variable',      1 => '/',      2 => '\\d+',      3 => 'id',    ),    1 =>     array (      0 => 'text',      1 => '/admin/zone/prefecture/supp',    ),  ),  4 =>   array (  ),  5 =>   array (  ),),
        'cicrtg_backend_localite_supp' => array (  0 =>   array (    0 => 'id',  ),  1 =>   array (    '_controller' => 'cicrtg\\BackendBundle\\Controller\\BackendController::suplocaliteAction',  ),  2 =>   array (    'id' => '\\d+',  ),  3 =>   array (    0 =>     array (      0 => 'variable',      1 => '/',      2 => '\\d+',      3 => 'id',    ),    1 =>     array (      0 => 'text',      1 => '/admin/zone/localite/supp',    ),  ),  4 =>   array (  ),  5 =>   array (  ),),
        'cicrtg_backend_localite' => array (  0 =>   array (  ),  1 =>   array (    '_controller' => 'cicrtg\\BackendBundle\\Controller\\BackendController::localiteAction',  ),  2 =>   array (  ),  3 =>   array (    0 =>     array (      0 => 'text',      1 => '/admin/localite',    ),  ),  4 =>   array (  ),  5 =>   array (  ),),
        'cicrtg_backend_type' => array (  0 =>   array (  ),  1 =>   array (    '_controller' => 'cicrtg\\BackendBundle\\Controller\\BackendController::typeDonneesAction',  ),  2 =>   array (    'method' => 'GET',  ),  3 =>   array (    0 =>     array (      0 => 'text',      1 => '/admin/typeDonnees',    ),  ),  4 =>   array (  ),  5 =>   array (  ),),
        'cicrtg_backend_pheno_post' => array (  0 =>   array (  ),  1 =>   array (    '_controller' => 'cicrtg\\BackendBundle\\Controller\\BackendController::phenoPostAction',  ),  2 =>   array (    'method' => 'POST',  ),  3 =>   array (    0 =>     array (      0 => 'text',      1 => '/admin/pheno/traitement',    ),  ),  4 =>   array (  ),  5 =>   array (  ),),
        'cicrtg_backend_pheno_delete' => array (  0 =>   array (    0 => 'id',  ),  1 =>   array (    '_controller' => 'cicrtg\\BackendBundle\\Controller\\BackendController::phenoDelAction',  ),  2 =>   array (    'id' => '\\d+',  ),  3 =>   array (    0 =>     array (      0 => 'variable',      1 => '/',      2 => '\\d+',      3 => 'id',    ),    1 =>     array (      0 => 'text',      1 => '/admin/pheno/delete',    ),  ),  4 =>   array (  ),  5 =>   array (  ),),
        'cicrtg_backend_type_post' => array (  0 =>   array (  ),  1 =>   array (    '_controller' => 'cicrtg\\BackendBundle\\Controller\\BackendController::typePostAction',  ),  2 =>   array (    'method' => 'POST',  ),  3 =>   array (    0 =>     array (      0 => 'text',      1 => '/admin/type/traitement',    ),  ),  4 =>   array (  ),  5 =>   array (  ),),
        'cicrtg_backend_type_delete' => array (  0 =>   array (    0 => 'id',  ),  1 =>   array (    '_controller' => 'cicrtg\\BackendBundle\\Controller\\BackendController::typeDelAction',  ),  2 =>   array (    'id' => '\\d+',  ),  3 =>   array (    0 =>     array (      0 => 'variable',      1 => '/',      2 => '\\d+',      3 => 'id',    ),    1 =>     array (      0 => 'text',      1 => '/admin/type/delete',    ),  ),  4 =>   array (  ),  5 =>   array (  ),),
        'cicrtg_frontend_homepage' => array (  0 =>   array (    0 => 'name',  ),  1 =>   array (    '_controller' => 'cicrtg\\FrontendBundle\\Controller\\DefaultController::indexAction',  ),  2 =>   array (  ),  3 =>   array (    0 =>     array (      0 => 'variable',      1 => '/',      2 => '[^/]++',      3 => 'name',    ),    1 =>     array (      0 => 'text',      1 => '/hello',    ),  ),  4 =>   array (  ),  5 =>   array (  ),),
        'frontend_homepage' => array (  0 =>   array (  ),  1 =>   array (    '_controller' => 'cicrtg\\FrontendBundle\\Controller\\FrontendController::indexAction',  ),  2 =>   array (  ),  3 =>   array (    0 =>     array (      0 => 'text',      1 => '/',    ),  ),  4 =>   array (  ),  5 =>   array (  ),),
        'frontend_donnee' => array (  0 =>   array (  ),  1 =>   array (    '_controller' => 'cicrtg\\FrontendBundle\\Controller\\FrontendController::donneesAction',  ),  2 =>   array (  ),  3 =>   array (    0 =>     array (      0 => 'text',      1 => '/donnees',    ),  ),  4 =>   array (  ),  5 =>   array (  ),),
        'frontend_donnee_zone' => array (  0 =>   array (    0 => 'id',  ),  1 =>   array (    '_controller' => 'cicrtg\\FrontendBundle\\Controller\\FrontendController::donneeszoneAction',  ),  2 =>   array (    'id' => '\\d*',  ),  3 =>   array (    0 =>     array (      0 => 'variable',      1 => '/',      2 => '\\d*',      3 => 'id',    ),    1 =>     array (      0 => 'text',      1 => '/donnees/zone',    ),  ),  4 =>   array (  ),  5 =>   array (  ),),
        'frontend_mode' => array (  0 =>   array (  ),  1 =>   array (    '_controller' => 'cicrtg\\FrontendBundle\\Controller\\FrontendController::modeAction',  ),  2 =>   array (  ),  3 =>   array (    0 =>     array (      0 => 'text',      1 => '/mode_demploi',    ),  ),  4 =>   array (  ),  5 =>   array (  ),),
        'frontend_plan' => array (  0 =>   array (  ),  1 =>   array (    '_controller' => 'cicrtg\\FrontendBundle\\Controller\\FrontendController::planAction',  ),  2 =>   array (  ),  3 =>   array (    0 =>     array (      0 => 'text',      1 => '/plan_site',    ),  ),  4 =>   array (  ),  5 =>   array (  ),),
        'frontend_contact' => array (  0 =>   array (  ),  1 =>   array (    '_controller' => 'cicrtg\\FrontendBundle\\Controller\\FrontendController::contactAction',  ),  2 =>   array (  ),  3 =>   array (    0 =>     array (      0 => 'text',      1 => '/contact',    ),  ),  4 =>   array (  ),  5 =>   array (  ),),
        'login' => array (  0 =>   array (  ),  1 =>   array (    '_controller' => 'cicrtg\\UserBundle\\Controller\\SecurityController::loginAction',  ),  2 =>   array (  ),  3 =>   array (    0 =>     array (      0 => 'text',      1 => '/login',    ),  ),  4 =>   array (  ),  5 =>   array (  ),),
        'login_check' => array (  0 =>   array (  ),  1 =>   array (  ),  2 =>   array (  ),  3 =>   array (    0 =>     array (      0 => 'text',      1 => '/login_check',    ),  ),  4 =>   array (  ),  5 =>   array (  ),),
        'logout' => array (  0 =>   array (  ),  1 =>   array (  ),  2 =>   array (  ),  3 =>   array (    0 =>     array (      0 => 'text',      1 => '/logout',    ),  ),  4 =>   array (  ),  5 =>   array (  ),),
    );

    /**
     * Constructor.
     */
    public function __construct(RequestContext $context, LoggerInterface $logger = null)
    {
        $this->context = $context;
        $this->logger = $logger;
    }

    public function generate($name, $parameters = array(), $referenceType = self::ABSOLUTE_PATH)
    {
        if (!isset(self::$declaredRoutes[$name])) {
            throw new RouteNotFoundException(sprintf('Unable to generate a URL for the named route "%s" as such route does not exist.', $name));
        }

        list($variables, $defaults, $requirements, $tokens, $hostTokens, $requiredSchemes) = self::$declaredRoutes[$name];

        return $this->doGenerate($variables, $defaults, $requirements, $tokens, $parameters, $name, $referenceType, $hostTokens, $requiredSchemes);
    }
}

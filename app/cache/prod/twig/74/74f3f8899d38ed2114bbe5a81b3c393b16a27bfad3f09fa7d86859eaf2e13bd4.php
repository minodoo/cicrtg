<?php

/* cicrtgUserBundle:Security:login.html.twig */
class __TwigTemplate_7fcc781ecb0c811f608a0f98be70d4ea6b5c12ee08ff8550619c1bf6b09998cc extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        // line 1
        $this->parent = $this->loadTemplate("::layout.html.twig", "cicrtgUserBundle:Security:login.html.twig", 1);
        $this->blocks = array(
            'title' => array($this, 'block_title'),
            'body_general' => array($this, 'block_body_general'),
        );
    }

    protected function doGetParent(array $context)
    {
        return "::layout.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $this->parent->display($context, array_merge($this->blocks, $blocks));
    }

    // line 3
    public function block_title($context, array $blocks = array())
    {
        echo " ";
        $this->displayParentBlock("title", $context, $blocks);
        echo " Login ";
    }

    // line 4
    public function block_body_general($context, array $blocks = array())
    {
        // line 5
        echo "           ";
        $this->loadTemplate("cicrtgUserBundle:includes:loginBody.html.twig", "cicrtgUserBundle:Security:login.html.twig", 5)->display($context);
    }

    public function getTemplateName()
    {
        return "cicrtgUserBundle:Security:login.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  40 => 5,  37 => 4,  29 => 3,  11 => 1,);
    }
}
/* {% extends "::layout.html.twig" %}*/
/* */
/* {% block title %} {{ parent() }} Login {% endblock %}*/
/* {% block body_general %}*/
/*            {% include "cicrtgUserBundle:includes:loginBody.html.twig" %}*/
/* {% endblock %}*/
/* */

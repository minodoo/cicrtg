<?php

/* cicrtgBackendBundle:includes:nav.html.twig */
class __TwigTemplate_497279885fed2e89e4ad3283f0168f7a09aaf4b8dc0c8da7d95db1e86dcecfd7 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        // line 1
        echo "﻿<nav class=\"navbar navbar-inverse navbar-fixed-top\" role=\"navigation\">
                    <!-- Brand and toggle get grouped for better mobile display -->
                    <div class=\"navbar-header\">
                        <button type=\"button\" class=\"navbar-toggle\" data-toggle=\"collapse\" data-target=\".navbar-ex1-collapse\">
                            <span class=\"sr-only\">Toggle navigation</span>
                            <span class=\"icon-bar\"></span>
                            <span class=\"icon-bar\"></span>
                            <span class=\"icon-bar\"></span>
                        </button>
                        <a class=\"navbar-brand\" href=\"";
        // line 10
        echo $this->env->getExtension('routing')->getPath("frontend_homepage");
        echo "\">CICRTG
                             ";
        // line 11
        if ($this->env->getExtension('security')->isGranted("ROLE_EXPLOITANT")) {
            // line 12
            echo "                                 Admin
                            ";
        }
        // line 14
        echo "                        </a>
                    </div>
                    <!-- Top Menu Items -->
                    <ul class=\"nav navbar-right top-nav\">
                        
                        <li class=\"dropdown\">
                            <a href=\"#\" class=\"dropdown-toggle\" data-toggle=\"dropdown\"><i class=\"fa fa-user\"></i>
                                Bienvenu
                                ";
        // line 22
        if ($this->env->getExtension('security')->isGranted("ROLE_EXPLOITANT")) {
            // line 23
            echo "                                    ";
            echo twig_escape_filter($this->env, $this->getAttribute($this->getAttribute((isset($context["app"]) ? $context["app"] : null), "user", array()), "username", array()), "html", null, true);
            echo "
                                ";
        } else {
            // line 25
            echo "                                    A vous.
                                ";
        }
        // line 27
        echo "                                <b class=\"caret\"></b></a>
                            <ul class=\"dropdown-menu\">
                                ";
        // line 29
        if ($this->env->getExtension('security')->isGranted("ROLE_EXPLOITANT")) {
            // line 30
            echo "                                    <li>
                                        <a href=\"#\"><i class=\"fa fa-fw fa-user\"></i> Profile</a>
                                    </li>
                                    <!--<li>
                                        <a href=\"#\"><i class=\"fa fa-fw fa-envelope\"></i> Inbox</a>
                                    </li>
                                    <li>
                                        <a href=\"#\"><i class=\"fa fa-fw fa-gear\"></i> Settings</a>
                                    </li>-->
                                    <li class=\"divider\"></li>
                                    <li>
                                        <a href=\"";
            // line 41
            echo $this->env->getExtension('routing')->getPath("logout");
            echo "\"><i class=\"fa fa-fw fa-power-off\"></i> Deconnèxion</a>
                                    </li>
                                ";
        } else {
            // line 44
            echo "                                    <li>
                                        <a href=\"";
            // line 45
            echo $this->env->getExtension('routing')->getPath("login");
            echo "\"><i class=\"fa fa-fw fa-power-off\"></i> Connèxion</a>
                                    </li>
                                ";
        }
        // line 48
        echo "                            </ul>
                        </li>
                    </ul>
                    <!-- Sidebar Menu Items - These collapse to the responsive navigation menu on small screens -->
                    <div class=\"collapse navbar-collapse navbar-ex1-collapse\">
                        <ul class=\"nav navbar-nav side-nav\">
                            ";
        // line 54
        if ($this->env->getExtension('security')->isGranted("ROLE_EXPLOITANT")) {
            // line 55
            echo "                                <li class=\"active\">
                                    <a href=\"";
            // line 56
            echo $this->env->getExtension('routing')->getPath("cicrtg_backend_home");
            echo "\"><i class=\"fa fa-fw fa-dashboard\"></i> Acceuil</a>
                                </li>
                                <li>
                                    <a href=\"javascript:;\" data-toggle=\"collapse\" data-target=\"#donnees\"><i class=\"fa fa-bars\"></i> Données <i class=\"fa fa-fw fa-caret-down\"></i></a>
                                    <ul id=\"donnees\" class=\"collapse\">
                                        ";
            // line 61
            $context['_parent'] = $context;
            $context['_seq'] = twig_ensure_traversable((isset($context["phenomenes"]) ? $context["phenomenes"] : null));
            foreach ($context['_seq'] as $context["_key"] => $context["pheno"]) {
                // line 62
                echo "                                            <li>
                                                <a href=\"";
                // line 63
                echo twig_escape_filter($this->env, $this->env->getExtension('routing')->getPath("cicrtg_backend_donnee", array("id" => $this->getAttribute($context["pheno"], "id", array()))), "html", null, true);
                echo "\">";
                echo twig_escape_filter($this->env, $this->getAttribute($context["pheno"], "lib", array()), "html", null, true);
                echo "</a>
                                            </li>
                                         ";
            }
            $_parent = $context['_parent'];
            unset($context['_seq'], $context['_iterated'], $context['_key'], $context['pheno'], $context['_parent'], $context['loop']);
            $context = array_intersect_key($context, $_parent) + $_parent;
            // line 65
            echo "  
                            
                                    </ul>
                                </li>
                            
                            <!--<li>
                                <a href=\"bootstrap-elements.html\"><i class=\"fa fa-fw fa-desktop\"></i> Bootstrap Elements</a>
                            </li>-->
                            <!--<li>-->
                            <!--    <a href=\"bootstrap-grid.html\"><i class=\"fa fa-fw fa-arrows-v\"></i> Bootstrap Grid</a>-->
                            <!--</li>-->
                            <li>
                                <a href=\"javascript:;\" data-toggle=\"collapse\" data-target=\"#params\"><i class=\"fa fa-fw fa-wrench\"></i> Paramètres <i class=\"fa fa-fw fa-caret-down\"></i></a>
                                <ul id=\"params\" class=\"collapse\">
                                    <li>
                                        <a href=\"";
            // line 80
            echo $this->env->getExtension('routing')->getPath("cicrtg_backend_user");
            echo "\">Utilisateurs</a>
                                    </li>
                                    <li>
                                        <a href=\"";
            // line 83
            echo $this->env->getExtension('routing')->getPath("cicrtg_backend_agent");
            echo "\">Agents</a>
                                    </li>
                                    <li>
                                        <a href=\"";
            // line 86
            echo $this->env->getExtension('routing')->getPath("cicrtg_backend_zone");
            echo "\">Zones</a>
                                    </li>
                                    <li>
                                        <a href=\"";
            // line 89
            echo $this->env->getExtension('routing')->getPath("cicrtg_backend_type");
            echo "\">Type de donnees</a>
                                    </li>
                        
                                </ul>
                            </li>
                            ";
        } else {
            // line 95
            echo "                                <li>
                                    <a href=\"";
            // line 96
            echo $this->env->getExtension('routing')->getPath("login");
            echo "\"><i class=\"fa fa-fw fa-arrows-v\"></i> Connexion </a>-->
                                </li>
                                
                                <li>
                                <a href=\"javascript:;\" data-toggle=\"collapse\" data-target=\"#donnees\"><i class=\"fa fa-bars\"></i> Données <i class=\"fa fa-fw fa-caret-down\"></i></a>
                                <ul id=\"donnees\" class=\"collapse\">
                                    ";
            // line 102
            $context['_parent'] = $context;
            $context['_seq'] = twig_ensure_traversable((isset($context["phenomenes"]) ? $context["phenomenes"] : null));
            foreach ($context['_seq'] as $context["_key"] => $context["pheno"]) {
                // line 103
                echo "                                        <li>
                                            <a href=\"";
                // line 104
                echo twig_escape_filter($this->env, $this->env->getExtension('routing')->getPath("cicrtg_backend_donnee", array("id" => $this->getAttribute($context["pheno"], "id", array()))), "html", null, true);
                echo "\">";
                echo twig_escape_filter($this->env, $this->getAttribute($context["pheno"], "lib", array()), "html", null, true);
                echo "</a>
                                        </li>
                                     ";
            }
            $_parent = $context['_parent'];
            unset($context['_seq'], $context['_iterated'], $context['_key'], $context['pheno'], $context['_parent'], $context['loop']);
            $context = array_intersect_key($context, $_parent) + $_parent;
            // line 106
            echo "  
                        
                                </ul>
                            </li>
                            ";
        }
        // line 111
        echo "                            <!--<li>-->
                            <!--    <a href=\"blank-page.html\"><i class=\"fa fa-fw fa-file\"></i> Blank Page</a>-->
                            <!--</li>-->
                            <!--<li>-->
                            <!--    <a href=\"index-rtl.html\"><i class=\"fa fa-fw fa-dashboard\"></i> RTL Dashboard</a>-->
                            <!--</li>-->
                        </ul>
                    </div>
                    <!-- /.navbar-collapse -->
                </nav>";
    }

    public function getTemplateName()
    {
        return "cicrtgBackendBundle:includes:nav.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  216 => 111,  209 => 106,  198 => 104,  195 => 103,  191 => 102,  182 => 96,  179 => 95,  170 => 89,  164 => 86,  158 => 83,  152 => 80,  135 => 65,  124 => 63,  121 => 62,  117 => 61,  109 => 56,  106 => 55,  104 => 54,  96 => 48,  90 => 45,  87 => 44,  81 => 41,  68 => 30,  66 => 29,  62 => 27,  58 => 25,  52 => 23,  50 => 22,  40 => 14,  36 => 12,  34 => 11,  30 => 10,  19 => 1,);
    }
}
/* ﻿<nav class="navbar navbar-inverse navbar-fixed-top" role="navigation">*/
/*                     <!-- Brand and toggle get grouped for better mobile display -->*/
/*                     <div class="navbar-header">*/
/*                         <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-ex1-collapse">*/
/*                             <span class="sr-only">Toggle navigation</span>*/
/*                             <span class="icon-bar"></span>*/
/*                             <span class="icon-bar"></span>*/
/*                             <span class="icon-bar"></span>*/
/*                         </button>*/
/*                         <a class="navbar-brand" href="{{ path('frontend_homepage') }}">CICRTG*/
/*                              {% if is_granted('ROLE_EXPLOITANT')  %}*/
/*                                  Admin*/
/*                             {% endif %}*/
/*                         </a>*/
/*                     </div>*/
/*                     <!-- Top Menu Items -->*/
/*                     <ul class="nav navbar-right top-nav">*/
/*                         */
/*                         <li class="dropdown">*/
/*                             <a href="#" class="dropdown-toggle" data-toggle="dropdown"><i class="fa fa-user"></i>*/
/*                                 Bienvenu*/
/*                                 {% if is_granted('ROLE_EXPLOITANT')  %}*/
/*                                     {{ app.user.username }}*/
/*                                 {% else %}*/
/*                                     A vous.*/
/*                                 {% endif %}*/
/*                                 <b class="caret"></b></a>*/
/*                             <ul class="dropdown-menu">*/
/*                                 {% if is_granted('ROLE_EXPLOITANT')  %}*/
/*                                     <li>*/
/*                                         <a href="#"><i class="fa fa-fw fa-user"></i> Profile</a>*/
/*                                     </li>*/
/*                                     <!--<li>*/
/*                                         <a href="#"><i class="fa fa-fw fa-envelope"></i> Inbox</a>*/
/*                                     </li>*/
/*                                     <li>*/
/*                                         <a href="#"><i class="fa fa-fw fa-gear"></i> Settings</a>*/
/*                                     </li>-->*/
/*                                     <li class="divider"></li>*/
/*                                     <li>*/
/*                                         <a href="{{ path('logout') }}"><i class="fa fa-fw fa-power-off"></i> Deconnèxion</a>*/
/*                                     </li>*/
/*                                 {% else %}*/
/*                                     <li>*/
/*                                         <a href="{{ path('login') }}"><i class="fa fa-fw fa-power-off"></i> Connèxion</a>*/
/*                                     </li>*/
/*                                 {% endif %}*/
/*                             </ul>*/
/*                         </li>*/
/*                     </ul>*/
/*                     <!-- Sidebar Menu Items - These collapse to the responsive navigation menu on small screens -->*/
/*                     <div class="collapse navbar-collapse navbar-ex1-collapse">*/
/*                         <ul class="nav navbar-nav side-nav">*/
/*                             {% if is_granted('ROLE_EXPLOITANT')  %}*/
/*                                 <li class="active">*/
/*                                     <a href="{{ path('cicrtg_backend_home') }}"><i class="fa fa-fw fa-dashboard"></i> Acceuil</a>*/
/*                                 </li>*/
/*                                 <li>*/
/*                                     <a href="javascript:;" data-toggle="collapse" data-target="#donnees"><i class="fa fa-bars"></i> Données <i class="fa fa-fw fa-caret-down"></i></a>*/
/*                                     <ul id="donnees" class="collapse">*/
/*                                         {% for pheno in phenomenes %}*/
/*                                             <li>*/
/*                                                 <a href="{{ path('cicrtg_backend_donnee', {'id':pheno.id}) }}">{{ pheno.lib }}</a>*/
/*                                             </li>*/
/*                                          {% endfor %}  */
/*                             */
/*                                     </ul>*/
/*                                 </li>*/
/*                             */
/*                             <!--<li>*/
/*                                 <a href="bootstrap-elements.html"><i class="fa fa-fw fa-desktop"></i> Bootstrap Elements</a>*/
/*                             </li>-->*/
/*                             <!--<li>-->*/
/*                             <!--    <a href="bootstrap-grid.html"><i class="fa fa-fw fa-arrows-v"></i> Bootstrap Grid</a>-->*/
/*                             <!--</li>-->*/
/*                             <li>*/
/*                                 <a href="javascript:;" data-toggle="collapse" data-target="#params"><i class="fa fa-fw fa-wrench"></i> Paramètres <i class="fa fa-fw fa-caret-down"></i></a>*/
/*                                 <ul id="params" class="collapse">*/
/*                                     <li>*/
/*                                         <a href="{{ path('cicrtg_backend_user') }}">Utilisateurs</a>*/
/*                                     </li>*/
/*                                     <li>*/
/*                                         <a href="{{ path('cicrtg_backend_agent') }}">Agents</a>*/
/*                                     </li>*/
/*                                     <li>*/
/*                                         <a href="{{ path('cicrtg_backend_zone') }}">Zones</a>*/
/*                                     </li>*/
/*                                     <li>*/
/*                                         <a href="{{ path('cicrtg_backend_type') }}">Type de donnees</a>*/
/*                                     </li>*/
/*                         */
/*                                 </ul>*/
/*                             </li>*/
/*                             {% else %}*/
/*                                 <li>*/
/*                                     <a href="{{ path('login') }}"><i class="fa fa-fw fa-arrows-v"></i> Connexion </a>-->*/
/*                                 </li>*/
/*                                 */
/*                                 <li>*/
/*                                 <a href="javascript:;" data-toggle="collapse" data-target="#donnees"><i class="fa fa-bars"></i> Données <i class="fa fa-fw fa-caret-down"></i></a>*/
/*                                 <ul id="donnees" class="collapse">*/
/*                                     {% for pheno in phenomenes %}*/
/*                                         <li>*/
/*                                             <a href="{{ path('cicrtg_backend_donnee', {'id':pheno.id}) }}">{{ pheno.lib }}</a>*/
/*                                         </li>*/
/*                                      {% endfor %}  */
/*                         */
/*                                 </ul>*/
/*                             </li>*/
/*                             {% endif %}*/
/*                             <!--<li>-->*/
/*                             <!--    <a href="blank-page.html"><i class="fa fa-fw fa-file"></i> Blank Page</a>-->*/
/*                             <!--</li>-->*/
/*                             <!--<li>-->*/
/*                             <!--    <a href="index-rtl.html"><i class="fa fa-fw fa-dashboard"></i> RTL Dashboard</a>-->*/
/*                             <!--</li>-->*/
/*                         </ul>*/
/*                     </div>*/
/*                     <!-- /.navbar-collapse -->*/
/*                 </nav>*/

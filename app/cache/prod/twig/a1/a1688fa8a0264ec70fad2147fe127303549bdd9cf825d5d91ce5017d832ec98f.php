<?php

/* cicrtgBackendBundle:includes:prefectureModifBody.html.twig */
class __TwigTemplate_4cfaab9d1714f29c6b4cd0b4c4ec0a4c7083bdb8a2bedc537c322f2796c40e18 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        // line 1
        echo "﻿";
        $context['_parent'] = $context;
        $context['_seq'] = twig_ensure_traversable($this->getAttribute($this->getAttribute($this->getAttribute((isset($context["app"]) ? $context["app"] : null), "session", array()), "flashbag", array()), "get", array(0 => "notice"), "method"));
        foreach ($context['_seq'] as $context["_key"] => $context["message"]) {
            // line 2
            echo "\t\t\t\t
    <div class=\"alert alert-success\">
        <a class=\"close fade in\" data-dismiss=\"alert\">&times;</a>
        ";
            // line 5
            echo twig_escape_filter($this->env, $context["message"], "html", null, true);
            echo "
    </div>
";
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['message'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 8
        echo "
<div class=\"row\">
\t<div class=\"col-xs-offset-3 col-xs-6\">
\t\t<div class=\"panel panel-info\">
\t\t
\t\t\t<div class=\"panel-heading\" style=\"color: #000000;\">
\t\t\t\t<h4 class=\"panel-title\">Modification de Préfecture</h4>
\t\t\t</div>
\t\t\t<div class=\"panel-body\" style=\"color: #000000;\">

\t\t\t\t  <form  method=\"post\" action=\"";
        // line 18
        echo twig_escape_filter($this->env, $this->env->getExtension('routing')->getPath("cicrtg_backend_prefecture_modif", array("id" => $this->getAttribute((isset($context["prefecture"]) ? $context["prefecture"] : null), "id", array()))), "html", null, true);
        echo "\">
\t\t\t\t\t  <span class=\"help-block\">
\t\t\t\t\t\t  ";
        // line 20
        echo $this->env->getExtension('form')->renderer->searchAndRenderBlock((isset($context["formPmodif"]) ? $context["formPmodif"] : null), 'errors');
        echo "
\t\t\t\t\t  </span>
\t\t\t\t\t\t  
\t\t\t\t\t\t <div class=\"form-group\">
\t\t\t\t\t\t   <div class=\"row\">
\t\t\t\t\t\t\t   <div class=\"col-md-3\">
\t\t\t\t\t\t\t\t   <label for=\"groupName\" >";
        // line 26
        echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute((isset($context["formPmodif"]) ? $context["formPmodif"] : null), "libP", array()), 'label', array("label" => "Le libellé"));
        echo " :</label>
\t\t\t\t\t\t\t   </div>

\t\t\t\t\t\t\t\t <small class=\"text-danger\"> ";
        // line 29
        echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute((isset($context["formPmodif"]) ? $context["formPmodif"] : null), "libP", array()), 'errors');
        echo " </small>
\t\t\t\t\t\t\t\t <div class=\"col-md-7 \">
\t\t\t\t\t\t\t\t\t  ";
        // line 31
        echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute((isset($context["formPmodif"]) ? $context["formPmodif"] : null), "libP", array()), 'widget', array("attr" => array("class" => "form-control", "placeholder" => "La région")));
        echo "
\t\t\t\t\t\t\t\t  </div>
\t\t\t\t\t\t\t </div>
\t\t\t\t\t\t </div>

\t\t\t\t\t\t<div class=\"form-group\">
\t\t\t\t\t\t\t<div class=\"row\">
\t\t\t\t\t\t\t\t<div class=\"col-sm-3\">
\t\t\t\t\t\t\t\t\t<label for=\"id\" >";
        // line 39
        echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute((isset($context["formPmodif"]) ? $context["formPmodif"] : null), "region", array()), 'label', array("label" => "la région"));
        echo " : </label>
\t\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t\t\t  <small class=\"text-danger\"> ";
        // line 41
        echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute((isset($context["formPmodif"]) ? $context["formPmodif"] : null), "region", array()), 'errors');
        echo " </small>
\t\t\t\t\t\t\t\t  <div class=\"col-sm-7 form-group\">
\t\t\t\t\t\t\t\t\t  ";
        // line 43
        echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute((isset($context["formPmodif"]) ? $context["formPmodif"] : null), "region", array()), 'widget', array("attr" => array("class" => "form-control")));
        echo "
\t\t\t\t\t\t\t\t  </div>
\t\t\t\t\t\t\t  </div>
\t\t\t\t\t\t  </div>


\t\t\t\t\t\t<div class=\"modal-footer\">
\t\t\t\t\t\t\t  
\t\t\t\t\t\t\t  <input type=\"submit\" class=\"btn btn-primary\" />
\t\t\t\t\t\t\t &nbsp; <a href=\"";
        // line 52
        echo $this->env->getExtension('routing')->getPath("cicrtg_backend_zone");
        echo "\" class=\"btn btn-default pull-right\">Retour</a>
\t\t\t\t\t\t\t <!--<button class=\"btn btn-default pull-right\" data-dismiss=\"modal\">Fermer !</button>-->
\t\t\t\t\t\t</div>
\t\t\t\t\t  ";
        // line 55
        echo $this->env->getExtension('form')->renderer->searchAndRenderBlock((isset($context["formPmodif"]) ? $context["formPmodif"] : null), 'rest');
        echo "
\t\t\t\t  </form>

\t\t\t\t\t 
\t\t\t</div>

\t\t
\t</div>

\t</div>
</div>
\t";
    }

    public function getTemplateName()
    {
        return "cicrtgBackendBundle:includes:prefectureModifBody.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  114 => 55,  108 => 52,  96 => 43,  91 => 41,  86 => 39,  75 => 31,  70 => 29,  64 => 26,  55 => 20,  50 => 18,  38 => 8,  29 => 5,  24 => 2,  19 => 1,);
    }
}
/* ﻿{% for message in app.session.flashbag.get('notice') %}*/
/* 				*/
/*     <div class="alert alert-success">*/
/*         <a class="close fade in" data-dismiss="alert">&times;</a>*/
/*         {{ message }}*/
/*     </div>*/
/* {% endfor %}*/
/* */
/* <div class="row">*/
/* 	<div class="col-xs-offset-3 col-xs-6">*/
/* 		<div class="panel panel-info">*/
/* 		*/
/* 			<div class="panel-heading" style="color: #000000;">*/
/* 				<h4 class="panel-title">Modification de Préfecture</h4>*/
/* 			</div>*/
/* 			<div class="panel-body" style="color: #000000;">*/
/* */
/* 				  <form  method="post" action="{{ path('cicrtg_backend_prefecture_modif', {'id': prefecture.id }) }}">*/
/* 					  <span class="help-block">*/
/* 						  {{ form_errors(formPmodif) }}*/
/* 					  </span>*/
/* 						  */
/* 						 <div class="form-group">*/
/* 						   <div class="row">*/
/* 							   <div class="col-md-3">*/
/* 								   <label for="groupName" >{{ form_label(formPmodif.libP, "Le libellé") }} :</label>*/
/* 							   </div>*/
/* */
/* 								 <small class="text-danger"> {{ form_errors(formPmodif.libP) }} </small>*/
/* 								 <div class="col-md-7 ">*/
/* 									  {{ form_widget(formPmodif.libP, { 'attr': {'class':'form-control', 'placeholder':'La région'} }) }}*/
/* 								  </div>*/
/* 							 </div>*/
/* 						 </div>*/
/* */
/* 						<div class="form-group">*/
/* 							<div class="row">*/
/* 								<div class="col-sm-3">*/
/* 									<label for="id" >{{ form_label(formPmodif.region, "la région") }} : </label>*/
/* 								</div>*/
/* 								  <small class="text-danger"> {{ form_errors(formPmodif.region) }} </small>*/
/* 								  <div class="col-sm-7 form-group">*/
/* 									  {{ form_widget(formPmodif.region, { 'attr': {'class':'form-control'} }) }}*/
/* 								  </div>*/
/* 							  </div>*/
/* 						  </div>*/
/* */
/* */
/* 						<div class="modal-footer">*/
/* 							  */
/* 							  <input type="submit" class="btn btn-primary" />*/
/* 							 &nbsp; <a href="{{ path('cicrtg_backend_zone') }}" class="btn btn-default pull-right">Retour</a>*/
/* 							 <!--<button class="btn btn-default pull-right" data-dismiss="modal">Fermer !</button>-->*/
/* 						</div>*/
/* 					  {{ form_rest(formPmodif) }}*/
/* 				  </form>*/
/* */
/* 					 */
/* 			</div>*/
/* */
/* 		*/
/* 	</div>*/
/* */
/* 	</div>*/
/* </div>*/
/* 	*/

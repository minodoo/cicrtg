<?php

/* cicrtgBackendBundle:includes:agentModifBody.html.twig */
class __TwigTemplate_2b70b6b6e70e36f6b09c2fd9e4f6b28b9d5325f1872d26eb65fe9db0007992c1 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        // line 1
        echo "﻿";
        $context['_parent'] = $context;
        $context['_seq'] = twig_ensure_traversable($this->getAttribute($this->getAttribute($this->getAttribute((isset($context["app"]) ? $context["app"] : null), "session", array()), "flashbag", array()), "get", array(0 => "notice"), "method"));
        foreach ($context['_seq'] as $context["_key"] => $context["message"]) {
            // line 2
            echo "\t\t\t\t
    <div class=\"alert alert-success\">
        <a class=\"close fade in\" data-dismiss=\"alert\">&times;</a>
        ";
            // line 5
            echo twig_escape_filter($this->env, $context["message"], "html", null, true);
            echo "
    </div>
";
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['message'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 8
        echo "
<div class=\"row\">
\t<div class=\"col-xs-offset-3 col-xs-6\">
\t\t<div class=\"panel panel-info\">
\t\t
\t\t\t<div class=\"panel-heading\" style=\"color: #000000;\">
\t\t\t\t<h4 class=\"panel-title\">Modification de L'agent sélèctionné</h4>
\t\t\t</div>
\t\t\t<div class=\"panel-body\" style=\"color: #000000;\">

\t\t\t\t  <form  method=\"post\" action=\"";
        // line 18
        echo twig_escape_filter($this->env, $this->env->getExtension('routing')->getPath("cicrtg_backend_agent_modif", array("id" => $this->getAttribute((isset($context["agent"]) ? $context["agent"] : null), "id", array()))), "html", null, true);
        echo "\">
\t\t\t\t\t  <form  method=\"post\" action=\"";
        // line 19
        echo $this->env->getExtension('routing')->getPath("cicrtg_backend_agent");
        echo "\">
                        <span class=\"help-block\">
                            ";
        // line 21
        echo $this->env->getExtension('form')->renderer->searchAndRenderBlock((isset($context["form"]) ? $context["form"] : null), 'errors');
        echo "
                        </span>
                            
                           <div class=\"form-group\">
                             <div class=\"row\">
                                 <div class=\"col-md-3\">
                                     <label for=\"groupName\" > ";
        // line 27
        echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute((isset($context["form"]) ? $context["form"] : null), "nom", array()), 'label', array("label" => "Nom"));
        echo " :</label>
                                 </div>
\t\t\t\t\t\t\t\t\t\t
                                   <small class=\"text-danger\"> ";
        // line 30
        echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute((isset($context["form"]) ? $context["form"] : null), "nom", array()), 'errors');
        echo " </small>
                                   <div class=\"col-md-7 \">
\t\t\t\t\t\t\t\t\t\t\t";
        // line 32
        echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute((isset($context["form"]) ? $context["form"] : null), "nom", array()), 'widget', array("attr" => array("class" => "form-control", "Placeholder" => "Le nom de l agent")));
        echo "
                                    </div>
                               </div>
                           </div>
\t\t\t\t\t\t\t
\t\t\t\t\t\t\t<div class=\"form-group\">
                             <div class=\"row\">
                                 <div class=\"col-md-3\">
                                     <label for=\"groupName\" > ";
        // line 40
        echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute((isset($context["form"]) ? $context["form"] : null), "prenom", array()), 'label', array("label" => "Prenom"));
        echo " :</label>
                                 </div>
\t\t\t\t\t\t\t\t\t\t
                                   <small class=\"text-danger\"> ";
        // line 43
        echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute((isset($context["form"]) ? $context["form"] : null), "prenom", array()), 'errors');
        echo " </small>
                                   <div class=\"col-md-7 \">
\t\t\t\t\t\t\t\t\t\t\t";
        // line 45
        echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute((isset($context["form"]) ? $context["form"] : null), "prenom", array()), 'widget', array("attr" => array("class" => "form-control", "Placeholder" => "Le prénom")));
        echo "
                                    </div>
                               </div>
                           </div>
\t\t\t\t\t\t\t
\t\t\t\t\t\t\t<div class=\"form-group\">
                             <div class=\"row\">
                                 <div class=\"col-md-3\">       
                                     <label for=\"groupName\" > ";
        // line 53
        echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute((isset($context["form"]) ? $context["form"] : null), "sexe", array()), 'label', array("label" => "Sexe"));
        echo " :</label>
                                 </div>
\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t 
                                   <small class=\"text-danger\"> ";
        // line 56
        echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute((isset($context["form"]) ? $context["form"] : null), "sexe", array()), 'errors');
        echo " </small>
                                   <div class=\"col-md-7 \">
\t\t\t\t\t\t\t\t\t\t\t";
        // line 58
        echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute((isset($context["form"]) ? $context["form"] : null), "sexe", array()), 'widget', array("attr" => array("class" => "form-control", "Placeholder" => "Veuillez choisir")));
        echo "
                                    </div>
                               </div>
                           </div>
\t\t\t\t\t\t\t<div class=\"form-group\">
                             <div class=\"row\">
                                 <div class=\"col-md-3\">
                                     <label for=\"groupName\" > ";
        // line 65
        echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute((isset($context["form"]) ? $context["form"] : null), "tel", array()), 'label', array("label" => "Téléphone"));
        echo " :</label>
                                 </div>
\t\t\t\t\t\t\t\t\t\t
                                   <small class=\"text-danger\"> ";
        // line 68
        echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute((isset($context["form"]) ? $context["form"] : null), "tel", array()), 'errors');
        echo " </small>
                                   <div class=\"col-md-7 \">
\t\t\t\t\t\t\t\t\t\t\t";
        // line 70
        echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute((isset($context["form"]) ? $context["form"] : null), "tel", array()), 'widget', array("attr" => array("class" => "form-control", "Placeholder" => "xx xx xx xx")));
        echo "
                                    </div>
                               </div>
                           </div>
\t\t\t\t\t\t\t
\t\t\t\t\t\t\t<div class=\"form-group\">
                             <div class=\"row\">
                                 <div class=\"col-md-3\">
                                     <label for=\"groupName\" > ";
        // line 78
        echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute((isset($context["form"]) ? $context["form"] : null), "codeS", array()), 'label', array("label" => "Code secret"));
        echo " :</label>
                                 </div>
\t\t\t\t\t\t\t\t\t\t
                                   <small class=\"text-danger\"> ";
        // line 81
        echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute((isset($context["form"]) ? $context["form"] : null), "codeS", array()), 'errors');
        echo " </small>
                                   <div class=\"col-md-7 \">
\t\t\t\t\t\t\t\t\t\t\t";
        // line 83
        echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute((isset($context["form"]) ? $context["form"] : null), "codeS", array()), 'widget', array("attr" => array("class" => "form-control", "Placeholder" => "Le code secret de l agent")));
        echo "
                                    </div>
                               </div>
                           </div>
\t\t\t\t\t\t\t
\t\t\t\t\t\t\t<div class=\"form-group\">
                             <div class=\"row\">
                                 <div class=\"col-md-3\">
                                     <label for=\"groupName\" > ";
        // line 91
        echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute((isset($context["form"]) ? $context["form"] : null), "email", array()), 'label', array("label" => "email"));
        echo " :</label>
                                 </div>
\t\t\t\t\t\t\t\t\t\t
                                   <small class=\"text-danger\"> ";
        // line 94
        echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute((isset($context["form"]) ? $context["form"] : null), "email", array()), 'errors');
        echo " </small>
                                   <div class=\"col-md-7 \">
\t\t\t\t\t\t\t\t\t\t\t";
        // line 96
        echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute((isset($context["form"]) ? $context["form"] : null), "email", array()), 'widget', array("attr" => array("class" => "form-control", "Placeholder" => "l email")));
        echo "
                                    </div>
                               </div>
                           </div>
\t\t\t\t\t\t\t<div class=\"form-group\">
                             <div class=\"row\">
                                 <div class=\"col-md-3\">
                                     <label for=\"groupName\" > ";
        // line 103
        echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute((isset($context["form"]) ? $context["form"] : null), "residence", array()), 'label', array("label" => "Résidence"));
        echo " :</label>
                                 </div>
\t\t\t\t\t\t\t\t\t\t
                                   <small class=\"text-danger\"> ";
        // line 106
        echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute((isset($context["form"]) ? $context["form"] : null), "residence", array()), 'errors');
        echo " </small>
                                   <div class=\"col-md-7 \">
\t\t\t\t\t\t\t\t\t\t\t";
        // line 108
        echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute((isset($context["form"]) ? $context["form"] : null), "residence", array()), 'widget', array("attr" => array("class" => "form-control", "Placeholder" => "La résidence")));
        echo "
                                    </div>
                               </div>
                           </div>
\t\t\t\t\t\t\t
\t\t\t\t\t\t\t<div class=\"form-group\">
                             <div class=\"row\">
                                 <div class=\"col-md-3\">
                                     <label for=\"groupName\" > ";
        // line 116
        echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute((isset($context["form"]) ? $context["form"] : null), "localites", array()), 'label', array("label" => "Localité"));
        echo " :</label>
                                 </div>
\t\t\t\t\t\t\t\t\t\t
                                   <small class=\"text-danger\"> ";
        // line 119
        echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute((isset($context["form"]) ? $context["form"] : null), "localites", array()), 'errors');
        echo " </small>
                                   <div class=\"col-md-7 \">
\t\t\t\t\t\t\t\t\t\t\t";
        // line 121
        echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute((isset($context["form"]) ? $context["form"] : null), "localites", array()), 'widget', array("attr" => array("class" => "form-control")));
        echo "
                                    </div>
                               </div>
                           </div>
\t\t\t\t\t\t<div class=\"modal-footer\">
\t\t\t\t\t\t\t  
\t\t\t\t\t\t\t  <input type=\"submit\" class=\"btn btn-primary\" />
\t\t\t\t\t\t\t &nbsp; <a href=\"";
        // line 128
        echo $this->env->getExtension('routing')->getPath("cicrtg_backend_agent");
        echo "\" class=\"btn btn-default pull-right\">Retour</a>
\t\t\t\t\t\t\t <!--<button class=\"btn btn-default pull-right\" data-dismiss=\"modal\">Fermer !</button>-->
\t\t\t\t\t\t</div>
\t\t\t\t\t  ";
        // line 131
        echo $this->env->getExtension('form')->renderer->searchAndRenderBlock((isset($context["form"]) ? $context["form"] : null), 'rest');
        echo "
\t\t\t\t  </form>

\t\t\t\t\t 
\t\t\t</div>

\t\t
\t</div>

\t</div>
</div>
\t";
    }

    public function getTemplateName()
    {
        return "cicrtgBackendBundle:includes:agentModifBody.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  247 => 131,  241 => 128,  231 => 121,  226 => 119,  220 => 116,  209 => 108,  204 => 106,  198 => 103,  188 => 96,  183 => 94,  177 => 91,  166 => 83,  161 => 81,  155 => 78,  144 => 70,  139 => 68,  133 => 65,  123 => 58,  118 => 56,  112 => 53,  101 => 45,  96 => 43,  90 => 40,  79 => 32,  74 => 30,  68 => 27,  59 => 21,  54 => 19,  50 => 18,  38 => 8,  29 => 5,  24 => 2,  19 => 1,);
    }
}
/* ﻿{% for message in app.session.flashbag.get('notice') %}*/
/* 				*/
/*     <div class="alert alert-success">*/
/*         <a class="close fade in" data-dismiss="alert">&times;</a>*/
/*         {{ message }}*/
/*     </div>*/
/* {% endfor %}*/
/* */
/* <div class="row">*/
/* 	<div class="col-xs-offset-3 col-xs-6">*/
/* 		<div class="panel panel-info">*/
/* 		*/
/* 			<div class="panel-heading" style="color: #000000;">*/
/* 				<h4 class="panel-title">Modification de L'agent sélèctionné</h4>*/
/* 			</div>*/
/* 			<div class="panel-body" style="color: #000000;">*/
/* */
/* 				  <form  method="post" action="{{ path('cicrtg_backend_agent_modif', {'id': agent.id }) }}">*/
/* 					  <form  method="post" action="{{ path('cicrtg_backend_agent') }}">*/
/*                         <span class="help-block">*/
/*                             {{ form_errors(form) }}*/
/*                         </span>*/
/*                             */
/*                            <div class="form-group">*/
/*                              <div class="row">*/
/*                                  <div class="col-md-3">*/
/*                                      <label for="groupName" > {{ form_label(form.nom, 'Nom') }} :</label>*/
/*                                  </div>*/
/* 										*/
/*                                    <small class="text-danger"> {{ form_errors(form.nom) }} </small>*/
/*                                    <div class="col-md-7 ">*/
/* 											{{ form_widget(form.nom,  {'attr': {'class':'form-control', 'Placeholder': 'Le nom de l agent'}}) }}*/
/*                                     </div>*/
/*                                </div>*/
/*                            </div>*/
/* 							*/
/* 							<div class="form-group">*/
/*                              <div class="row">*/
/*                                  <div class="col-md-3">*/
/*                                      <label for="groupName" > {{ form_label(form.prenom, 'Prenom') }} :</label>*/
/*                                  </div>*/
/* 										*/
/*                                    <small class="text-danger"> {{ form_errors(form.prenom) }} </small>*/
/*                                    <div class="col-md-7 ">*/
/* 											{{ form_widget(form.prenom,  {'attr': {'class':'form-control', 'Placeholder': 'Le prénom'}} ) }}*/
/*                                     </div>*/
/*                                </div>*/
/*                            </div>*/
/* 							*/
/* 							<div class="form-group">*/
/*                              <div class="row">*/
/*                                  <div class="col-md-3">       */
/*                                      <label for="groupName" > {{ form_label(form.sexe, 'Sexe') }} :</label>*/
/*                                  </div>*/
/* 															 */
/*                                    <small class="text-danger"> {{ form_errors(form.sexe) }} </small>*/
/*                                    <div class="col-md-7 ">*/
/* 											{{ form_widget(form.sexe,  {'attr': {'class':'form-control', 'Placeholder': 'Veuillez choisir'}} ) }}*/
/*                                     </div>*/
/*                                </div>*/
/*                            </div>*/
/* 							<div class="form-group">*/
/*                              <div class="row">*/
/*                                  <div class="col-md-3">*/
/*                                      <label for="groupName" > {{ form_label(form.tel, 'Téléphone') }} :</label>*/
/*                                  </div>*/
/* 										*/
/*                                    <small class="text-danger"> {{ form_errors(form.tel) }} </small>*/
/*                                    <div class="col-md-7 ">*/
/* 											{{ form_widget(form.tel,  {'attr': {'class':'form-control', 'Placeholder': 'xx xx xx xx'}}) }}*/
/*                                     </div>*/
/*                                </div>*/
/*                            </div>*/
/* 							*/
/* 							<div class="form-group">*/
/*                              <div class="row">*/
/*                                  <div class="col-md-3">*/
/*                                      <label for="groupName" > {{ form_label(form.codeS, 'Code secret') }} :</label>*/
/*                                  </div>*/
/* 										*/
/*                                    <small class="text-danger"> {{ form_errors(form.codeS) }} </small>*/
/*                                    <div class="col-md-7 ">*/
/* 											{{ form_widget(form.codeS,  {'attr': {'class':'form-control', 'Placeholder': 'Le code secret de l agent'}}) }}*/
/*                                     </div>*/
/*                                </div>*/
/*                            </div>*/
/* 							*/
/* 							<div class="form-group">*/
/*                              <div class="row">*/
/*                                  <div class="col-md-3">*/
/*                                      <label for="groupName" > {{ form_label(form.email, 'email') }} :</label>*/
/*                                  </div>*/
/* 										*/
/*                                    <small class="text-danger"> {{ form_errors(form.email) }} </small>*/
/*                                    <div class="col-md-7 ">*/
/* 											{{ form_widget(form.email,  {'attr': {'class':'form-control', 'Placeholder': 'l email'}}) }}*/
/*                                     </div>*/
/*                                </div>*/
/*                            </div>*/
/* 							<div class="form-group">*/
/*                              <div class="row">*/
/*                                  <div class="col-md-3">*/
/*                                      <label for="groupName" > {{ form_label(form.residence, 'Résidence') }} :</label>*/
/*                                  </div>*/
/* 										*/
/*                                    <small class="text-danger"> {{ form_errors(form.residence) }} </small>*/
/*                                    <div class="col-md-7 ">*/
/* 											{{ form_widget(form.residence,  {'attr': {'class':'form-control', 'Placeholder': 'La résidence'}}) }}*/
/*                                     </div>*/
/*                                </div>*/
/*                            </div>*/
/* 							*/
/* 							<div class="form-group">*/
/*                              <div class="row">*/
/*                                  <div class="col-md-3">*/
/*                                      <label for="groupName" > {{ form_label(form.localites, 'Localité') }} :</label>*/
/*                                  </div>*/
/* 										*/
/*                                    <small class="text-danger"> {{ form_errors(form.localites) }} </small>*/
/*                                    <div class="col-md-7 ">*/
/* 											{{ form_widget(form.localites,  {'attr': {'class':'form-control'}}) }}*/
/*                                     </div>*/
/*                                </div>*/
/*                            </div>*/
/* 						<div class="modal-footer">*/
/* 							  */
/* 							  <input type="submit" class="btn btn-primary" />*/
/* 							 &nbsp; <a href="{{ path('cicrtg_backend_agent') }}" class="btn btn-default pull-right">Retour</a>*/
/* 							 <!--<button class="btn btn-default pull-right" data-dismiss="modal">Fermer !</button>-->*/
/* 						</div>*/
/* 					  {{ form_rest(form) }}*/
/* 				  </form>*/
/* */
/* 					 */
/* 			</div>*/
/* */
/* 		*/
/* 	</div>*/
/* */
/* 	</div>*/
/* </div>*/
/* 	*/

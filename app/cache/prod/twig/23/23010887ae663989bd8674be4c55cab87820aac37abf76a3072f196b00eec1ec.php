<?php

/* cicrtgBackendBundle:includes:donneesBody.html.twig */
class __TwigTemplate_94db496397be77b4bc739dd147f11fd0f16adbfed5d7258c896d015e189bd8e8 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        // line 1
        echo "

<div class=\"row\">
<div class=\"col-lg-12\">
<div class=\"panel panel-default\">
    <div class=\"panel-heading\">
        <h3 class=\"panel-title\"><i class=\"fa fa-money fa-fw\"></i> ";
        // line 7
        echo twig_escape_filter($this->env, $this->getAttribute((isset($context["phenomene"]) ? $context["phenomene"] : null), "lib", array()), "html", null, true);
        echo "
        
        </h3>
        
    </div>
    <div class=\"panel-body\">
        <div class=\"\">
           <table id=\"datatable\" class=\"table table-bordered table-hover table-striped\">
            ";
        // line 15
        if (($this->getAttribute((isset($context["phenomene"]) ? $context["phenomene"] : null), "id", array()) == 1)) {
            // line 16
            echo "                <thead>
                    <tr>
                        <th>Localite</th>
                        <th>Date</th>
                        <th>Releve</th>
                    </tr>
                </thead>
                <tbody>
                    ";
            // line 24
            $context['_parent'] = $context;
            $context['_seq'] = twig_ensure_traversable((isset($context["donnees"]) ? $context["donnees"] : null));
            foreach ($context['_seq'] as $context["_key"] => $context["donnee"]) {
                echo "  
                           <tr>
                               <td>";
                // line 26
                echo twig_escape_filter($this->env, $this->getAttribute($this->getAttribute($this->getAttribute($this->getAttribute($context["donnee"], "agent", array()), "localites", array()), 0, array(), "array"), "libL", array()), "html", null, true);
                echo "</td>
                               <td>";
                // line 27
                echo twig_escape_filter($this->env, twig_localized_date_filter($this->env, $this->getAttribute($context["donnee"], "date", array()), "medium", "none", "fr_FR"), "html", null, true);
                echo "</td>
                               <td>";
                // line 28
                echo twig_escape_filter($this->env, $this->getAttribute($context["donnee"], "notation", array()), "html", null, true);
                echo "</td>
                               
                           </tr>
                           ";
            }
            $_parent = $context['_parent'];
            unset($context['_seq'], $context['_iterated'], $context['_key'], $context['donnee'], $context['_parent'], $context['loop']);
            $context = array_intersect_key($context, $_parent) + $_parent;
            // line 32
            echo "                </tbody>
<div class=\"modal fade\" id=\"phenomene\">
      <div class=\"modal-dialog\">
          <div class=\"modal-content\">
              <div class=\"modal-header\" style=\"color: #000000;\">
                  <button type=\"button\" class=\"close\" data-dismiss=\"modal\">x</button>
                  <h4 class=\"modal-title\">Ajout de ";
            // line 38
            echo twig_escape_filter($this->env, $this->getAttribute((isset($context["phenomene"]) ? $context["phenomene"] : null), "lib", array()), "html", null, true);
            echo "</h4>
              </div>
              <div class=\"modal-body\" style=\"color: #000000;\">

                    <form  method=\"post\" action=\"";
            // line 42
            echo $this->env->getExtension('routing')->getPath("cicrtg_backend_pheno_post");
            echo "\">
                        <span class=\"help-block\">
                            
                        </span>
                            
                           <div class=\"form-group\">
                             <div class=\"row\">
                                 <div class=\"col-md-3\">
                                     <label for=\"groupName\" >  :</label>
                                 </div>
\t\t\t\t\t\t\t\t\t\t
                                   <small class=\"text-danger\">  </small>
                                   <div class=\"col-md-7 \">
\t\t\t\t\t\t\t\t\t\t\t
                                    </div>
                               </div>
                           </div>
\t\t\t\t\t\t\t
\t\t\t\t\t\t\t

                          <div class=\"modal-footer\">
                                
                                <input type=\"submit\" class=\"btn btn-primary\" />
                               &nbsp; <button class=\"btn btn-default pull-right\" data-dismiss=\"modal\">Fermer !</button>
                          </div>
                        
                    </form>

                       
              </div>

          </div>
      </div>
    </div>
            ";
        }
        // line 77
        echo "              
              ";
        // line 78
        if (($this->getAttribute((isset($context["phenomene"]) ? $context["phenomene"] : null), "id", array()) == 2)) {
            // line 79
            echo "                <thead>
                    <tr>
                        <th>Localite</th>
                        <th>Date</th>
                        <th>hauteur de pluie</th>
                    </tr>
                </thead>
                <tbody>
                  
                  ";
            // line 88
            $context['_parent'] = $context;
            $context['_seq'] = twig_ensure_traversable((isset($context["donnees"]) ? $context["donnees"] : null));
            foreach ($context['_seq'] as $context["_key"] => $context["donnee"]) {
                echo "  
                           <tr>
                               <td>";
                // line 90
                echo twig_escape_filter($this->env, $this->getAttribute($this->getAttribute($this->getAttribute($this->getAttribute($context["donnee"], "agent", array()), "localites", array()), 0, array(), "array"), "libL", array()), "html", null, true);
                echo "</td>
                               <td>";
                // line 91
                echo twig_escape_filter($this->env, twig_localized_date_filter($this->env, $this->getAttribute($context["donnee"], "date", array()), "medium", "none", "fr_FR"), "html", null, true);
                echo "</td>
                               <td>";
                // line 92
                echo twig_escape_filter($this->env, $this->getAttribute($context["donnee"], "notation", array()), "html", null, true);
                echo "</td>
                               
                           </tr>
                           ";
            }
            $_parent = $context['_parent'];
            unset($context['_seq'], $context['_iterated'], $context['_key'], $context['donnee'], $context['_parent'], $context['loop']);
            $context = array_intersect_key($context, $_parent) + $_parent;
            // line 96
            echo "                </tbody>

<!--========================    forlulaire pour l'ajout de de climatographie =================================           -->

<div class=\"modal fade\" id=\"phenomene\">
      <div class=\"modal-dialog\">
          <div class=\"modal-content\">
              <div class=\"modal-header\" style=\"color: #000000;\">
                  <button type=\"button\" class=\"close\" data-dismiss=\"modal\">x</button>
                  <h4 class=\"modal-title\">Ajout de ";
            // line 105
            echo twig_escape_filter($this->env, $this->getAttribute((isset($context["phenomene"]) ? $context["phenomene"] : null), "lib", array()), "html", null, true);
            echo "</h4>
              </div>
              <div class=\"modal-body\" style=\"color: #000000;\">

                    <form  method=\"post\" action=\"";
            // line 109
            echo $this->env->getExtension('routing')->getPath("cicrtg_backend_pheno_post");
            echo "\">
                        <span class=\"help-block\">
                            
                        </span>
                            
                           <div class=\"form-group\">
                             <div class=\"row\">
                                 <div class=\"col-md-3\">
                                     <label for=\"groupName\" >  :</label>
                                 </div>
\t\t\t\t\t\t\t\t\t\t
                                   <small class=\"text-danger\">  </small>
                                   <div class=\"col-md-7 \">
\t\t\t\t\t\t\t\t\t\t\t
                                    </div>
                               </div>
                           </div>
\t\t\t\t\t\t\t
\t\t\t\t\t\t\t

                          <div class=\"modal-footer\">
                                
                                <input type=\"submit\" class=\"btn btn-primary\" />
                               &nbsp; <button class=\"btn btn-default pull-right\" data-dismiss=\"modal\">Fermer !</button>
                          </div>
                        
                    </form>

                       
              </div>

          </div>
      </div>
    </div>
<!--================================================================================================================-->
              ";
        }
        // line 145
        echo "
              ";
        // line 146
        if (($this->getAttribute((isset($context["phenomene"]) ? $context["phenomene"] : null), "id", array()) == 3)) {
            // line 147
            echo "                <thead>
                    <tr>
                        <th>Localite</th>
                        <th>Date</th>
                        <th>donnees</th>
                        <th>Type</th>
                    </tr>
                </thead>
                <tbody>
                           
                         ";
            // line 157
            $context['_parent'] = $context;
            $context['_seq'] = twig_ensure_traversable((isset($context["donnees"]) ? $context["donnees"] : null));
            foreach ($context['_seq'] as $context["_key"] => $context["donnee"]) {
                // line 158
                echo "                         
                           <tr>
                               <td>";
                // line 160
                echo twig_escape_filter($this->env, $this->getAttribute($this->getAttribute($this->getAttribute($this->getAttribute($context["donnee"], "agent", array()), "localites", array()), 0, array(), "array"), "libL", array()), "html", null, true);
                echo "</td>
                               <td>";
                // line 161
                echo twig_escape_filter($this->env, ((twig_localized_date_filter($this->env, $this->getAttribute($context["donnee"], "date", array()), "medium", "none", "fr_FR") . "  ") . twig_date_format_filter($this->env, $this->getAttribute($context["donnee"], "date", array()), "m")), "html", null, true);
                echo "
                                                      
                               </td>
                               <td>";
                // line 164
                echo twig_escape_filter($this->env, $this->getAttribute($context["donnee"], "notation", array()), "html", null, true);
                echo "</td>
                               <td>";
                // line 165
                echo twig_escape_filter($this->env, $this->getAttribute($this->getAttribute($context["donnee"], "type", array()), "libT", array()), "html", null, true);
                echo "
                               </td>
                               
                           </tr>
                                
                           ";
            }
            $_parent = $context['_parent'];
            unset($context['_seq'], $context['_iterated'], $context['_key'], $context['donnee'], $context['_parent'], $context['loop']);
            $context = array_intersect_key($context, $_parent) + $_parent;
            // line 171
            echo "                </tbody>
<!--========================    forlulaire pour l'ajout de de climatographie =================================           -->

<div class=\"modal fade\" id=\"phenomene\">
      <div class=\"modal-dialog\">
          <div class=\"modal-content\">
              <div class=\"modal-header\" style=\"color: #000000;\">
                  <button type=\"button\" class=\"close\" data-dismiss=\"modal\">x</button>
                  <h4 class=\"modal-title\">Ajout de ";
            // line 179
            echo twig_escape_filter($this->env, $this->getAttribute((isset($context["phenomene"]) ? $context["phenomene"] : null), "lib", array()), "html", null, true);
            echo "</h4>
              </div>
              <div class=\"modal-body\" style=\"color: #000000;\">

                    <form  method=\"post\" action=\"";
            // line 183
            echo $this->env->getExtension('routing')->getPath("cicrtg_backend_pheno_post");
            echo "\">
                        <span class=\"help-block\">
                            
                        </span>
                            
                           <div class=\"form-group\">
                             <div class=\"row\">
                                 <div class=\"col-md-3\">
                                     <label for=\"groupName\" >  :</label>
                                 </div>
\t\t\t\t\t\t\t\t\t\t
                                   <small class=\"text-danger\">  </small>
                                   <div class=\"col-md-7 \">
\t\t\t\t\t\t\t\t\t\t\t
                                    </div>
                               </div>
                           </div>
\t\t\t\t\t\t\t
\t\t\t\t\t\t\t

                          <div class=\"modal-footer\">
                                
                                <input type=\"submit\" class=\"btn btn-primary\" />
                               &nbsp; <button class=\"btn btn-default pull-right\" data-dismiss=\"modal\">Fermer !</button>
                          </div>
                        
                    </form>

                       
              </div>

          </div>
      </div>
    </div>

<!--================================================================================================================-->
             ";
        }
        // line 220
        echo "
            </table>                 
            
        </div>
        <div class=\"text-right\">
            <a href=\"#\"><i class=\"fa fa-arrow-circle-right\"></i></a>
        </div>
    </div>
</div>
</div>                           

</div>

<div class=\"col-lg-12\">
<div class=\"panel panel-default\">
    <div class=\"panel-heading\">
        <h3 class=\"panel-title\"><i class=\"fa fa-bar-chart-o fa-fw\"></i> Graphique</h3>
    </div>
    <div class=\"panel-body\">
        <!--<div id=\"morris-area-chart\"></div>-->
        <div id=\"morris-bar-chart\"></div>
    </div>
    
</div>
</div>


<!-- /.row -->

<!--<div class=\"row\">
<div class=\"col-lg-4\">
<div class=\"panel panel-default\">
    <div class=\"panel-heading\">
        <h3 class=\"panel-title\"><i class=\"fa fa-long-arrow-right fa-fw\"></i> Donut Chart</h3>
    </div>
    <div class=\"panel-body\">
        <div id=\"morris-donut-chart\"></div>
        <div class=\"text-right\">
            <a href=\"#\">View Details <i class=\"fa fa-arrow-circle-right\"></i></a>
        </div>
    </div>
</div>
</div>
<div class=\"col-lg-4\">
<div class=\"panel panel-default\">
    <div class=\"panel-heading\">
        <h3 class=\"panel-title\"><i class=\"fa fa-clock-o fa-fw\"></i> Tasks Panel</h3>
    </div>
    <div class=\"panel-body\">
        <div class=\"list-group\">
            <a href=\"#\" class=\"list-group-item\">
                <span class=\"badge\">just now</span>
                <i class=\"fa fa-fw fa-calendar\"></i> Calendar updated
            </a>
            <a href=\"#\" class=\"list-group-item\">
                <span class=\"badge\">4 minutes ago</span>
                <i class=\"fa fa-fw fa-comment\"></i> Commented on a post
            </a>
            <a href=\"#\" class=\"list-group-item\">
                <span class=\"badge\">23 minutes ago</span>
                <i class=\"fa fa-fw fa-truck\"></i> Order 392 shipped
            </a>
            <a href=\"#\" class=\"list-group-item\">
                <span class=\"badge\">46 minutes ago</span>
                <i class=\"fa fa-fw fa-money\"></i> Invoice 653 has been paid
            </a>
            <a href=\"#\" class=\"list-group-item\">
                <span class=\"badge\">1 hour ago</span>
                <i class=\"fa fa-fw fa-user\"></i> A new user has been added
            </a>
            <a href=\"#\" class=\"list-group-item\">
                <span class=\"badge\">2 hours ago</span>
                <i class=\"fa fa-fw fa-check\"></i> Completed task: \"pick up dry cleaning\"
            </a>
            <a href=\"#\" class=\"list-group-item\">
                <span class=\"badge\">yesterday</span>
                <i class=\"fa fa-fw fa-globe\"></i> Saved the world
            </a>
            <a href=\"#\" class=\"list-group-item\">
                <span class=\"badge\">two days ago</span>
                <i class=\"fa fa-fw fa-check\"></i> Completed task: \"fix error on sales page\"
            </a>
        </div>
        <div class=\"text-right\">
            <a href=\"#\">View All Activity <i class=\"fa fa-arrow-circle-right\"></i></a>
        </div>
    </div>
</div>
</div>
<div class=\"col-lg-4\">
<div class=\"panel panel-default\">
    <div class=\"panel-heading\">
        <h3 class=\"panel-title\"><i class=\"fa fa-money fa-fw\"></i> Transactions Panel</h3>
    </div>
    <div class=\"panel-body\">
        <div class=\"table-responsive\">
            <table class=\"table table-bordered table-hover table-striped\">
                <thead>
                    <tr>
                        <th>Order #</th>
                        <th>Order Date</th>
                        <th>Order Time</th>
                        <th>Amount (USD)</th>
                    </tr>
                </thead>
                <tbody>
                    <tr>
                        <td>3326</td>
                        <td>10/21/2013</td>
                        <td>3:29 PM</td>
                        <td>\$321.33</td>
                    </tr>
                    <tr>
                        <td>3325</td>
                        <td>10/21/2013</td>
                        <td>3:20 PM</td>
                        <td>\$234.34</td>
                    </tr>
                    <tr>
                        <td>3324</td>
                        <td>10/21/2013</td>
                        <td>3:03 PM</td>
                        <td>\$724.17</td>
                    </tr>
                    <tr>
                        <td>3323</td>
                        <td>10/21/2013</td>
                        <td>3:00 PM</td>
                        <td>\$23.71</td>
                    </tr>
                    <tr>
                        <td>3322</td>
                        <td>10/21/2013</td>
                        <td>2:49 PM</td>
                        <td>\$8345.23</td>
                    </tr>
                    <tr>
                        <td>3321</td>
                        <td>10/21/2013</td>
                        <td>2:23 PM</td>
                        <td>\$245.12</td>
                    </tr>
                    <tr>
                        <td>3320</td>
                        <td>10/21/2013</td>
                        <td>2:15 PM</td>
                        <td>\$5663.54</td>
                    </tr>
                    <tr>
                        <td>3319</td>
                        <td>10/21/2013</td>
                        <td>2:13 PM</td>
                        <td>\$943.45</td>
                    </tr>
                </tbody>
            </table>
        </div>
        <div class=\"text-right\">
            <a href=\"#\">View All Transactions <i class=\"fa fa-arrow-circle-right\"></i></a>
        </div>
    </div>
</div>
</div>
</div>-->
                        <!-- /.row -->";
    }

    public function getTemplateName()
    {
        return "cicrtgBackendBundle:includes:donneesBody.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  334 => 220,  294 => 183,  287 => 179,  277 => 171,  265 => 165,  261 => 164,  255 => 161,  251 => 160,  247 => 158,  243 => 157,  231 => 147,  229 => 146,  226 => 145,  187 => 109,  180 => 105,  169 => 96,  159 => 92,  155 => 91,  151 => 90,  144 => 88,  133 => 79,  131 => 78,  128 => 77,  90 => 42,  83 => 38,  75 => 32,  65 => 28,  61 => 27,  57 => 26,  50 => 24,  40 => 16,  38 => 15,  27 => 7,  19 => 1,);
    }
}
/* */
/* */
/* <div class="row">*/
/* <div class="col-lg-12">*/
/* <div class="panel panel-default">*/
/*     <div class="panel-heading">*/
/*         <h3 class="panel-title"><i class="fa fa-money fa-fw"></i> {{ phenomene.lib }}*/
/*         */
/*         </h3>*/
/*         */
/*     </div>*/
/*     <div class="panel-body">*/
/*         <div class="">*/
/*            <table id="datatable" class="table table-bordered table-hover table-striped">*/
/*             {% if phenomene.id == 1   %}*/
/*                 <thead>*/
/*                     <tr>*/
/*                         <th>Localite</th>*/
/*                         <th>Date</th>*/
/*                         <th>Releve</th>*/
/*                     </tr>*/
/*                 </thead>*/
/*                 <tbody>*/
/*                     {% for donnee in donnees %}  */
/*                            <tr>*/
/*                                <td>{{ donnee.agent.localites[0].libL }}</td>*/
/*                                <td>{{ donnee.date|localizeddate('medium', 'none', 'fr_FR') }}</td>*/
/*                                <td>{{ donnee.notation }}</td>*/
/*                                */
/*                            </tr>*/
/*                            {% endfor %}*/
/*                 </tbody>*/
/* <div class="modal fade" id="phenomene">*/
/*       <div class="modal-dialog">*/
/*           <div class="modal-content">*/
/*               <div class="modal-header" style="color: #000000;">*/
/*                   <button type="button" class="close" data-dismiss="modal">x</button>*/
/*                   <h4 class="modal-title">Ajout de {{ phenomene.lib }}</h4>*/
/*               </div>*/
/*               <div class="modal-body" style="color: #000000;">*/
/* */
/*                     <form  method="post" action="{{ path('cicrtg_backend_pheno_post') }}">*/
/*                         <span class="help-block">*/
/*                             */
/*                         </span>*/
/*                             */
/*                            <div class="form-group">*/
/*                              <div class="row">*/
/*                                  <div class="col-md-3">*/
/*                                      <label for="groupName" >  :</label>*/
/*                                  </div>*/
/* 										*/
/*                                    <small class="text-danger">  </small>*/
/*                                    <div class="col-md-7 ">*/
/* 											*/
/*                                     </div>*/
/*                                </div>*/
/*                            </div>*/
/* 							*/
/* 							*/
/* */
/*                           <div class="modal-footer">*/
/*                                 */
/*                                 <input type="submit" class="btn btn-primary" />*/
/*                                &nbsp; <button class="btn btn-default pull-right" data-dismiss="modal">Fermer !</button>*/
/*                           </div>*/
/*                         */
/*                     </form>*/
/* */
/*                        */
/*               </div>*/
/* */
/*           </div>*/
/*       </div>*/
/*     </div>*/
/*             {% endif %}*/
/*               */
/*               {% if phenomene.id == 2   %}*/
/*                 <thead>*/
/*                     <tr>*/
/*                         <th>Localite</th>*/
/*                         <th>Date</th>*/
/*                         <th>hauteur de pluie</th>*/
/*                     </tr>*/
/*                 </thead>*/
/*                 <tbody>*/
/*                   */
/*                   {% for donnee in donnees %}  */
/*                            <tr>*/
/*                                <td>{{ donnee.agent.localites[0].libL }}</td>*/
/*                                <td>{{ donnee.date|localizeddate('medium', 'none', 'fr_FR') }}</td>*/
/*                                <td>{{ donnee.notation }}</td>*/
/*                                */
/*                            </tr>*/
/*                            {% endfor %}*/
/*                 </tbody>*/
/* */
/* <!--========================    forlulaire pour l'ajout de de climatographie =================================           -->*/
/* */
/* <div class="modal fade" id="phenomene">*/
/*       <div class="modal-dialog">*/
/*           <div class="modal-content">*/
/*               <div class="modal-header" style="color: #000000;">*/
/*                   <button type="button" class="close" data-dismiss="modal">x</button>*/
/*                   <h4 class="modal-title">Ajout de {{ phenomene.lib }}</h4>*/
/*               </div>*/
/*               <div class="modal-body" style="color: #000000;">*/
/* */
/*                     <form  method="post" action="{{ path('cicrtg_backend_pheno_post') }}">*/
/*                         <span class="help-block">*/
/*                             */
/*                         </span>*/
/*                             */
/*                            <div class="form-group">*/
/*                              <div class="row">*/
/*                                  <div class="col-md-3">*/
/*                                      <label for="groupName" >  :</label>*/
/*                                  </div>*/
/* 										*/
/*                                    <small class="text-danger">  </small>*/
/*                                    <div class="col-md-7 ">*/
/* 											*/
/*                                     </div>*/
/*                                </div>*/
/*                            </div>*/
/* 							*/
/* 							*/
/* */
/*                           <div class="modal-footer">*/
/*                                 */
/*                                 <input type="submit" class="btn btn-primary" />*/
/*                                &nbsp; <button class="btn btn-default pull-right" data-dismiss="modal">Fermer !</button>*/
/*                           </div>*/
/*                         */
/*                     </form>*/
/* */
/*                        */
/*               </div>*/
/* */
/*           </div>*/
/*       </div>*/
/*     </div>*/
/* <!--================================================================================================================-->*/
/*               {% endif %}*/
/* */
/*               {% if phenomene.id == 3   %}*/
/*                 <thead>*/
/*                     <tr>*/
/*                         <th>Localite</th>*/
/*                         <th>Date</th>*/
/*                         <th>donnees</th>*/
/*                         <th>Type</th>*/
/*                     </tr>*/
/*                 </thead>*/
/*                 <tbody>*/
/*                            */
/*                          {% for donnee in donnees %}*/
/*                          */
/*                            <tr>*/
/*                                <td>{{ donnee.agent.localites[0].libL }}</td>*/
/*                                <td>{{ donnee.date|localizeddate('medium', 'none', 'fr_FR') ~'  '~ donnee.date|date('m') }}*/
/*                                                       */
/*                                </td>*/
/*                                <td>{{ donnee.notation }}</td>*/
/*                                <td>{{ donnee.type.libT }}*/
/*                                </td>*/
/*                                */
/*                            </tr>*/
/*                                 */
/*                            {% endfor %}*/
/*                 </tbody>*/
/* <!--========================    forlulaire pour l'ajout de de climatographie =================================           -->*/
/* */
/* <div class="modal fade" id="phenomene">*/
/*       <div class="modal-dialog">*/
/*           <div class="modal-content">*/
/*               <div class="modal-header" style="color: #000000;">*/
/*                   <button type="button" class="close" data-dismiss="modal">x</button>*/
/*                   <h4 class="modal-title">Ajout de {{ phenomene.lib }}</h4>*/
/*               </div>*/
/*               <div class="modal-body" style="color: #000000;">*/
/* */
/*                     <form  method="post" action="{{ path('cicrtg_backend_pheno_post') }}">*/
/*                         <span class="help-block">*/
/*                             */
/*                         </span>*/
/*                             */
/*                            <div class="form-group">*/
/*                              <div class="row">*/
/*                                  <div class="col-md-3">*/
/*                                      <label for="groupName" >  :</label>*/
/*                                  </div>*/
/* 										*/
/*                                    <small class="text-danger">  </small>*/
/*                                    <div class="col-md-7 ">*/
/* 											*/
/*                                     </div>*/
/*                                </div>*/
/*                            </div>*/
/* 							*/
/* 							*/
/* */
/*                           <div class="modal-footer">*/
/*                                 */
/*                                 <input type="submit" class="btn btn-primary" />*/
/*                                &nbsp; <button class="btn btn-default pull-right" data-dismiss="modal">Fermer !</button>*/
/*                           </div>*/
/*                         */
/*                     </form>*/
/* */
/*                        */
/*               </div>*/
/* */
/*           </div>*/
/*       </div>*/
/*     </div>*/
/* */
/* <!--================================================================================================================-->*/
/*              {% endif %}*/
/* */
/*             </table>                 */
/*             */
/*         </div>*/
/*         <div class="text-right">*/
/*             <a href="#"><i class="fa fa-arrow-circle-right"></i></a>*/
/*         </div>*/
/*     </div>*/
/* </div>*/
/* </div>                           */
/* */
/* </div>*/
/* */
/* <div class="col-lg-12">*/
/* <div class="panel panel-default">*/
/*     <div class="panel-heading">*/
/*         <h3 class="panel-title"><i class="fa fa-bar-chart-o fa-fw"></i> Graphique</h3>*/
/*     </div>*/
/*     <div class="panel-body">*/
/*         <!--<div id="morris-area-chart"></div>-->*/
/*         <div id="morris-bar-chart"></div>*/
/*     </div>*/
/*     */
/* </div>*/
/* </div>*/
/* */
/* */
/* <!-- /.row -->*/
/* */
/* <!--<div class="row">*/
/* <div class="col-lg-4">*/
/* <div class="panel panel-default">*/
/*     <div class="panel-heading">*/
/*         <h3 class="panel-title"><i class="fa fa-long-arrow-right fa-fw"></i> Donut Chart</h3>*/
/*     </div>*/
/*     <div class="panel-body">*/
/*         <div id="morris-donut-chart"></div>*/
/*         <div class="text-right">*/
/*             <a href="#">View Details <i class="fa fa-arrow-circle-right"></i></a>*/
/*         </div>*/
/*     </div>*/
/* </div>*/
/* </div>*/
/* <div class="col-lg-4">*/
/* <div class="panel panel-default">*/
/*     <div class="panel-heading">*/
/*         <h3 class="panel-title"><i class="fa fa-clock-o fa-fw"></i> Tasks Panel</h3>*/
/*     </div>*/
/*     <div class="panel-body">*/
/*         <div class="list-group">*/
/*             <a href="#" class="list-group-item">*/
/*                 <span class="badge">just now</span>*/
/*                 <i class="fa fa-fw fa-calendar"></i> Calendar updated*/
/*             </a>*/
/*             <a href="#" class="list-group-item">*/
/*                 <span class="badge">4 minutes ago</span>*/
/*                 <i class="fa fa-fw fa-comment"></i> Commented on a post*/
/*             </a>*/
/*             <a href="#" class="list-group-item">*/
/*                 <span class="badge">23 minutes ago</span>*/
/*                 <i class="fa fa-fw fa-truck"></i> Order 392 shipped*/
/*             </a>*/
/*             <a href="#" class="list-group-item">*/
/*                 <span class="badge">46 minutes ago</span>*/
/*                 <i class="fa fa-fw fa-money"></i> Invoice 653 has been paid*/
/*             </a>*/
/*             <a href="#" class="list-group-item">*/
/*                 <span class="badge">1 hour ago</span>*/
/*                 <i class="fa fa-fw fa-user"></i> A new user has been added*/
/*             </a>*/
/*             <a href="#" class="list-group-item">*/
/*                 <span class="badge">2 hours ago</span>*/
/*                 <i class="fa fa-fw fa-check"></i> Completed task: "pick up dry cleaning"*/
/*             </a>*/
/*             <a href="#" class="list-group-item">*/
/*                 <span class="badge">yesterday</span>*/
/*                 <i class="fa fa-fw fa-globe"></i> Saved the world*/
/*             </a>*/
/*             <a href="#" class="list-group-item">*/
/*                 <span class="badge">two days ago</span>*/
/*                 <i class="fa fa-fw fa-check"></i> Completed task: "fix error on sales page"*/
/*             </a>*/
/*         </div>*/
/*         <div class="text-right">*/
/*             <a href="#">View All Activity <i class="fa fa-arrow-circle-right"></i></a>*/
/*         </div>*/
/*     </div>*/
/* </div>*/
/* </div>*/
/* <div class="col-lg-4">*/
/* <div class="panel panel-default">*/
/*     <div class="panel-heading">*/
/*         <h3 class="panel-title"><i class="fa fa-money fa-fw"></i> Transactions Panel</h3>*/
/*     </div>*/
/*     <div class="panel-body">*/
/*         <div class="table-responsive">*/
/*             <table class="table table-bordered table-hover table-striped">*/
/*                 <thead>*/
/*                     <tr>*/
/*                         <th>Order #</th>*/
/*                         <th>Order Date</th>*/
/*                         <th>Order Time</th>*/
/*                         <th>Amount (USD)</th>*/
/*                     </tr>*/
/*                 </thead>*/
/*                 <tbody>*/
/*                     <tr>*/
/*                         <td>3326</td>*/
/*                         <td>10/21/2013</td>*/
/*                         <td>3:29 PM</td>*/
/*                         <td>$321.33</td>*/
/*                     </tr>*/
/*                     <tr>*/
/*                         <td>3325</td>*/
/*                         <td>10/21/2013</td>*/
/*                         <td>3:20 PM</td>*/
/*                         <td>$234.34</td>*/
/*                     </tr>*/
/*                     <tr>*/
/*                         <td>3324</td>*/
/*                         <td>10/21/2013</td>*/
/*                         <td>3:03 PM</td>*/
/*                         <td>$724.17</td>*/
/*                     </tr>*/
/*                     <tr>*/
/*                         <td>3323</td>*/
/*                         <td>10/21/2013</td>*/
/*                         <td>3:00 PM</td>*/
/*                         <td>$23.71</td>*/
/*                     </tr>*/
/*                     <tr>*/
/*                         <td>3322</td>*/
/*                         <td>10/21/2013</td>*/
/*                         <td>2:49 PM</td>*/
/*                         <td>$8345.23</td>*/
/*                     </tr>*/
/*                     <tr>*/
/*                         <td>3321</td>*/
/*                         <td>10/21/2013</td>*/
/*                         <td>2:23 PM</td>*/
/*                         <td>$245.12</td>*/
/*                     </tr>*/
/*                     <tr>*/
/*                         <td>3320</td>*/
/*                         <td>10/21/2013</td>*/
/*                         <td>2:15 PM</td>*/
/*                         <td>$5663.54</td>*/
/*                     </tr>*/
/*                     <tr>*/
/*                         <td>3319</td>*/
/*                         <td>10/21/2013</td>*/
/*                         <td>2:13 PM</td>*/
/*                         <td>$943.45</td>*/
/*                     </tr>*/
/*                 </tbody>*/
/*             </table>*/
/*         </div>*/
/*         <div class="text-right">*/
/*             <a href="#">View All Transactions <i class="fa fa-arrow-circle-right"></i></a>*/
/*         </div>*/
/*     </div>*/
/* </div>*/
/* </div>*/
/* </div>-->*/
/*                         <!-- /.row -->*/

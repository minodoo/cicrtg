<?php

/* cicrtgBackendBundle::layout.html.twig */
class __TwigTemplate_5324fedfa41a6dab5e90675f2cb8f42712527e894a238e971163d3c9987a2aa3 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        // line 1
        $this->parent = $this->loadTemplate("::layout.html.twig", "cicrtgBackendBundle::layout.html.twig", 1);
        $this->blocks = array(
            'body_general' => array($this, 'block_body_general'),
            'fileDarian' => array($this, 'block_fileDarian'),
            'body_bundle' => array($this, 'block_body_bundle'),
        );
    }

    protected function doGetParent(array $context)
    {
        return "::layout.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $this->parent->display($context, array_merge($this->blocks, $blocks));
    }

    // line 4
    public function block_body_general($context, array $blocks = array())
    {
        // line 5
        echo "                    ";
        $this->displayParentBlock("body_general", $context, $blocks);
        echo "
<div id=\"page-wrapper\" style=\"margin-top: -20px;\">

                    <div class=\"container-fluid\">

                        <!-- Page Heading -->
                        <div class=\"row\">
                            <div class=\"col-lg-12\">
                                
                                <ol class=\"breadcrumb\">
                                    <li class=\"active\">
                                        <i class=\"fa fa-dashboard\"></i> ";
        // line 16
        $this->displayBlock('fileDarian', $context, $blocks);
        // line 17
        echo "                                    </li>
                                </ol>
                            </div>
                        </div>
                        <!-- /.row -->

                        <!--<div class=\"row\">
                            <div class=\"col-lg-12\">
                                <div class=\"alert alert-info alert-dismissable\">
                                    <button type=\"button\" class=\"close\" data-dismiss=\"alert\" aria-hidden=\"true\">&times;</button>
                                    <i class=\"fa fa-info-circle\"></i>  <strong>Like SB Admin?</strong> Try out <a href=\"http://startbootstrap.com/template-overviews/sb-admin-2\" class=\"alert-link\">SB Admin 2</a> for additional features!
                                </div>
                            </div>
                        </div>-->
                        <!-- /.row -->

                        ";
        // line 33
        $this->displayBlock('body_bundle', $context, $blocks);
        // line 36
        echo "
                    </div>
                    <!-- /.container-fluid -->

                </div>
";
    }

    // line 16
    public function block_fileDarian($context, array $blocks = array())
    {
        echo " Dashboard / ";
    }

    // line 33
    public function block_body_bundle($context, array $blocks = array())
    {
        // line 34
        echo "                            
                        ";
    }

    public function getTemplateName()
    {
        return "cicrtgBackendBundle::layout.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  88 => 34,  85 => 33,  79 => 16,  70 => 36,  68 => 33,  50 => 17,  48 => 16,  33 => 5,  30 => 4,  11 => 1,);
    }
}
/* {% extends "::layout.html.twig" %}*/
/* */
/* */
/* {% block body_general%}*/
/*                     {{ parent() }}*/
/* <div id="page-wrapper" style="margin-top: -20px;">*/
/* */
/*                     <div class="container-fluid">*/
/* */
/*                         <!-- Page Heading -->*/
/*                         <div class="row">*/
/*                             <div class="col-lg-12">*/
/*                                 */
/*                                 <ol class="breadcrumb">*/
/*                                     <li class="active">*/
/*                                         <i class="fa fa-dashboard"></i> {% block fileDarian %} Dashboard / {% endblock %}*/
/*                                     </li>*/
/*                                 </ol>*/
/*                             </div>*/
/*                         </div>*/
/*                         <!-- /.row -->*/
/* */
/*                         <!--<div class="row">*/
/*                             <div class="col-lg-12">*/
/*                                 <div class="alert alert-info alert-dismissable">*/
/*                                     <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>*/
/*                                     <i class="fa fa-info-circle"></i>  <strong>Like SB Admin?</strong> Try out <a href="http://startbootstrap.com/template-overviews/sb-admin-2" class="alert-link">SB Admin 2</a> for additional features!*/
/*                                 </div>*/
/*                             </div>*/
/*                         </div>-->*/
/*                         <!-- /.row -->*/
/* */
/*                         {% block body_bundle %}*/
/*                             */
/*                         {% endblock %}*/
/* */
/*                     </div>*/
/*                     <!-- /.container-fluid -->*/
/* */
/*                 </div>*/
/* {% endblock %}*/

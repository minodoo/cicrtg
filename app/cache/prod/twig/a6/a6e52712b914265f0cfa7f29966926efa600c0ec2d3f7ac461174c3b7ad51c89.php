<?php

/* cicrtgBackendBundle:backend:index.html.twig */
class __TwigTemplate_ee317809c24274600057db913783dbdaef66f5c80d85cfd03793f83afefb484d extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        // line 1
        $this->parent = $this->loadTemplate("cicrtgBackendBundle::layout.html.twig", "cicrtgBackendBundle:backend:index.html.twig", 1);
        $this->blocks = array(
            'title' => array($this, 'block_title'),
            'fileDarian' => array($this, 'block_fileDarian'),
            'body_bundle' => array($this, 'block_body_bundle'),
        );
    }

    protected function doGetParent(array $context)
    {
        return "cicrtgBackendBundle::layout.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $this->parent->display($context, array_merge($this->blocks, $blocks));
    }

    // line 3
    public function block_title($context, array $blocks = array())
    {
        echo " ";
        $this->displayParentBlock("title", $context, $blocks);
        echo " Acceuil ";
    }

    // line 4
    public function block_fileDarian($context, array $blocks = array())
    {
        echo " ";
        $this->displayParentBlock("fileDarian", $context, $blocks);
        echo " ";
    }

    // line 6
    public function block_body_bundle($context, array $blocks = array())
    {
        // line 7
        echo "           ";
        $this->loadTemplate("cicrtgBackendBundle:includes:indexBody.html.twig", "cicrtgBackendBundle:backend:index.html.twig", 7)->display($context);
    }

    public function getTemplateName()
    {
        return "cicrtgBackendBundle:backend:index.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  49 => 7,  46 => 6,  38 => 4,  30 => 3,  11 => 1,);
    }
}
/* {% extends "cicrtgBackendBundle::layout.html.twig" %}*/
/* */
/* {% block title %} {{ parent() }} Acceuil {% endblock %}*/
/* {% block fileDarian %} {{ parent() }} {% endblock %}*/
/* */
/* {% block body_bundle %}*/
/*            {% include "cicrtgBackendBundle:includes:indexBody.html.twig" %}*/
/* {% endblock %}*/
/* */
/* */

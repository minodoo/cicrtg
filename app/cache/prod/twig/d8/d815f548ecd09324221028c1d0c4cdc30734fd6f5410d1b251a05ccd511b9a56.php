<?php

/* cicrtgBackendBundle:includes:typeDonneesBody.html.twig */
class __TwigTemplate_3616838fc11e46502359d698772f6271614c51443beae95e3106bb3fd9d1a223 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        // line 1
        echo "﻿";
        $context['_parent'] = $context;
        $context['_seq'] = twig_ensure_traversable($this->getAttribute($this->getAttribute($this->getAttribute((isset($context["app"]) ? $context["app"] : null), "session", array()), "flashbag", array()), "get", array(0 => "notice"), "method"));
        foreach ($context['_seq'] as $context["_key"] => $context["message"]) {
            // line 2
            echo "\t\t\t\t
    <div class=\"alert alert-success\">
        <a class=\"close fade in\" data-dismiss=\"alert\">&times;</a>
        ";
            // line 5
            echo twig_escape_filter($this->env, $context["message"], "html", null, true);
            echo "
    </div>
";
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['message'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 8
        echo "

<div class=\"row\">
   <div class=\"col-sm-6\">
\t\t<div class=\"panel panel-default\">
\t\t\t<div class=\"panel-heading\">
\t\t\t\t<h2 class=\"panel-title\"> Liste des postes phénomenes
\t\t\t\t<button data-toggle=\"modal\" data-backdrop=\"false\"  href=\"#phenomene\" class=\"btn btn-info btn-sm pull-right\" style=\"margin-top: -5px;\">
\t\t\t\t\t<i class=\"fa fa-plus\"></i> Ajouter </a>
\t\t\t\t</button></h2>
\t\t\t</div>
\t\t\t<div class=\"panel-body\">
\t\t\t\t<table  class=\"zone hover table table-bordered table-striped table-condensed\" cellspacing=\"0\" width=\"100%\" >
\t\t\t\t\t<thead>
\t\t\t\t\t\t<tr>
\t\t\t\t\t\t\t
\t\t\t\t\t\t\t
\t\t\t\t\t\t\t<th>Libelle</th>
\t\t\t\t\t\t\t
\t\t\t\t\t\t\t";
        // line 27
        if ($this->env->getExtension('security')->isGranted("ROLE_GESTIONNAIRE")) {
            // line 28
            echo "\t\t\t\t\t\t\t\t<th>Action</th>
\t\t\t\t\t\t\t";
        }
        // line 30
        echo "\t\t\t\t\t\t</tr>
\t\t\t\t\t</thead>
\t\t\t\t 
\t\t\t\t\t<tfoot>
\t\t\t\t\t\t<tr>
\t\t\t\t\t\t\t
\t\t\t\t\t\t\t
\t\t\t\t\t\t</tr>
\t\t\t\t\t</tfoot>
\t\t\t\t 
\t\t\t\t\t<tbody>
\t\t\t\t\t\t
\t\t\t\t\t\t";
        // line 42
        $context['_parent'] = $context;
        $context['_seq'] = twig_ensure_traversable((isset($context["postPhenomes"]) ? $context["postPhenomes"] : null));
        foreach ($context['_seq'] as $context["_key"] => $context["postPhenome"]) {
            // line 43
            echo "\t\t\t\t\t\t<tr>
\t\t\t\t\t\t\t
\t\t\t\t\t\t\t<th>";
            // line 45
            echo twig_escape_filter($this->env, $this->getAttribute($context["postPhenome"], "lib", array()), "html", null, true);
            echo "</th>
\t\t\t\t\t\t\t
\t\t\t\t\t\t\t";
            // line 47
            if ($this->env->getExtension('security')->isGranted("ROLE_GESTIONNAIRE")) {
                // line 48
                echo "\t\t\t\t\t\t\t\t<td class=\"row\">
\t\t\t\t\t\t\t\t\t
\t\t\t\t\t\t\t\t\t<span class=\"col-xs-3 col-xs-offset-1\">
\t\t\t\t\t\t\t\t\t\t";
                // line 51
                if ($this->env->getExtension('security')->isGranted("ROLE_ADMIN")) {
                    // line 52
                    echo "\t\t\t\t\t\t\t\t\t\t\t<a href=\"";
                    echo twig_escape_filter($this->env, $this->env->getExtension('routing')->getPath("cicrtg_backend_pheno_delete", array("id" => $this->getAttribute($context["postPhenome"], "id", array()))), "html", null, true);
                    echo "\" target-data=\"#supp\" class=\"btn btn-xs btn-danger\">
\t\t\t\t\t\t\t\t\t\t\t\t<i class=\"fa fa-frown-o\"></i>Supprimer
\t\t\t\t\t\t\t\t\t\t\t</a>
\t\t\t\t\t\t\t\t\t\t";
                }
                // line 56
                echo "\t\t\t\t\t\t\t\t\t</span>
\t\t\t\t\t\t\t\t</td>
\t\t\t\t\t\t\t";
            }
            // line 59
            echo "\t\t\t\t\t\t</tr>
\t\t\t\t\t\t";
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['postPhenome'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 61
        echo "\t\t\t\t\t\t
\t\t\t\t\t</tbody>
\t\t\t\t</table>
\t\t\t</div>
\t\t</div>
   </div>


\t<div class=\"col-sm-6\">
\t\t<div class=\"panel panel-default\">
\t\t\t<div class=\"panel-heading\">
\t\t\t\t<h2 class=\"panel-title\"> Liste types de données 
\t\t\t\t<button data-toggle=\"modal\" data-backdrop=\"false\"  href=\"#type\" class=\"btn btn-info btn-sm pull-right\" style=\"margin-top: -5px;\">
\t\t\t\t\t<i class=\"fa fa-plus\"></i> Ajouter </a>
\t\t\t\t</button></h2>
\t\t\t</div>
\t\t\t<div class=\"panel-body\">
\t\t\t\t<table  class=\"zone hover table table-bordered table-striped table-condensed\" cellspacing=\"0\" width=\"100%\" >
\t\t\t\t\t<thead>
\t\t\t\t\t\t<tr>
\t\t\t\t\t\t\t<th>Libelle</th>
\t\t\t\t\t\t\t<th>Phénomenes</th>
\t\t\t\t\t\t\t
\t\t\t\t\t\t\t";
        // line 84
        if ($this->env->getExtension('security')->isGranted("ROLE_GESTIONNAIRE")) {
            // line 85
            echo "\t\t\t\t\t\t\t\t<th>Action</th>
\t\t\t\t\t\t\t";
        }
        // line 87
        echo "\t\t\t\t\t\t</tr>
\t\t\t\t\t</thead>
\t\t\t\t 
\t\t\t\t\t<tfoot>
\t\t\t\t\t\t<tr>
\t\t\t\t\t\t\t
\t\t\t\t\t\t\t
\t\t\t\t\t\t</tr>
\t\t\t\t\t</tfoot>
\t\t\t\t 
\t\t\t\t\t<tbody>
\t\t\t\t\t\t
\t\t\t\t\t\t";
        // line 99
        $context['_parent'] = $context;
        $context['_seq'] = twig_ensure_traversable((isset($context["types"]) ? $context["types"] : null));
        foreach ($context['_seq'] as $context["_key"] => $context["type"]) {
            // line 100
            echo "\t\t\t\t\t\t<tr>
\t\t\t\t\t\t\t<th>";
            // line 101
            echo twig_escape_filter($this->env, $this->getAttribute($context["type"], "libT", array()), "html", null, true);
            echo "</th>
\t\t\t\t\t\t\t<th>";
            // line 102
            echo twig_escape_filter($this->env, $this->getAttribute($this->getAttribute($context["type"], "postphenomene", array()), "lib", array()), "html", null, true);
            echo "</th>
\t\t\t\t\t\t\t
\t\t\t\t\t\t\t";
            // line 104
            if ($this->env->getExtension('security')->isGranted("ROLE_GESTIONNAIRE")) {
                // line 105
                echo "\t\t\t\t\t\t\t\t<td class=\"row\">
\t\t\t\t\t\t\t\t\t
\t\t\t\t\t\t\t\t\t<span class=\"col-xs-3 col-xs-offset-1\">
\t\t\t\t\t\t\t\t\t\t";
                // line 108
                if ($this->env->getExtension('security')->isGranted("ROLE_ADMIN")) {
                    // line 109
                    echo "\t\t\t\t\t\t\t\t\t\t\t<a href=\"";
                    echo twig_escape_filter($this->env, $this->env->getExtension('routing')->getPath("cicrtg_backend_type_delete", array("id" => $this->getAttribute($context["type"], "id", array()))), "html", null, true);
                    echo "\" target-data=\"#supp\" class=\"btn btn-xs btn-danger\">
\t\t\t\t\t\t\t\t\t\t\t\t<i class=\"fa fa-frown-o\"></i>Supprimer
\t\t\t\t\t\t\t\t\t\t\t</a>
\t\t\t\t\t\t\t\t\t\t";
                }
                // line 113
                echo "\t\t\t\t\t\t\t\t\t</span>
\t\t\t\t\t\t\t\t</td>
\t\t\t\t\t\t\t";
            }
            // line 116
            echo "\t\t\t\t\t\t</tr>
\t\t\t\t\t\t";
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['type'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 118
        echo "\t\t\t\t\t\t
\t\t\t\t\t</tbody>
\t\t\t\t</table>
\t\t\t</div>
\t\t</div>
   </div>
</div>
       
     
<div class=\"modal fade\" id=\"phenomene\">
      <div class=\"modal-dialog\">
          <div class=\"modal-content\">
              <div class=\"modal-header\" style=\"color: #000000;\">
                  <button type=\"button\" class=\"close\" data-dismiss=\"modal\">x</button>
                  <h4 class=\"modal-title\">Ajout d'un Poste phénome</h4>
              </div>
              <div class=\"modal-body\" style=\"color: #000000;\">

                    <form  method=\"post\" action=\"";
        // line 136
        echo $this->env->getExtension('routing')->getPath("cicrtg_backend_pheno_post");
        echo "\">
                        <span class=\"help-block\">
                            ";
        // line 138
        echo $this->env->getExtension('form')->renderer->searchAndRenderBlock((isset($context["formPheno"]) ? $context["formPheno"] : null), 'errors');
        echo "
                        </span>
                            
                           <div class=\"form-group\">
                             <div class=\"row\">
                                 <div class=\"col-md-3\">
                                     <label for=\"groupName\" > ";
        // line 144
        echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute((isset($context["formPheno"]) ? $context["formPheno"] : null), "lib", array()), 'label', array("label" => "Nom"));
        echo " :</label>
                                 </div>
\t\t\t\t\t\t\t\t\t\t
                                   <small class=\"text-danger\"> ";
        // line 147
        echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute((isset($context["formPheno"]) ? $context["formPheno"] : null), "lib", array()), 'errors');
        echo " </small>
                                   <div class=\"col-md-7 \">
\t\t\t\t\t\t\t\t\t\t\t";
        // line 149
        echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute((isset($context["formPheno"]) ? $context["formPheno"] : null), "lib", array()), 'widget', array("attr" => array("class" => "form-control", "Placeholder" => "Le nom de l agent")));
        echo "
                                    </div>
                               </div>
                           </div>
\t\t\t\t\t\t\t
\t\t\t\t\t\t\t

                          <div class=\"modal-footer\">
                                
                                <input type=\"submit\" class=\"btn btn-primary\" />
                               &nbsp; <button class=\"btn btn-default pull-right\" data-dismiss=\"modal\">Fermer !</button>
                          </div>
                        ";
        // line 161
        echo $this->env->getExtension('form')->renderer->searchAndRenderBlock((isset($context["formPheno"]) ? $context["formPheno"] : null), 'rest');
        echo "
                    </form>

                       
              </div>

          </div>
      </div>
    </div>



<div class=\"modal fade\" id=\"type\">
      <div class=\"modal-dialog\">
          <div class=\"modal-content\">
              <div class=\"modal-header\" style=\"color: #000000;\">
                  <button type=\"button\" class=\"close\" data-dismiss=\"modal\">x</button>
                  <h4 class=\"modal-title\">Ajout d'un type de donnée</h4>
              </div>
              <div class=\"modal-body\" style=\"color: #000000;\">

                    <form  method=\"post\" action=\"";
        // line 182
        echo $this->env->getExtension('routing')->getPath("cicrtg_backend_type_post");
        echo "\">
                        <span class=\"help-block\">
                            ";
        // line 184
        echo $this->env->getExtension('form')->renderer->searchAndRenderBlock((isset($context["formType"]) ? $context["formType"] : null), 'errors');
        echo "
                        </span>
                            
                           <div class=\"form-group\">
                             <div class=\"row\">
                                 <div class=\"col-md-3\">
                                     <label for=\"groupName\" > ";
        // line 190
        echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute((isset($context["formType"]) ? $context["formType"] : null), "libT", array()), 'label', array("label" => "Nom"));
        echo " :</label>
                                 </div>
\t\t\t\t\t\t\t\t\t\t
                                   <small class=\"text-danger\"> ";
        // line 193
        echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute((isset($context["formType"]) ? $context["formType"] : null), "libT", array()), 'errors');
        echo " </small>
                                   <div class=\"col-md-7 \">
\t\t\t\t\t\t\t\t\t\t\t";
        // line 195
        echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute((isset($context["formType"]) ? $context["formType"] : null), "libT", array()), 'widget', array("attr" => array("class" => "form-control", "Placeholder" => "Le type de donnée")));
        echo "
                                    </div>
                               </div>
                           </div>
\t\t\t\t\t\t   
\t\t\t\t\t\t   <div class=\"form-group\" >
\t\t\t\t\t\t\t\t<div class=\"row\">
                                 <div class=\"col-md-3\">
                                     <label for=\"groupName\" > ";
        // line 203
        echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute((isset($context["formType"]) ? $context["formType"] : null), "postphenomene", array()), 'label', array("label" => "phénomène"));
        echo " :</label>
                                 </div>
\t\t\t\t\t\t\t\t\t\t
                                   <small class=\"text-danger\"> ";
        // line 206
        echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute((isset($context["formType"]) ? $context["formType"] : null), "postphenomene", array()), 'errors');
        echo " </small>
                                   <div class=\"col-md-7 \">
\t\t\t\t\t\t\t\t\t\t\t";
        // line 208
        echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute((isset($context["formType"]) ? $context["formType"] : null), "postphenomene", array()), 'widget', array("attr" => array("class" => "form-control", "Placeholder" => "Le nom de l agent")));
        echo "
                                    </div>
                               </div>
\t\t\t\t\t\t   </div>
\t\t\t\t\t\t\t
\t\t\t\t\t\t\t

                          <div class=\"modal-footer\">
                                
                                <input type=\"submit\" class=\"btn btn-primary\" />
                               &nbsp; <button class=\"btn btn-default pull-right\" data-dismiss=\"modal\">Fermer !</button>
                          </div>
                        ";
        // line 220
        echo $this->env->getExtension('form')->renderer->searchAndRenderBlock((isset($context["formType"]) ? $context["formType"] : null), 'rest');
        echo "
                    </form>

                       
              </div>

          </div>
      </div>
    </div>";
    }

    public function getTemplateName()
    {
        return "cicrtgBackendBundle:includes:typeDonneesBody.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  357 => 220,  342 => 208,  337 => 206,  331 => 203,  320 => 195,  315 => 193,  309 => 190,  300 => 184,  295 => 182,  271 => 161,  256 => 149,  251 => 147,  245 => 144,  236 => 138,  231 => 136,  211 => 118,  204 => 116,  199 => 113,  191 => 109,  189 => 108,  184 => 105,  182 => 104,  177 => 102,  173 => 101,  170 => 100,  166 => 99,  152 => 87,  148 => 85,  146 => 84,  121 => 61,  114 => 59,  109 => 56,  101 => 52,  99 => 51,  94 => 48,  92 => 47,  87 => 45,  83 => 43,  79 => 42,  65 => 30,  61 => 28,  59 => 27,  38 => 8,  29 => 5,  24 => 2,  19 => 1,);
    }
}
/* ﻿{% for message in app.session.flashbag.get('notice') %}*/
/* 				*/
/*     <div class="alert alert-success">*/
/*         <a class="close fade in" data-dismiss="alert">&times;</a>*/
/*         {{ message }}*/
/*     </div>*/
/* {% endfor %}*/
/* */
/* */
/* <div class="row">*/
/*    <div class="col-sm-6">*/
/* 		<div class="panel panel-default">*/
/* 			<div class="panel-heading">*/
/* 				<h2 class="panel-title"> Liste des postes phénomenes*/
/* 				<button data-toggle="modal" data-backdrop="false"  href="#phenomene" class="btn btn-info btn-sm pull-right" style="margin-top: -5px;">*/
/* 					<i class="fa fa-plus"></i> Ajouter </a>*/
/* 				</button></h2>*/
/* 			</div>*/
/* 			<div class="panel-body">*/
/* 				<table  class="zone hover table table-bordered table-striped table-condensed" cellspacing="0" width="100%" >*/
/* 					<thead>*/
/* 						<tr>*/
/* 							*/
/* 							*/
/* 							<th>Libelle</th>*/
/* 							*/
/* 							{% if is_granted('ROLE_GESTIONNAIRE') %}*/
/* 								<th>Action</th>*/
/* 							{% endif %}*/
/* 						</tr>*/
/* 					</thead>*/
/* 				 */
/* 					<tfoot>*/
/* 						<tr>*/
/* 							*/
/* 							*/
/* 						</tr>*/
/* 					</tfoot>*/
/* 				 */
/* 					<tbody>*/
/* 						*/
/* 						{% for postPhenome in postPhenomes %}*/
/* 						<tr>*/
/* 							*/
/* 							<th>{{ postPhenome.lib }}</th>*/
/* 							*/
/* 							{% if is_granted('ROLE_GESTIONNAIRE') %}*/
/* 								<td class="row">*/
/* 									*/
/* 									<span class="col-xs-3 col-xs-offset-1">*/
/* 										{% if(is_granted('ROLE_ADMIN')) %}*/
/* 											<a href="{{ path('cicrtg_backend_pheno_delete', {'id': postPhenome.id}) }}" target-data="#supp" class="btn btn-xs btn-danger">*/
/* 												<i class="fa fa-frown-o"></i>Supprimer*/
/* 											</a>*/
/* 										{% endif %}*/
/* 									</span>*/
/* 								</td>*/
/* 							{% endif %}*/
/* 						</tr>*/
/* 						{% endfor %}*/
/* 						*/
/* 					</tbody>*/
/* 				</table>*/
/* 			</div>*/
/* 		</div>*/
/*    </div>*/
/* */
/* */
/* 	<div class="col-sm-6">*/
/* 		<div class="panel panel-default">*/
/* 			<div class="panel-heading">*/
/* 				<h2 class="panel-title"> Liste types de données */
/* 				<button data-toggle="modal" data-backdrop="false"  href="#type" class="btn btn-info btn-sm pull-right" style="margin-top: -5px;">*/
/* 					<i class="fa fa-plus"></i> Ajouter </a>*/
/* 				</button></h2>*/
/* 			</div>*/
/* 			<div class="panel-body">*/
/* 				<table  class="zone hover table table-bordered table-striped table-condensed" cellspacing="0" width="100%" >*/
/* 					<thead>*/
/* 						<tr>*/
/* 							<th>Libelle</th>*/
/* 							<th>Phénomenes</th>*/
/* 							*/
/* 							{% if is_granted('ROLE_GESTIONNAIRE') %}*/
/* 								<th>Action</th>*/
/* 							{% endif %}*/
/* 						</tr>*/
/* 					</thead>*/
/* 				 */
/* 					<tfoot>*/
/* 						<tr>*/
/* 							*/
/* 							*/
/* 						</tr>*/
/* 					</tfoot>*/
/* 				 */
/* 					<tbody>*/
/* 						*/
/* 						{% for type in types %}*/
/* 						<tr>*/
/* 							<th>{{ type.libT }}</th>*/
/* 							<th>{{ type.postphenomene.lib }}</th>*/
/* 							*/
/* 							{% if is_granted('ROLE_GESTIONNAIRE') %}*/
/* 								<td class="row">*/
/* 									*/
/* 									<span class="col-xs-3 col-xs-offset-1">*/
/* 										{% if(is_granted('ROLE_ADMIN')) %}*/
/* 											<a href="{{ path('cicrtg_backend_type_delete', {'id': type.id}) }}" target-data="#supp" class="btn btn-xs btn-danger">*/
/* 												<i class="fa fa-frown-o"></i>Supprimer*/
/* 											</a>*/
/* 										{% endif %}*/
/* 									</span>*/
/* 								</td>*/
/* 							{% endif %}*/
/* 						</tr>*/
/* 						{% endfor %}*/
/* 						*/
/* 					</tbody>*/
/* 				</table>*/
/* 			</div>*/
/* 		</div>*/
/*    </div>*/
/* </div>*/
/*        */
/*      */
/* <div class="modal fade" id="phenomene">*/
/*       <div class="modal-dialog">*/
/*           <div class="modal-content">*/
/*               <div class="modal-header" style="color: #000000;">*/
/*                   <button type="button" class="close" data-dismiss="modal">x</button>*/
/*                   <h4 class="modal-title">Ajout d'un Poste phénome</h4>*/
/*               </div>*/
/*               <div class="modal-body" style="color: #000000;">*/
/* */
/*                     <form  method="post" action="{{ path('cicrtg_backend_pheno_post') }}">*/
/*                         <span class="help-block">*/
/*                             {{ form_errors(formPheno) }}*/
/*                         </span>*/
/*                             */
/*                            <div class="form-group">*/
/*                              <div class="row">*/
/*                                  <div class="col-md-3">*/
/*                                      <label for="groupName" > {{ form_label(formPheno.lib, 'Nom') }} :</label>*/
/*                                  </div>*/
/* 										*/
/*                                    <small class="text-danger"> {{ form_errors(formPheno.lib) }} </small>*/
/*                                    <div class="col-md-7 ">*/
/* 											{{ form_widget(formPheno.lib,  {'attr': {'class':'form-control', 'Placeholder': 'Le nom de l agent'}}) }}*/
/*                                     </div>*/
/*                                </div>*/
/*                            </div>*/
/* 							*/
/* 							*/
/* */
/*                           <div class="modal-footer">*/
/*                                 */
/*                                 <input type="submit" class="btn btn-primary" />*/
/*                                &nbsp; <button class="btn btn-default pull-right" data-dismiss="modal">Fermer !</button>*/
/*                           </div>*/
/*                         {{ form_rest(formPheno) }}*/
/*                     </form>*/
/* */
/*                        */
/*               </div>*/
/* */
/*           </div>*/
/*       </div>*/
/*     </div>*/
/* */
/* */
/* */
/* <div class="modal fade" id="type">*/
/*       <div class="modal-dialog">*/
/*           <div class="modal-content">*/
/*               <div class="modal-header" style="color: #000000;">*/
/*                   <button type="button" class="close" data-dismiss="modal">x</button>*/
/*                   <h4 class="modal-title">Ajout d'un type de donnée</h4>*/
/*               </div>*/
/*               <div class="modal-body" style="color: #000000;">*/
/* */
/*                     <form  method="post" action="{{ path('cicrtg_backend_type_post') }}">*/
/*                         <span class="help-block">*/
/*                             {{ form_errors(formType) }}*/
/*                         </span>*/
/*                             */
/*                            <div class="form-group">*/
/*                              <div class="row">*/
/*                                  <div class="col-md-3">*/
/*                                      <label for="groupName" > {{ form_label(formType.libT, 'Nom') }} :</label>*/
/*                                  </div>*/
/* 										*/
/*                                    <small class="text-danger"> {{ form_errors(formType.libT) }} </small>*/
/*                                    <div class="col-md-7 ">*/
/* 											{{ form_widget(formType.libT,  {'attr': {'class':'form-control', 'Placeholder': 'Le type de donnée'}}) }}*/
/*                                     </div>*/
/*                                </div>*/
/*                            </div>*/
/* 						   */
/* 						   <div class="form-group" >*/
/* 								<div class="row">*/
/*                                  <div class="col-md-3">*/
/*                                      <label for="groupName" > {{ form_label(formType.postphenomene, 'phénomène') }} :</label>*/
/*                                  </div>*/
/* 										*/
/*                                    <small class="text-danger"> {{ form_errors(formType.postphenomene) }} </small>*/
/*                                    <div class="col-md-7 ">*/
/* 											{{ form_widget(formType.postphenomene,  {'attr': {'class':'form-control', 'Placeholder': 'Le nom de l agent'}}) }}*/
/*                                     </div>*/
/*                                </div>*/
/* 						   </div>*/
/* 							*/
/* 							*/
/* */
/*                           <div class="modal-footer">*/
/*                                 */
/*                                 <input type="submit" class="btn btn-primary" />*/
/*                                &nbsp; <button class="btn btn-default pull-right" data-dismiss="modal">Fermer !</button>*/
/*                           </div>*/
/*                         {{ form_rest(formType) }}*/
/*                     </form>*/
/* */
/*                        */
/*               </div>*/
/* */
/*           </div>*/
/*       </div>*/
/*     </div>*/

<?php

/* cicrtgBackendBundle:backend:donnees.html.twig */
class __TwigTemplate_0d0f7d5739c5f9bacffb0055461002e23d898ac5662a5c2440d0d6ba9c1b6930 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        // line 1
        $this->parent = $this->loadTemplate("cicrtgBackendBundle::layout.html.twig", "cicrtgBackendBundle:backend:donnees.html.twig", 1);
        $this->blocks = array(
            'title' => array($this, 'block_title'),
            'fileDarian' => array($this, 'block_fileDarian'),
            'body_bundle' => array($this, 'block_body_bundle'),
            'addFunctionDocument' => array($this, 'block_addFunctionDocument'),
        );
    }

    protected function doGetParent(array $context)
    {
        return "cicrtgBackendBundle::layout.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $this->parent->display($context, array_merge($this->blocks, $blocks));
    }

    // line 3
    public function block_title($context, array $blocks = array())
    {
        echo " ";
        $this->displayParentBlock("title", $context, $blocks);
        echo " Données ";
    }

    // line 4
    public function block_fileDarian($context, array $blocks = array())
    {
        echo " ";
        $this->displayParentBlock("fileDarian", $context, $blocks);
        echo " Donées";
    }

    // line 6
    public function block_body_bundle($context, array $blocks = array())
    {
        // line 7
        echo "           ";
        $this->loadTemplate("cicrtgBackendBundle:includes:donneesBody.html.twig", "cicrtgBackendBundle:backend:donnees.html.twig", 7)->display($context);
    }

    // line 11
    public function block_addFunctionDocument($context, array $blocks = array())
    {
        // line 12
        echo "    ";
        $this->displayParentBlock("addFunctionDocument", $context, $blocks);
        echo "
    // Area Chart
    <!--Morris.Area({-->
    <!--    element: 'morris-area-chart',-->
    <!--    data: [-->
    <!--    ";
        // line 17
        $context["nombre"] = 0;
        echo "-->
    <!--        ";
        // line 18
        $context['_parent'] = $context;
        $context['_seq'] = twig_ensure_traversable((isset($context["donnees"]) ? $context["donnees"] : null));
        foreach ($context['_seq'] as $context["_key"] => $context["donnee"]) {
            echo "-->
    <!--            {-->
    <!--                ";
            // line 20
            $context["nombre"] = ((isset($context["nombre"]) ? $context["nombre"] : null) + 1);
            echo "-->
    <!--                period: ";
            // line 21
            echo twig_escape_filter($this->env, $this->getAttribute($this->getAttribute($this->getAttribute($this->getAttribute($context["donnee"], "agent", array()), "localites", array()), 0, array(), "array"), "libL", array()), "html", null, true);
            echo " ,-->
    <!--                iphone: 2666-->
    <!--            },-->
    <!--        ";
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['donnee'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 24
        echo "-->
    <!--    ],-->
    <!--    xkey: 'period',-->
    <!--    ykeys: ['iphone'],-->
    <!--    labels: ['iPhone', 'iPad', 'iPod Touch'],-->
    <!--    pointSize: 2,-->
    <!--    hideHover: 'auto',-->
    <!--    resize: true-->
    <!--});-->

    // Donut Chart
    <!--Morris.Donut({-->
    <!--    element: 'morris-donut-chart',-->
    <!--    data: [{-->
    <!--        label: \"Download Sales\",-->
    <!--        value: 12-->
    <!--    }, {-->
    <!--        label: \"In-Store Sales\",-->
    <!--        value: 30-->
    <!--    }, {-->
    <!--        label: \"Mail-Order Sales\",-->
    <!--        value: 20-->
    <!--    }],-->
    <!--    resize: true-->
    <!--});-->
    
    
     // Bar Chart
     ";
        // line 52
        $context["donnee"] = "";
        // line 53
        echo "    Morris.Bar({
        element: 'morris-bar-chart',
        data: [
            ";
        // line 56
        $context['_parent'] = $context;
        $context['_seq'] = twig_ensure_traversable((isset($context["donneesCount"]) ? $context["donneesCount"] : null));
        foreach ($context['_seq'] as $context["_key"] => $context["donnee"]) {
            // line 57
            echo "                    {
                        Region: '";
            // line 58
            echo twig_escape_filter($this->env, $this->getAttribute($context["donnee"], "libR", array(), "array"), "html", null, true);
            echo "',
                        Moyenne: ";
            // line 59
            echo twig_escape_filter($this->env, $this->getAttribute($context["donnee"], "Moyenne", array(), "array"), "html", null, true);
            echo "
                    },
                    
            ";
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['donnee'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 63
        echo "        ],
        xkey: 'Region',
        ykeys: ['Moyenne'],
        labels: ['Moyenne'],
        barRatio: 0.8,
        xLabelAngle: 35,
        hideHover: 'auto',
        resize: true
    });
";
    }

    public function getTemplateName()
    {
        return "cicrtgBackendBundle:backend:donnees.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  150 => 63,  140 => 59,  136 => 58,  133 => 57,  129 => 56,  124 => 53,  122 => 52,  92 => 24,  82 => 21,  78 => 20,  71 => 18,  67 => 17,  58 => 12,  55 => 11,  50 => 7,  47 => 6,  39 => 4,  31 => 3,  11 => 1,);
    }
}
/* {% extends "cicrtgBackendBundle::layout.html.twig" %}*/
/* */
/* {% block title %} {{ parent() }} Données {% endblock %}*/
/* {% block fileDarian %} {{ parent() }} Donées{% endblock %}*/
/* */
/* {% block body_bundle %}*/
/*            {% include "cicrtgBackendBundle:includes:donneesBody.html.twig" %}*/
/* {% endblock %}*/
/* */
/* */
/* {% block addFunctionDocument %}*/
/*     {{ parent() }}*/
/*     // Area Chart*/
/*     <!--Morris.Area({-->*/
/*     <!--    element: 'morris-area-chart',-->*/
/*     <!--    data: [-->*/
/*     <!--    {%  set nombre = 0 %}-->*/
/*     <!--        {% for donnee in donnees %}-->*/
/*     <!--            {-->*/
/*     <!--                {%  set nombre = nombre + 1 %}-->*/
/*     <!--                period: {{ donnee.agent.localites[0].libL }} ,-->*/
/*     <!--                iphone: 2666-->*/
/*     <!--            },-->*/
/*     <!--        {% endfor %}-->*/
/*     <!--    ],-->*/
/*     <!--    xkey: 'period',-->*/
/*     <!--    ykeys: ['iphone'],-->*/
/*     <!--    labels: ['iPhone', 'iPad', 'iPod Touch'],-->*/
/*     <!--    pointSize: 2,-->*/
/*     <!--    hideHover: 'auto',-->*/
/*     <!--    resize: true-->*/
/*     <!--});-->*/
/* */
/*     // Donut Chart*/
/*     <!--Morris.Donut({-->*/
/*     <!--    element: 'morris-donut-chart',-->*/
/*     <!--    data: [{-->*/
/*     <!--        label: "Download Sales",-->*/
/*     <!--        value: 12-->*/
/*     <!--    }, {-->*/
/*     <!--        label: "In-Store Sales",-->*/
/*     <!--        value: 30-->*/
/*     <!--    }, {-->*/
/*     <!--        label: "Mail-Order Sales",-->*/
/*     <!--        value: 20-->*/
/*     <!--    }],-->*/
/*     <!--    resize: true-->*/
/*     <!--});-->*/
/*     */
/*     */
/*      // Bar Chart*/
/*      {% set donnee = '' %}*/
/*     Morris.Bar({*/
/*         element: 'morris-bar-chart',*/
/*         data: [*/
/*             {% for donnee in donneesCount %}*/
/*                     {*/
/*                         Region: '{{ donnee['libR'] }}',*/
/*                         Moyenne: {{ donnee['Moyenne'] }}*/
/*                     },*/
/*                     */
/*             {% endfor %}*/
/*         ],*/
/*         xkey: 'Region',*/
/*         ykeys: ['Moyenne'],*/
/*         labels: ['Moyenne'],*/
/*         barRatio: 0.8,*/
/*         xLabelAngle: 35,*/
/*         hideHover: 'auto',*/
/*         resize: true*/
/*     });*/
/* {% endblock %}*/
/* */

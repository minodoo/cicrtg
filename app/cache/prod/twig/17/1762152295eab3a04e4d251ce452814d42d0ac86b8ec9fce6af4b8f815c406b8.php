<?php

/* ::layout.html.twig */
class __TwigTemplate_def5e702d61eb444ecaa186062b1c5c3f71dde43d6ed9080eba008985109b5e2 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
            'title' => array($this, 'block_title'),
            'stylesheet' => array($this, 'block_stylesheet'),
            'body_general' => array($this, 'block_body_general'),
            'javascripts' => array($this, 'block_javascripts'),
            'addFunctionDocument' => array($this, 'block_addFunctionDocument'),
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        // line 1
        echo "﻿<!DOCTYPE html>
<html lang=\"en\">

<head>

    <meta charset=\"utf-8\">
    <meta http-equiv=\"X-UA-Compatible\" content=\"IE=edge\">
    <meta name=\"viewport\" content=\"width=device-width, initial-scale=1\">
    <meta name=\"description\" content=\"\">
    <meta name=\"author\" content=\"\">

    <title>";
        // line 12
        $this->displayBlock('title', $context, $blocks);
        echo "</title>

    ";
        // line 14
        $this->displayBlock('stylesheet', $context, $blocks);
        // line 41
        echo "        
    <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
        <script src=\"https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js\"></script>
        <script src=\"https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js\"></script>
    <![endif]-->

</head>

<body>
    
        <div id=\"wrapper\">

                <!-- Navigation -->
               
            ";
        // line 57
        $this->displayBlock('body_general', $context, $blocks);
        // line 62
        echo "        </div>

    
    
    <!-- /#wrapper -->

    <!-- jQuery -->
    ";
        // line 69
        $this->displayBlock('javascripts', $context, $blocks);
        // line 108
        echo "    <script>
    \$(document).ready(function()
    {
        ";
        // line 111
        $this->displayBlock('addFunctionDocument', $context, $blocks);
        // line 165
        echo "    } );
    </script>
</body>

</html>
";
    }

    // line 12
    public function block_title($context, array $blocks = array())
    {
        echo " Cicrtg | ";
    }

    // line 14
    public function block_stylesheet($context, array $blocks = array())
    {
        // line 15
        echo "        ";
        if (isset($context['assetic']['debug']) && $context['assetic']['debug']) {
            // asset "f7a88ed_0"
            $context["asset_url"] = isset($context['assetic']['use_controller']) && $context['assetic']['use_controller'] ? $this->env->getExtension('routing')->getPath("_assetic_f7a88ed_0") : $this->env->getExtension('assets')->getAssetUrl("css/f7a88ed_bootstrap.min_1.css");
            // line 26
            echo "                        
                        
                   
        <!-- Bootstrap Core CSS -->
        <link href=\"";
            // line 30
            echo twig_escape_filter($this->env, (isset($context["asset_url"]) ? $context["asset_url"] : null), "html", null, true);
            echo "\" rel=\"stylesheet\">
        
        <link href=\"https://cdn.datatables.net/1.10.9/css/jquery.dataTables.min.css\" rel=\"stylesheet\">
        <link href=\"https://cdn.datatables.net/buttons/1.0.3/css/buttons.dataTables.min.css\" rel=\"stylesheet\">
        
    
    

        ";
            // asset "f7a88ed_1"
            $context["asset_url"] = isset($context['assetic']['use_controller']) && $context['assetic']['use_controller'] ? $this->env->getExtension('routing')->getPath("_assetic_f7a88ed_1") : $this->env->getExtension('assets')->getAssetUrl("css/f7a88ed_sb-admin_2.css");
            // line 26
            echo "                        
                        
                   
        <!-- Bootstrap Core CSS -->
        <link href=\"";
            // line 30
            echo twig_escape_filter($this->env, (isset($context["asset_url"]) ? $context["asset_url"] : null), "html", null, true);
            echo "\" rel=\"stylesheet\">
        
        <link href=\"https://cdn.datatables.net/1.10.9/css/jquery.dataTables.min.css\" rel=\"stylesheet\">
        <link href=\"https://cdn.datatables.net/buttons/1.0.3/css/buttons.dataTables.min.css\" rel=\"stylesheet\">
        
    
    

        ";
            // asset "f7a88ed_2"
            $context["asset_url"] = isset($context['assetic']['use_controller']) && $context['assetic']['use_controller'] ? $this->env->getExtension('routing')->getPath("_assetic_f7a88ed_2") : $this->env->getExtension('assets')->getAssetUrl("css/f7a88ed_morris_3.css");
            // line 26
            echo "                        
                        
                   
        <!-- Bootstrap Core CSS -->
        <link href=\"";
            // line 30
            echo twig_escape_filter($this->env, (isset($context["asset_url"]) ? $context["asset_url"] : null), "html", null, true);
            echo "\" rel=\"stylesheet\">
        
        <link href=\"https://cdn.datatables.net/1.10.9/css/jquery.dataTables.min.css\" rel=\"stylesheet\">
        <link href=\"https://cdn.datatables.net/buttons/1.0.3/css/buttons.dataTables.min.css\" rel=\"stylesheet\">
        
    
    

        ";
            // asset "f7a88ed_3"
            $context["asset_url"] = isset($context['assetic']['use_controller']) && $context['assetic']['use_controller'] ? $this->env->getExtension('routing')->getPath("_assetic_f7a88ed_3") : $this->env->getExtension('assets')->getAssetUrl("css/f7a88ed_font-awesome.min_4.css");
            // line 26
            echo "                        
                        
                   
        <!-- Bootstrap Core CSS -->
        <link href=\"";
            // line 30
            echo twig_escape_filter($this->env, (isset($context["asset_url"]) ? $context["asset_url"] : null), "html", null, true);
            echo "\" rel=\"stylesheet\">
        
        <link href=\"https://cdn.datatables.net/1.10.9/css/jquery.dataTables.min.css\" rel=\"stylesheet\">
        <link href=\"https://cdn.datatables.net/buttons/1.0.3/css/buttons.dataTables.min.css\" rel=\"stylesheet\">
        
    
    

        ";
            // asset "f7a88ed_4"
            $context["asset_url"] = isset($context['assetic']['use_controller']) && $context['assetic']['use_controller'] ? $this->env->getExtension('routing')->getPath("_assetic_f7a88ed_4") : $this->env->getExtension('assets')->getAssetUrl("css/f7a88ed_jquery.dataTables.min_5.css");
            // line 26
            echo "                        
                        
                   
        <!-- Bootstrap Core CSS -->
        <link href=\"";
            // line 30
            echo twig_escape_filter($this->env, (isset($context["asset_url"]) ? $context["asset_url"] : null), "html", null, true);
            echo "\" rel=\"stylesheet\">
        
        <link href=\"https://cdn.datatables.net/1.10.9/css/jquery.dataTables.min.css\" rel=\"stylesheet\">
        <link href=\"https://cdn.datatables.net/buttons/1.0.3/css/buttons.dataTables.min.css\" rel=\"stylesheet\">
        
    
    

        ";
            // asset "f7a88ed_5"
            $context["asset_url"] = isset($context['assetic']['use_controller']) && $context['assetic']['use_controller'] ? $this->env->getExtension('routing')->getPath("_assetic_f7a88ed_5") : $this->env->getExtension('assets')->getAssetUrl("css/f7a88ed_dataTables.tableTools.min_6.css");
            // line 26
            echo "                        
                        
                   
        <!-- Bootstrap Core CSS -->
        <link href=\"";
            // line 30
            echo twig_escape_filter($this->env, (isset($context["asset_url"]) ? $context["asset_url"] : null), "html", null, true);
            echo "\" rel=\"stylesheet\">
        
        <link href=\"https://cdn.datatables.net/1.10.9/css/jquery.dataTables.min.css\" rel=\"stylesheet\">
        <link href=\"https://cdn.datatables.net/buttons/1.0.3/css/buttons.dataTables.min.css\" rel=\"stylesheet\">
        
    
    

        ";
            // asset "f7a88ed_6"
            $context["asset_url"] = isset($context['assetic']['use_controller']) && $context['assetic']['use_controller'] ? $this->env->getExtension('routing')->getPath("_assetic_f7a88ed_6") : $this->env->getExtension('assets')->getAssetUrl("css/f7a88ed_select.bootstrap.min_7.css");
            // line 26
            echo "                        
                        
                   
        <!-- Bootstrap Core CSS -->
        <link href=\"";
            // line 30
            echo twig_escape_filter($this->env, (isset($context["asset_url"]) ? $context["asset_url"] : null), "html", null, true);
            echo "\" rel=\"stylesheet\">
        
        <link href=\"https://cdn.datatables.net/1.10.9/css/jquery.dataTables.min.css\" rel=\"stylesheet\">
        <link href=\"https://cdn.datatables.net/buttons/1.0.3/css/buttons.dataTables.min.css\" rel=\"stylesheet\">
        
    
    

        ";
            // asset "f7a88ed_7"
            $context["asset_url"] = isset($context['assetic']['use_controller']) && $context['assetic']['use_controller'] ? $this->env->getExtension('routing')->getPath("_assetic_f7a88ed_7") : $this->env->getExtension('assets')->getAssetUrl("css/f7a88ed_select.dataTables.min_8.css");
            // line 26
            echo "                        
                        
                   
        <!-- Bootstrap Core CSS -->
        <link href=\"";
            // line 30
            echo twig_escape_filter($this->env, (isset($context["asset_url"]) ? $context["asset_url"] : null), "html", null, true);
            echo "\" rel=\"stylesheet\">
        
        <link href=\"https://cdn.datatables.net/1.10.9/css/jquery.dataTables.min.css\" rel=\"stylesheet\">
        <link href=\"https://cdn.datatables.net/buttons/1.0.3/css/buttons.dataTables.min.css\" rel=\"stylesheet\">
        
    
    

        ";
            // asset "f7a88ed_8"
            $context["asset_url"] = isset($context['assetic']['use_controller']) && $context['assetic']['use_controller'] ? $this->env->getExtension('routing')->getPath("_assetic_f7a88ed_8") : $this->env->getExtension('assets')->getAssetUrl("css/f7a88ed_select.foundation.min_9.css");
            // line 26
            echo "                        
                        
                   
        <!-- Bootstrap Core CSS -->
        <link href=\"";
            // line 30
            echo twig_escape_filter($this->env, (isset($context["asset_url"]) ? $context["asset_url"] : null), "html", null, true);
            echo "\" rel=\"stylesheet\">
        
        <link href=\"https://cdn.datatables.net/1.10.9/css/jquery.dataTables.min.css\" rel=\"stylesheet\">
        <link href=\"https://cdn.datatables.net/buttons/1.0.3/css/buttons.dataTables.min.css\" rel=\"stylesheet\">
        
    
    

        ";
            // asset "f7a88ed_9"
            $context["asset_url"] = isset($context['assetic']['use_controller']) && $context['assetic']['use_controller'] ? $this->env->getExtension('routing')->getPath("_assetic_f7a88ed_9") : $this->env->getExtension('assets')->getAssetUrl("css/f7a88ed_select.jqueryui.min_10.css");
            // line 26
            echo "                        
                        
                   
        <!-- Bootstrap Core CSS -->
        <link href=\"";
            // line 30
            echo twig_escape_filter($this->env, (isset($context["asset_url"]) ? $context["asset_url"] : null), "html", null, true);
            echo "\" rel=\"stylesheet\">
        
        <link href=\"https://cdn.datatables.net/1.10.9/css/jquery.dataTables.min.css\" rel=\"stylesheet\">
        <link href=\"https://cdn.datatables.net/buttons/1.0.3/css/buttons.dataTables.min.css\" rel=\"stylesheet\">
        
    
    

        ";
        } else {
            // asset "f7a88ed"
            $context["asset_url"] = isset($context['assetic']['use_controller']) && $context['assetic']['use_controller'] ? $this->env->getExtension('routing')->getPath("_assetic_f7a88ed") : $this->env->getExtension('assets')->getAssetUrl("css/f7a88ed.css");
            // line 26
            echo "                        
                        
                   
        <!-- Bootstrap Core CSS -->
        <link href=\"";
            // line 30
            echo twig_escape_filter($this->env, (isset($context["asset_url"]) ? $context["asset_url"] : null), "html", null, true);
            echo "\" rel=\"stylesheet\">
        
        <link href=\"https://cdn.datatables.net/1.10.9/css/jquery.dataTables.min.css\" rel=\"stylesheet\">
        <link href=\"https://cdn.datatables.net/buttons/1.0.3/css/buttons.dataTables.min.css\" rel=\"stylesheet\">
        
    
    

        ";
        }
        unset($context["asset_url"]);
        // line 39
        echo "
    ";
    }

    // line 57
    public function block_body_general($context, array $blocks = array())
    {
        echo " 
                 ";
        // line 58
        echo $this->env->getExtension('http_kernel')->renderFragment($this->env->getExtension('http_kernel')->controller("cicrtgBackendBundle:Backend:nav"));
        echo "
                
                <!-- /#page-wrapper -->
            ";
    }

    // line 69
    public function block_javascripts($context, array $blocks = array())
    {
        // line 70
        echo "        
        ";
        // line 71
        if (isset($context['assetic']['debug']) && $context['assetic']['debug']) {
            // asset "a6d92ce_0"
            $context["asset_url"] = isset($context['assetic']['use_controller']) && $context['assetic']['use_controller'] ? $this->env->getExtension('routing')->getPath("_assetic_a6d92ce_0") : $this->env->getExtension('assets')->getAssetUrl("js/a6d92ce_jquery_1.js");
            // line 81
            echo "        <script src=\"";
            echo twig_escape_filter($this->env, (isset($context["asset_url"]) ? $context["asset_url"] : null), "html", null, true);
            echo "\"></script>
        
        
        
        <script src=\"//code.jquery.com/jquery-1.11.3.min.js\"></script>
        <script src=\"https://cdn.datatables.net/1.10.9/js/jquery.dataTables.min.js\"></script>
        <script src=\"https://cdn.datatables.net/buttons/1.0.3/js/dataTables.buttons.min.js\"></script>
        <script src=\"//cdn.datatables.net/buttons/1.0.3/js/buttons.flash.min.js\"></script>
        <script src=\"//cdnjs.cloudflare.com/ajax/libs/jszip/2.5.0/jszip.min.js\"></script>
        <script src=\"//cdn.rawgit.com/bpampuch/pdfmake/0.1.18/build/pdfmake.min.js\"></script>
        <script src=\"//cdn.rawgit.com/bpampuch/pdfmake/0.1.18/build/vfs_fonts.js\"></script>
        <script src=\"//cdn.datatables.net/buttons/1.0.3/js/buttons.html5.min.js\"></script>
        <script src=\"//cdn.datatables.net/buttons/1.0.3/js/buttons.print.min.js\"></script>
        
    
    
    
    
    
    
    
    
    

    
        ";
            // asset "a6d92ce_1"
            $context["asset_url"] = isset($context['assetic']['use_controller']) && $context['assetic']['use_controller'] ? $this->env->getExtension('routing')->getPath("_assetic_a6d92ce_1") : $this->env->getExtension('assets')->getAssetUrl("js/a6d92ce_bootstrap.min_2.js");
            echo "        <script src=\"";
            echo twig_escape_filter($this->env, (isset($context["asset_url"]) ? $context["asset_url"] : null), "html", null, true);
            echo "\"></script>
        
        
        
        <script src=\"//code.jquery.com/jquery-1.11.3.min.js\"></script>
        <script src=\"https://cdn.datatables.net/1.10.9/js/jquery.dataTables.min.js\"></script>
        <script src=\"https://cdn.datatables.net/buttons/1.0.3/js/dataTables.buttons.min.js\"></script>
        <script src=\"//cdn.datatables.net/buttons/1.0.3/js/buttons.flash.min.js\"></script>
        <script src=\"//cdnjs.cloudflare.com/ajax/libs/jszip/2.5.0/jszip.min.js\"></script>
        <script src=\"//cdn.rawgit.com/bpampuch/pdfmake/0.1.18/build/pdfmake.min.js\"></script>
        <script src=\"//cdn.rawgit.com/bpampuch/pdfmake/0.1.18/build/vfs_fonts.js\"></script>
        <script src=\"//cdn.datatables.net/buttons/1.0.3/js/buttons.html5.min.js\"></script>
        <script src=\"//cdn.datatables.net/buttons/1.0.3/js/buttons.print.min.js\"></script>
        
    
    
    
    
    
    
    
    
    

    
        ";
            // asset "a6d92ce_2"
            $context["asset_url"] = isset($context['assetic']['use_controller']) && $context['assetic']['use_controller'] ? $this->env->getExtension('routing')->getPath("_assetic_a6d92ce_2") : $this->env->getExtension('assets')->getAssetUrl("js/a6d92ce_raphael.min_3.js");
            echo "        <script src=\"";
            echo twig_escape_filter($this->env, (isset($context["asset_url"]) ? $context["asset_url"] : null), "html", null, true);
            echo "\"></script>
        
        
        
        <script src=\"//code.jquery.com/jquery-1.11.3.min.js\"></script>
        <script src=\"https://cdn.datatables.net/1.10.9/js/jquery.dataTables.min.js\"></script>
        <script src=\"https://cdn.datatables.net/buttons/1.0.3/js/dataTables.buttons.min.js\"></script>
        <script src=\"//cdn.datatables.net/buttons/1.0.3/js/buttons.flash.min.js\"></script>
        <script src=\"//cdnjs.cloudflare.com/ajax/libs/jszip/2.5.0/jszip.min.js\"></script>
        <script src=\"//cdn.rawgit.com/bpampuch/pdfmake/0.1.18/build/pdfmake.min.js\"></script>
        <script src=\"//cdn.rawgit.com/bpampuch/pdfmake/0.1.18/build/vfs_fonts.js\"></script>
        <script src=\"//cdn.datatables.net/buttons/1.0.3/js/buttons.html5.min.js\"></script>
        <script src=\"//cdn.datatables.net/buttons/1.0.3/js/buttons.print.min.js\"></script>
        
    
    
    
    
    
    
    
    
    

    
        ";
            // asset "a6d92ce_3"
            $context["asset_url"] = isset($context['assetic']['use_controller']) && $context['assetic']['use_controller'] ? $this->env->getExtension('routing')->getPath("_assetic_a6d92ce_3") : $this->env->getExtension('assets')->getAssetUrl("js/a6d92ce_morris.min_4.js");
            echo "        <script src=\"";
            echo twig_escape_filter($this->env, (isset($context["asset_url"]) ? $context["asset_url"] : null), "html", null, true);
            echo "\"></script>
        
        
        
        <script src=\"//code.jquery.com/jquery-1.11.3.min.js\"></script>
        <script src=\"https://cdn.datatables.net/1.10.9/js/jquery.dataTables.min.js\"></script>
        <script src=\"https://cdn.datatables.net/buttons/1.0.3/js/dataTables.buttons.min.js\"></script>
        <script src=\"//cdn.datatables.net/buttons/1.0.3/js/buttons.flash.min.js\"></script>
        <script src=\"//cdnjs.cloudflare.com/ajax/libs/jszip/2.5.0/jszip.min.js\"></script>
        <script src=\"//cdn.rawgit.com/bpampuch/pdfmake/0.1.18/build/pdfmake.min.js\"></script>
        <script src=\"//cdn.rawgit.com/bpampuch/pdfmake/0.1.18/build/vfs_fonts.js\"></script>
        <script src=\"//cdn.datatables.net/buttons/1.0.3/js/buttons.html5.min.js\"></script>
        <script src=\"//cdn.datatables.net/buttons/1.0.3/js/buttons.print.min.js\"></script>
        
    
    
    
    
    
    
    
    
    

    
        ";
            // asset "a6d92ce_4"
            $context["asset_url"] = isset($context['assetic']['use_controller']) && $context['assetic']['use_controller'] ? $this->env->getExtension('routing')->getPath("_assetic_a6d92ce_4") : $this->env->getExtension('assets')->getAssetUrl("js/a6d92ce_jquerydataTables.min_5.js");
            echo "        <script src=\"";
            echo twig_escape_filter($this->env, (isset($context["asset_url"]) ? $context["asset_url"] : null), "html", null, true);
            echo "\"></script>
        
        
        
        <script src=\"//code.jquery.com/jquery-1.11.3.min.js\"></script>
        <script src=\"https://cdn.datatables.net/1.10.9/js/jquery.dataTables.min.js\"></script>
        <script src=\"https://cdn.datatables.net/buttons/1.0.3/js/dataTables.buttons.min.js\"></script>
        <script src=\"//cdn.datatables.net/buttons/1.0.3/js/buttons.flash.min.js\"></script>
        <script src=\"//cdnjs.cloudflare.com/ajax/libs/jszip/2.5.0/jszip.min.js\"></script>
        <script src=\"//cdn.rawgit.com/bpampuch/pdfmake/0.1.18/build/pdfmake.min.js\"></script>
        <script src=\"//cdn.rawgit.com/bpampuch/pdfmake/0.1.18/build/vfs_fonts.js\"></script>
        <script src=\"//cdn.datatables.net/buttons/1.0.3/js/buttons.html5.min.js\"></script>
        <script src=\"//cdn.datatables.net/buttons/1.0.3/js/buttons.print.min.js\"></script>
        
    
    
    
    
    
    
    
    
    

    
        ";
            // asset "a6d92ce_5"
            $context["asset_url"] = isset($context['assetic']['use_controller']) && $context['assetic']['use_controller'] ? $this->env->getExtension('routing')->getPath("_assetic_a6d92ce_5") : $this->env->getExtension('assets')->getAssetUrl("js/a6d92ce_dataTables.select.min_6.js");
            echo "        <script src=\"";
            echo twig_escape_filter($this->env, (isset($context["asset_url"]) ? $context["asset_url"] : null), "html", null, true);
            echo "\"></script>
        
        
        
        <script src=\"//code.jquery.com/jquery-1.11.3.min.js\"></script>
        <script src=\"https://cdn.datatables.net/1.10.9/js/jquery.dataTables.min.js\"></script>
        <script src=\"https://cdn.datatables.net/buttons/1.0.3/js/dataTables.buttons.min.js\"></script>
        <script src=\"//cdn.datatables.net/buttons/1.0.3/js/buttons.flash.min.js\"></script>
        <script src=\"//cdnjs.cloudflare.com/ajax/libs/jszip/2.5.0/jszip.min.js\"></script>
        <script src=\"//cdn.rawgit.com/bpampuch/pdfmake/0.1.18/build/pdfmake.min.js\"></script>
        <script src=\"//cdn.rawgit.com/bpampuch/pdfmake/0.1.18/build/vfs_fonts.js\"></script>
        <script src=\"//cdn.datatables.net/buttons/1.0.3/js/buttons.html5.min.js\"></script>
        <script src=\"//cdn.datatables.net/buttons/1.0.3/js/buttons.print.min.js\"></script>
        
    
    
    
    
    
    
    
    
    

    
        ";
            // asset "a6d92ce_6"
            $context["asset_url"] = isset($context['assetic']['use_controller']) && $context['assetic']['use_controller'] ? $this->env->getExtension('routing')->getPath("_assetic_a6d92ce_6") : $this->env->getExtension('assets')->getAssetUrl("js/a6d92ce_custom_7.js");
            echo "        <script src=\"";
            echo twig_escape_filter($this->env, (isset($context["asset_url"]) ? $context["asset_url"] : null), "html", null, true);
            echo "\"></script>
        
        
        
        <script src=\"//code.jquery.com/jquery-1.11.3.min.js\"></script>
        <script src=\"https://cdn.datatables.net/1.10.9/js/jquery.dataTables.min.js\"></script>
        <script src=\"https://cdn.datatables.net/buttons/1.0.3/js/dataTables.buttons.min.js\"></script>
        <script src=\"//cdn.datatables.net/buttons/1.0.3/js/buttons.flash.min.js\"></script>
        <script src=\"//cdnjs.cloudflare.com/ajax/libs/jszip/2.5.0/jszip.min.js\"></script>
        <script src=\"//cdn.rawgit.com/bpampuch/pdfmake/0.1.18/build/pdfmake.min.js\"></script>
        <script src=\"//cdn.rawgit.com/bpampuch/pdfmake/0.1.18/build/vfs_fonts.js\"></script>
        <script src=\"//cdn.datatables.net/buttons/1.0.3/js/buttons.html5.min.js\"></script>
        <script src=\"//cdn.datatables.net/buttons/1.0.3/js/buttons.print.min.js\"></script>
        
    
    
    
    
    
    
    
    
    

    
        ";
        } else {
            // asset "a6d92ce"
            $context["asset_url"] = isset($context['assetic']['use_controller']) && $context['assetic']['use_controller'] ? $this->env->getExtension('routing')->getPath("_assetic_a6d92ce") : $this->env->getExtension('assets')->getAssetUrl("js/a6d92ce.js");
            echo "        <script src=\"";
            echo twig_escape_filter($this->env, (isset($context["asset_url"]) ? $context["asset_url"] : null), "html", null, true);
            echo "\"></script>
        
        
        
        <script src=\"//code.jquery.com/jquery-1.11.3.min.js\"></script>
        <script src=\"https://cdn.datatables.net/1.10.9/js/jquery.dataTables.min.js\"></script>
        <script src=\"https://cdn.datatables.net/buttons/1.0.3/js/dataTables.buttons.min.js\"></script>
        <script src=\"//cdn.datatables.net/buttons/1.0.3/js/buttons.flash.min.js\"></script>
        <script src=\"//cdnjs.cloudflare.com/ajax/libs/jszip/2.5.0/jszip.min.js\"></script>
        <script src=\"//cdn.rawgit.com/bpampuch/pdfmake/0.1.18/build/pdfmake.min.js\"></script>
        <script src=\"//cdn.rawgit.com/bpampuch/pdfmake/0.1.18/build/vfs_fonts.js\"></script>
        <script src=\"//cdn.datatables.net/buttons/1.0.3/js/buttons.html5.min.js\"></script>
        <script src=\"//cdn.datatables.net/buttons/1.0.3/js/buttons.print.min.js\"></script>
        
    
    
    
    
    
    
    
    
    

    
        ";
        }
        unset($context["asset_url"]);
        // line 107
        echo "    ";
    }

    // line 111
    public function block_addFunctionDocument($context, array $blocks = array())
    {
        // line 112
        echo "            \$('.zone').dataTable();
            \$('#datatable').dataTable(
                                      {
                                        //dom: 'T<\"clear\">lfrtip',
                                        //\"tableTools\": {
                                        //                    \"sSwfPath\": \"/swf/copy_csv_xls_pdf.swf\"
                                        //                },
                                        dom: 'Bfrtip',
                                        buttons:
                                        [{
                                             extend: 'collection',
                                            text: 'Exporter !',
                                            buttons:
                                            [
                                               'copy',
                                                'csv',
                                                'excel',
                                                {
                                                    extend: 'pdfHtml5',
                                                    download: 'open'
                                                },
                                                'print' 
                                            ]
                                            
                                        }],
                                         language: 
                                            {        
                                                processing:     \"Traitement en cours...\",        
                                                search:         \"Rechercher&nbsp;:\",        
                                                lengthMenu:    \"Afficher _MENU_ &eacute;l&eacute;ments\",        
                                                info:           \"Affichage de l'&eacute;lement _START_ &agrave; _END_ sur _TOTAL_ &eacute;l&eacute;ments\",        
                                                infoEmpty:      \"Affichage de l'&eacute;lement 0 &agrave; 0 sur 0 &eacute;l&eacute;ments\",        
                                                infoFiltered:   \"(filtr&eacute; de _MAX_ &eacute;l&eacute;ments au total)\",        
                                                infoPostFix:    \"\",        
                                                loadingRecords: \"Chargement en cours...\",        
                                                zeroRecords:    \"Aucun &eacute;l&eacute;ment &agrave; afficher\",        
                                                emptyTable:     \"Aucune donnée disponible dans le tableau\",        
                                                paginate: 
                                                {            
                                                    first:      \"Premier\",            
                                                    previous:   \"Pr&eacute;c&eacute;dent\",            
                                                    next:       \"Suivant\",            
                                                    last:       \"Dernier\"        
                                                },        
                                                aria: 
                                                {            
                                                    sortAscending:  \": activer pour trier la colonne par ordre croissant\",            
                                                    sortDescending: \": activer pour trier la colonne par ordre décroissant\"        
                                                }    
                                            }
                                        }
                                    );
        ";
    }

    public function getTemplateName()
    {
        return "::layout.html.twig";
    }

    public function getDebugInfo()
    {
        return array (  592 => 112,  589 => 111,  585 => 107,  343 => 81,  339 => 71,  336 => 70,  333 => 69,  325 => 58,  320 => 57,  315 => 39,  302 => 30,  296 => 26,  282 => 30,  276 => 26,  263 => 30,  257 => 26,  244 => 30,  238 => 26,  225 => 30,  219 => 26,  206 => 30,  200 => 26,  187 => 30,  181 => 26,  168 => 30,  162 => 26,  149 => 30,  143 => 26,  130 => 30,  124 => 26,  111 => 30,  105 => 26,  100 => 15,  97 => 14,  91 => 12,  82 => 165,  80 => 111,  75 => 108,  73 => 69,  64 => 62,  62 => 57,  44 => 41,  42 => 14,  37 => 12,  24 => 1,);
    }
}
/* ﻿<!DOCTYPE html>*/
/* <html lang="en">*/
/* */
/* <head>*/
/* */
/*     <meta charset="utf-8">*/
/*     <meta http-equiv="X-UA-Compatible" content="IE=edge">*/
/*     <meta name="viewport" content="width=device-width, initial-scale=1">*/
/*     <meta name="description" content="">*/
/*     <meta name="author" content="">*/
/* */
/*     <title>{% block title %} Cicrtg | {% endblock %}</title>*/
/* */
/*     {% block stylesheet %}*/
/*         {% stylesheets 'bundles/cicrtg/backend/css/bootstrap.min.css'*/
/*                         'bundles/cicrtg/backend/css/sb-admin.css'*/
/*                         'bundles/cicrtg/backend/css/plugins/morris.css'*/
/*                         'bundles/cicrtg/backend/font-awesome/css/font-awesome.min.css'*/
/*                         'bundles/cicrtg/media/css/jquery.dataTables.min.css'*/
/*                         'bundles/cicrtg/extensions/TableTools/css/dataTables.tableTools.min.css'*/
/*                         */
/*                         'bundles/cicrtg/extensions/Select/css/select.bootstrap.min.css'*/
/*                         'bundles/cicrtg/extensions/Select/css/select.dataTables.min.css'*/
/*                         'bundles/cicrtg/extensions/Select/css/select.foundation.min.css'*/
/*                         'bundles/cicrtg/extensions/Select/css/select.jqueryui.min.css' filter="cssrewrite" %}*/
/*                         */
/*                         */
/*                    */
/*         <!-- Bootstrap Core CSS -->*/
/*         <link href="{{ asset_url }}" rel="stylesheet">*/
/*         */
/*         <link href="https://cdn.datatables.net/1.10.9/css/jquery.dataTables.min.css" rel="stylesheet">*/
/*         <link href="https://cdn.datatables.net/buttons/1.0.3/css/buttons.dataTables.min.css" rel="stylesheet">*/
/*         */
/*     */
/*     */
/* */
/*         {% endstylesheets %}*/
/* */
/*     {% endblock %}*/
/*         */
/*     <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->*/
/*     <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->*/
/*     <!--[if lt IE 9]>*/
/*         <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>*/
/*         <script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>*/
/*     <![endif]-->*/
/* */
/* </head>*/
/* */
/* <body>*/
/*     */
/*         <div id="wrapper">*/
/* */
/*                 <!-- Navigation -->*/
/*                */
/*             {% block body_general%} */
/*                  {{ render(controller("cicrtgBackendBundle:Backend:nav")) }}*/
/*                 */
/*                 <!-- /#page-wrapper -->*/
/*             {% endblock %}*/
/*         </div>*/
/* */
/*     */
/*     */
/*     <!-- /#wrapper -->*/
/* */
/*     <!-- jQuery -->*/
/*     {% block javascripts %}*/
/*         */
/*         {% javascripts 'bundles/cicrtg/backend/js/jquery.js'*/
/*                         'bundles/cicrtg/backend/js/bootstrap.min.js'*/
/*                         'bundles/cicrtg/backend/js/plugins/morris/raphael.min.js'*/
/*                         'bundles/cicrtg/backend/js/plugins/morris/morris.min.js'*/
/*                         */
/*                                                 */
/*                         */
/*                         'bundles/cicrtg/media/js/jquerydataTables.min.js'*/
/*                         'bundles/cicrtg/extensions/Select/js/dataTables.select.min.js'*/
/*                         'bundles/cicrtg/worthy/js/custom.js' %}*/
/*         <script src="{{ asset_url }}"></script>*/
/*         */
/*         */
/*         */
/*         <script src="//code.jquery.com/jquery-1.11.3.min.js"></script>*/
/*         <script src="https://cdn.datatables.net/1.10.9/js/jquery.dataTables.min.js"></script>*/
/*         <script src="https://cdn.datatables.net/buttons/1.0.3/js/dataTables.buttons.min.js"></script>*/
/*         <script src="//cdn.datatables.net/buttons/1.0.3/js/buttons.flash.min.js"></script>*/
/*         <script src="//cdnjs.cloudflare.com/ajax/libs/jszip/2.5.0/jszip.min.js"></script>*/
/*         <script src="//cdn.rawgit.com/bpampuch/pdfmake/0.1.18/build/pdfmake.min.js"></script>*/
/*         <script src="//cdn.rawgit.com/bpampuch/pdfmake/0.1.18/build/vfs_fonts.js"></script>*/
/*         <script src="//cdn.datatables.net/buttons/1.0.3/js/buttons.html5.min.js"></script>*/
/*         <script src="//cdn.datatables.net/buttons/1.0.3/js/buttons.print.min.js"></script>*/
/*         */
/*     */
/*     */
/*     */
/*     */
/*     */
/*     */
/*     */
/*     */
/*     */
/* */
/*     */
/*         {% endjavascripts %}*/
/*     {% endblock %}*/
/*     <script>*/
/*     $(document).ready(function()*/
/*     {*/
/*         {% block addFunctionDocument %}*/
/*             $('.zone').dataTable();*/
/*             $('#datatable').dataTable(*/
/*                                       {*/
/*                                         //dom: 'T<"clear">lfrtip',*/
/*                                         //"tableTools": {*/
/*                                         //                    "sSwfPath": "/swf/copy_csv_xls_pdf.swf"*/
/*                                         //                },*/
/*                                         dom: 'Bfrtip',*/
/*                                         buttons:*/
/*                                         [{*/
/*                                              extend: 'collection',*/
/*                                             text: 'Exporter !',*/
/*                                             buttons:*/
/*                                             [*/
/*                                                'copy',*/
/*                                                 'csv',*/
/*                                                 'excel',*/
/*                                                 {*/
/*                                                     extend: 'pdfHtml5',*/
/*                                                     download: 'open'*/
/*                                                 },*/
/*                                                 'print' */
/*                                             ]*/
/*                                             */
/*                                         }],*/
/*                                          language: */
/*                                             {        */
/*                                                 processing:     "Traitement en cours...",        */
/*                                                 search:         "Rechercher&nbsp;:",        */
/*                                                 lengthMenu:    "Afficher _MENU_ &eacute;l&eacute;ments",        */
/*                                                 info:           "Affichage de l'&eacute;lement _START_ &agrave; _END_ sur _TOTAL_ &eacute;l&eacute;ments",        */
/*                                                 infoEmpty:      "Affichage de l'&eacute;lement 0 &agrave; 0 sur 0 &eacute;l&eacute;ments",        */
/*                                                 infoFiltered:   "(filtr&eacute; de _MAX_ &eacute;l&eacute;ments au total)",        */
/*                                                 infoPostFix:    "",        */
/*                                                 loadingRecords: "Chargement en cours...",        */
/*                                                 zeroRecords:    "Aucun &eacute;l&eacute;ment &agrave; afficher",        */
/*                                                 emptyTable:     "Aucune donnée disponible dans le tableau",        */
/*                                                 paginate: */
/*                                                 {            */
/*                                                     first:      "Premier",            */
/*                                                     previous:   "Pr&eacute;c&eacute;dent",            */
/*                                                     next:       "Suivant",            */
/*                                                     last:       "Dernier"        */
/*                                                 },        */
/*                                                 aria: */
/*                                                 {            */
/*                                                     sortAscending:  ": activer pour trier la colonne par ordre croissant",            */
/*                                                     sortDescending: ": activer pour trier la colonne par ordre décroissant"        */
/*                                                 }    */
/*                                             }*/
/*                                         }*/
/*                                     );*/
/*         {% endblock %}*/
/*     } );*/
/*     </script>*/
/* </body>*/
/* */
/* </html>*/
/* */

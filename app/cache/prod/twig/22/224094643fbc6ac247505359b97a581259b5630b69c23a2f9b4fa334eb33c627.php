<?php

/* cicrtgBackendBundle:includes:agentsBody.html.twig */
class __TwigTemplate_694ac085478633935997fe7294736c01bb91ea0885a2659b68ca8065d46c9be5 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        // line 1
        echo "﻿";
        $context['_parent'] = $context;
        $context['_seq'] = twig_ensure_traversable($this->getAttribute($this->getAttribute($this->getAttribute((isset($context["app"]) ? $context["app"] : null), "session", array()), "flashbag", array()), "get", array(0 => "notice"), "method"));
        foreach ($context['_seq'] as $context["_key"] => $context["message"]) {
            // line 2
            echo "\t\t\t\t
    <div class=\"alert alert-success\">
        <a class=\"close fade in\" data-dismiss=\"alert\">&times;</a>
        ";
            // line 5
            echo twig_escape_filter($this->env, $context["message"], "html", null, true);
            echo "
    </div>
";
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['message'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 8
        echo "

<div class=\"row\">
   <div class=\"col-sm-12\">
\t\t<div class=\"panel panel-default\">
\t\t\t<div class=\"panel-heading\">
\t\t\t\t<h2 class=\"panel-title\"> Liste des agents sur le terrain 
\t\t\t\t<button data-toggle=\"modal\" data-backdrop=\"false\"  href=\"#prefecture\" class=\"btn btn-info btn-sm pull-right\" style=\"margin-top: -5px;\">
\t\t\t\t\t<i class=\"fa fa-plus\"></i> Ajouter </a>
\t\t\t\t</button></h2>
\t\t\t</div>
\t\t\t<div class=\"panel-body\">
\t\t\t\t<table  class=\"zone hover table table-bordered table-striped table-condensed\" cellspacing=\"0\" width=\"100%\" >
\t\t\t\t\t<thead>
\t\t\t\t\t\t<tr>
\t\t\t\t\t\t\t
\t\t\t\t\t\t\t
\t\t\t\t\t\t\t<th>Nom & Prénoms</th>
\t\t\t\t\t\t\t<th>Sexe</th>
\t\t\t\t\t\t\t<th>Télephone</th>
\t\t\t\t\t\t\t";
        // line 28
        if ($this->env->getExtension('security')->isGranted("ROLE_ADMIN")) {
            // line 29
            echo "\t\t\t\t\t\t\t<th>code secret</th>
\t\t\t\t\t\t\t";
        }
        // line 31
        echo "\t\t\t\t\t\t\t<th>Email</th>
\t\t\t\t\t\t\t<th>Résidence</th>
\t\t\t\t\t\t\t<th>Localité</th>
\t\t\t\t\t\t\t";
        // line 34
        if ($this->env->getExtension('security')->isGranted("ROLE_GESTIONNAIRE")) {
            // line 35
            echo "\t\t\t\t\t\t\t\t<th>Action</th>
\t\t\t\t\t\t\t";
        }
        // line 37
        echo "\t\t\t\t\t\t</tr>
\t\t\t\t\t</thead>
\t\t\t\t 
\t\t\t\t\t<tfoot>
\t\t\t\t\t\t<tr>
\t\t\t\t\t\t\t
\t\t\t\t\t\t\t
\t\t\t\t\t\t</tr>
\t\t\t\t\t</tfoot>
\t\t\t\t 
\t\t\t\t\t<tbody>
\t\t\t\t\t\t
\t\t\t\t\t\t";
        // line 49
        $context['_parent'] = $context;
        $context['_seq'] = twig_ensure_traversable((isset($context["agents"]) ? $context["agents"] : null));
        foreach ($context['_seq'] as $context["_key"] => $context["agent"]) {
            // line 50
            echo "\t\t\t\t\t\t<tr>
\t\t\t\t\t\t\t
\t\t\t\t\t\t\t<th>";
            // line 52
            echo twig_escape_filter($this->env, (($this->getAttribute($context["agent"], "nom", array()) . " ") . $this->getAttribute($context["agent"], "prenom", array())), "html", null, true);
            echo "</th>
\t\t\t\t\t\t\t<th>";
            // line 53
            echo twig_escape_filter($this->env, $this->getAttribute($context["agent"], "sexe", array()), "html", null, true);
            echo "</th>
\t\t\t\t\t\t\t<th>";
            // line 54
            echo twig_escape_filter($this->env, $this->getAttribute($context["agent"], "tel", array()), "html", null, true);
            echo "</th>
\t\t\t\t\t\t\t";
            // line 55
            if ($this->env->getExtension('security')->isGranted("ROLE_ADMIN")) {
                // line 56
                echo "\t\t\t\t\t\t\t\t<th>";
                echo twig_escape_filter($this->env, $this->getAttribute($context["agent"], "codeS", array()), "html", null, true);
                echo "</th>
\t\t\t\t\t\t\t";
            }
            // line 58
            echo "\t\t\t\t\t\t\t<th>";
            echo twig_escape_filter($this->env, $this->getAttribute($context["agent"], "email", array()), "html", null, true);
            echo "</th>
\t\t\t\t\t\t\t<th>";
            // line 59
            echo twig_escape_filter($this->env, $this->getAttribute($context["agent"], "residence", array()), "html", null, true);
            echo "</th>
\t\t\t\t\t\t\t<th>
\t\t\t\t\t\t\t\t";
            // line 61
            if (($this->getAttribute($this->getAttribute($context["agent"], "localites", array()), "count", array()) > 0)) {
                // line 62
                echo "\t\t\t\t\t\t\t\t\t";
                $context['_parent'] = $context;
                $context['_seq'] = twig_ensure_traversable($this->getAttribute($context["agent"], "localites", array()));
                $context['loop'] = array(
                  'parent' => $context['_parent'],
                  'index0' => 0,
                  'index'  => 1,
                  'first'  => true,
                );
                if (is_array($context['_seq']) || (is_object($context['_seq']) && $context['_seq'] instanceof Countable)) {
                    $length = count($context['_seq']);
                    $context['loop']['revindex0'] = $length - 1;
                    $context['loop']['revindex'] = $length;
                    $context['loop']['length'] = $length;
                    $context['loop']['last'] = 1 === $length;
                }
                foreach ($context['_seq'] as $context["_key"] => $context["localite"]) {
                    // line 63
                    echo "\t\t\t\t\t\t\t\t\t\t";
                    echo twig_escape_filter($this->env, $this->getAttribute($context["localite"], "libL", array()), "html", null, true);
                    echo "
\t\t\t\t\t\t\t\t\t\t";
                    // line 64
                    if ( !$this->getAttribute($context["loop"], "last", array())) {
                        echo " - ";
                    }
                    // line 65
                    echo "\t\t\t\t\t\t\t\t\t";
                    ++$context['loop']['index0'];
                    ++$context['loop']['index'];
                    $context['loop']['first'] = false;
                    if (isset($context['loop']['length'])) {
                        --$context['loop']['revindex0'];
                        --$context['loop']['revindex'];
                        $context['loop']['last'] = 0 === $context['loop']['revindex0'];
                    }
                }
                $_parent = $context['_parent'];
                unset($context['_seq'], $context['_iterated'], $context['_key'], $context['localite'], $context['_parent'], $context['loop']);
                $context = array_intersect_key($context, $_parent) + $_parent;
                // line 66
                echo "\t\t\t\t\t\t\t\t";
            }
            // line 67
            echo "\t\t\t\t\t\t\t</th>
\t\t\t\t\t\t\t";
            // line 68
            if ($this->env->getExtension('security')->isGranted("ROLE_GESTIONNAIRE")) {
                // line 69
                echo "\t\t\t\t\t\t\t\t<td class=\"row\">
\t\t\t\t\t\t\t\t\t<span class=\"col-xs-4\">
\t\t\t\t\t\t\t\t\t\t<a href=\"";
                // line 71
                echo twig_escape_filter($this->env, $this->env->getExtension('routing')->getPath("cicrtg_backend_agent_modif", array("id" => $this->getAttribute($context["agent"], "id", array()))), "html", null, true);
                echo "\" class=\"btn btn-xs btn-default\">
\t\t\t\t\t\t\t\t\t\t\t<li class=\"fa fa-edit\" ></li>Modifier
\t\t\t\t\t\t\t\t\t\t</a>
\t\t\t\t\t\t\t\t\t</span>
\t\t\t\t\t\t\t\t\t<span class=\"col-xs-4 col-xs-offset-2\">
\t\t\t\t\t\t\t\t\t\t";
                // line 76
                if ($this->env->getExtension('security')->isGranted("ROLE_ADMIN")) {
                    // line 77
                    echo "\t\t\t\t\t\t\t\t\t\t\t<a href=\"";
                    echo twig_escape_filter($this->env, $this->env->getExtension('routing')->getPath("cicrtg_backend_agent_supp", array("id" => $this->getAttribute($context["agent"], "id", array()))), "html", null, true);
                    echo "\" target-data=\"#supp\" class=\"btn btn-xs btn-danger\">
\t\t\t\t\t\t\t\t\t\t\t\t<i class=\"fa fa-frown-o\"></i>Supprimer
\t\t\t\t\t\t\t\t\t\t\t</a>
\t\t\t\t\t\t\t\t\t\t";
                }
                // line 81
                echo "\t\t\t\t\t\t\t\t\t</span>
\t\t\t\t\t\t\t\t</td>
\t\t\t\t\t\t\t";
            }
            // line 84
            echo "\t\t\t\t\t\t</tr>
\t\t\t\t\t\t";
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['agent'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 86
        echo "\t\t\t\t\t\t
\t\t\t\t\t</tbody>
\t\t\t\t</table>
\t\t\t</div>
\t\t</div>
   </div>
</div>
       
     
<div class=\"modal fade\" id=\"prefecture\">
      <div class=\"modal-dialog\">
          <div class=\"modal-content\">
              <div class=\"modal-header\" style=\"color: #000000;\">
                  <button type=\"button\" class=\"close\" data-dismiss=\"modal\">x</button>
                  <h4 class=\"modal-title\">Ajout d'un agent</h4>
              </div>
              <div class=\"modal-body\" style=\"color: #000000;\">

                    <form  method=\"post\" action=\"";
        // line 104
        echo $this->env->getExtension('routing')->getPath("cicrtg_backend_agent");
        echo "\">
                        <span class=\"help-block\">
                            ";
        // line 106
        echo $this->env->getExtension('form')->renderer->searchAndRenderBlock((isset($context["form"]) ? $context["form"] : null), 'errors');
        echo "
                        </span>
                            
                           <div class=\"form-group\">
                             <div class=\"row\">
                                 <div class=\"col-md-3\">
                                     <label for=\"groupName\" > ";
        // line 112
        echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute((isset($context["form"]) ? $context["form"] : null), "nom", array()), 'label', array("label" => "Nom"));
        echo " :</label>
                                 </div>
\t\t\t\t\t\t\t\t\t\t
                                   <small class=\"text-danger\"> ";
        // line 115
        echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute((isset($context["form"]) ? $context["form"] : null), "nom", array()), 'errors');
        echo " </small>
                                   <div class=\"col-md-7 \">
\t\t\t\t\t\t\t\t\t\t\t";
        // line 117
        echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute((isset($context["form"]) ? $context["form"] : null), "nom", array()), 'widget', array("attr" => array("class" => "form-control", "Placeholder" => "Le nom de l agent")));
        echo "
                                    </div>
                               </div>
                           </div>
\t\t\t\t\t\t\t
\t\t\t\t\t\t\t<div class=\"form-group\">
                             <div class=\"row\">
                                 <div class=\"col-md-3\">
                                     <label for=\"groupName\" > ";
        // line 125
        echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute((isset($context["form"]) ? $context["form"] : null), "prenom", array()), 'label', array("label" => "Prenom"));
        echo " :</label>
                                 </div>
\t\t\t\t\t\t\t\t\t\t
                                   <small class=\"text-danger\"> ";
        // line 128
        echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute((isset($context["form"]) ? $context["form"] : null), "prenom", array()), 'errors');
        echo " </small>
                                   <div class=\"col-md-7 \">
\t\t\t\t\t\t\t\t\t\t\t";
        // line 130
        echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute((isset($context["form"]) ? $context["form"] : null), "prenom", array()), 'widget', array("attr" => array("class" => "form-control", "Placeholder" => "Le prénom")));
        echo "
                                    </div>
                               </div>
                           </div>
\t\t\t\t\t\t\t
\t\t\t\t\t\t\t<div class=\"form-group\">
                             <div class=\"row\">
                                 <div class=\"col-md-3\">       
                                     <label for=\"groupName\" > ";
        // line 138
        echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute((isset($context["form"]) ? $context["form"] : null), "sexe", array()), 'label', array("label" => "Sexe"));
        echo " :</label>
                                 </div>
\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t 
                                   <small class=\"text-danger\"> ";
        // line 141
        echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute((isset($context["form"]) ? $context["form"] : null), "sexe", array()), 'errors');
        echo " </small>
                                   <div class=\"col-md-7 \">
\t\t\t\t\t\t\t\t\t\t\t";
        // line 143
        echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute((isset($context["form"]) ? $context["form"] : null), "sexe", array()), 'widget', array("attr" => array("class" => "form-control", "Placeholder" => "Veuillez choisir")));
        echo "
                                    </div>
                               </div>
                           </div>
\t\t\t\t\t\t\t<div class=\"form-group\">
                             <div class=\"row\">
                                 <div class=\"col-md-3\">
                                     <label for=\"groupName\" > ";
        // line 150
        echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute((isset($context["form"]) ? $context["form"] : null), "tel", array()), 'label', array("label" => "Téléphone"));
        echo " :</label>
                                 </div>
\t\t\t\t\t\t\t\t\t\t
                                   <small class=\"text-danger\"> ";
        // line 153
        echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute((isset($context["form"]) ? $context["form"] : null), "tel", array()), 'errors');
        echo " </small>
                                   <div class=\"col-md-7 \">
\t\t\t\t\t\t\t\t\t\t\t";
        // line 155
        echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute((isset($context["form"]) ? $context["form"] : null), "tel", array()), 'widget', array("attr" => array("class" => "form-control", "Placeholder" => "xx xx xx xx")));
        echo "
                                    </div>
                               </div>
                           </div>
\t\t\t\t\t\t\t
\t\t\t\t\t\t\t<div class=\"form-group\">
                             <div class=\"row\">
                                 <div class=\"col-md-3\">
                                     <label for=\"groupName\" > ";
        // line 163
        echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute((isset($context["form"]) ? $context["form"] : null), "codeS", array()), 'label', array("label" => "Code secret"));
        echo " :</label>
                                 </div>
\t\t\t\t\t\t\t\t\t\t
                                   <small class=\"text-danger\"> ";
        // line 166
        echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute((isset($context["form"]) ? $context["form"] : null), "codeS", array()), 'errors');
        echo " </small>
                                   <div class=\"col-md-7 \">
\t\t\t\t\t\t\t\t\t\t\t";
        // line 168
        echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute((isset($context["form"]) ? $context["form"] : null), "codeS", array()), 'widget', array("attr" => array("class" => "form-control", "Placeholder" => "Le code secret de l agent")));
        echo "
                                    </div>
                               </div>
                           </div>
\t\t\t\t\t\t\t
\t\t\t\t\t\t\t<div class=\"form-group\">
                             <div class=\"row\">
                                 <div class=\"col-md-3\">
                                     <label for=\"groupName\" > ";
        // line 176
        echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute((isset($context["form"]) ? $context["form"] : null), "email", array()), 'label', array("label" => "email"));
        echo " :</label>
                                 </div>
\t\t\t\t\t\t\t\t\t\t
                                   <small class=\"text-danger\"> ";
        // line 179
        echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute((isset($context["form"]) ? $context["form"] : null), "email", array()), 'errors');
        echo " </small>
                                   <div class=\"col-md-7 \">
\t\t\t\t\t\t\t\t\t\t\t";
        // line 181
        echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute((isset($context["form"]) ? $context["form"] : null), "email", array()), 'widget', array("attr" => array("class" => "form-control", "Placeholder" => "l email")));
        echo "
                                    </div>
                               </div>
                           </div>
\t\t\t\t\t\t\t<div class=\"form-group\">
                             <div class=\"row\">
                                 <div class=\"col-md-3\">
                                     <label for=\"groupName\" > ";
        // line 188
        echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute((isset($context["form"]) ? $context["form"] : null), "residence", array()), 'label', array("label" => "Résidence"));
        echo " :</label>
                                 </div>
\t\t\t\t\t\t\t\t\t\t
                                   <small class=\"text-danger\"> ";
        // line 191
        echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute((isset($context["form"]) ? $context["form"] : null), "residence", array()), 'errors');
        echo " </small>
                                   <div class=\"col-md-7 \">
\t\t\t\t\t\t\t\t\t\t\t";
        // line 193
        echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute((isset($context["form"]) ? $context["form"] : null), "residence", array()), 'widget', array("attr" => array("class" => "form-control", "Placeholder" => "La résidence")));
        echo "
                                    </div>
                               </div>
                           </div>
\t\t\t\t\t\t\t
\t\t\t\t\t\t\t<div class=\"form-group\">
                             <div class=\"row\">
                                 <div class=\"col-md-3\">
                                     <label for=\"groupName\" > ";
        // line 201
        echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute((isset($context["form"]) ? $context["form"] : null), "localites", array()), 'label', array("label" => "Localité"));
        echo " :</label>
                                 </div>
\t\t\t\t\t\t\t\t\t\t
                                   <small class=\"text-danger\"> ";
        // line 204
        echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute((isset($context["form"]) ? $context["form"] : null), "localites", array()), 'errors');
        echo " </small>
                                   <div class=\"col-md-7 \">
\t\t\t\t\t\t\t\t\t\t\t";
        // line 206
        echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute((isset($context["form"]) ? $context["form"] : null), "localites", array()), 'widget', array("attr" => array("class" => "form-control")));
        echo "
                                    </div>
                               </div>
                           </div>

                          <div class=\"modal-footer\">
                                
                                <input type=\"submit\" class=\"btn btn-primary\" />
                               &nbsp; <button class=\"btn btn-default pull-right\" data-dismiss=\"modal\">Fermer !</button>
                          </div>
                        ";
        // line 216
        echo $this->env->getExtension('form')->renderer->searchAndRenderBlock((isset($context["form"]) ? $context["form"] : null), 'rest');
        echo "
                    </form>

                       
              </div>

          </div>
      </div>
    </div>
";
    }

    public function getTemplateName()
    {
        return "cicrtgBackendBundle:includes:agentsBody.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  424 => 216,  411 => 206,  406 => 204,  400 => 201,  389 => 193,  384 => 191,  378 => 188,  368 => 181,  363 => 179,  357 => 176,  346 => 168,  341 => 166,  335 => 163,  324 => 155,  319 => 153,  313 => 150,  303 => 143,  298 => 141,  292 => 138,  281 => 130,  276 => 128,  270 => 125,  259 => 117,  254 => 115,  248 => 112,  239 => 106,  234 => 104,  214 => 86,  207 => 84,  202 => 81,  194 => 77,  192 => 76,  184 => 71,  180 => 69,  178 => 68,  175 => 67,  172 => 66,  158 => 65,  154 => 64,  149 => 63,  131 => 62,  129 => 61,  124 => 59,  119 => 58,  113 => 56,  111 => 55,  107 => 54,  103 => 53,  99 => 52,  95 => 50,  91 => 49,  77 => 37,  73 => 35,  71 => 34,  66 => 31,  62 => 29,  60 => 28,  38 => 8,  29 => 5,  24 => 2,  19 => 1,);
    }
}
/* ﻿{% for message in app.session.flashbag.get('notice') %}*/
/* 				*/
/*     <div class="alert alert-success">*/
/*         <a class="close fade in" data-dismiss="alert">&times;</a>*/
/*         {{ message }}*/
/*     </div>*/
/* {% endfor %}*/
/* */
/* */
/* <div class="row">*/
/*    <div class="col-sm-12">*/
/* 		<div class="panel panel-default">*/
/* 			<div class="panel-heading">*/
/* 				<h2 class="panel-title"> Liste des agents sur le terrain */
/* 				<button data-toggle="modal" data-backdrop="false"  href="#prefecture" class="btn btn-info btn-sm pull-right" style="margin-top: -5px;">*/
/* 					<i class="fa fa-plus"></i> Ajouter </a>*/
/* 				</button></h2>*/
/* 			</div>*/
/* 			<div class="panel-body">*/
/* 				<table  class="zone hover table table-bordered table-striped table-condensed" cellspacing="0" width="100%" >*/
/* 					<thead>*/
/* 						<tr>*/
/* 							*/
/* 							*/
/* 							<th>Nom & Prénoms</th>*/
/* 							<th>Sexe</th>*/
/* 							<th>Télephone</th>*/
/* 							{% if is_granted('ROLE_ADMIN') %}*/
/* 							<th>code secret</th>*/
/* 							{% endif %}*/
/* 							<th>Email</th>*/
/* 							<th>Résidence</th>*/
/* 							<th>Localité</th>*/
/* 							{% if is_granted('ROLE_GESTIONNAIRE') %}*/
/* 								<th>Action</th>*/
/* 							{% endif %}*/
/* 						</tr>*/
/* 					</thead>*/
/* 				 */
/* 					<tfoot>*/
/* 						<tr>*/
/* 							*/
/* 							*/
/* 						</tr>*/
/* 					</tfoot>*/
/* 				 */
/* 					<tbody>*/
/* 						*/
/* 						{% for agent in agents %}*/
/* 						<tr>*/
/* 							*/
/* 							<th>{{ agent.nom ~' '~agent.prenom }}</th>*/
/* 							<th>{{ agent.sexe }}</th>*/
/* 							<th>{{ agent.tel }}</th>*/
/* 							{% if is_granted('ROLE_ADMIN') %}*/
/* 								<th>{{ agent.codeS }}</th>*/
/* 							{% endif %}*/
/* 							<th>{{ agent.email }}</th>*/
/* 							<th>{{ agent.residence }}</th>*/
/* 							<th>*/
/* 								{% if agent.localites.count > 0 %}*/
/* 									{% for localite in agent.localites %}*/
/* 										{{ localite.libL }}*/
/* 										{% if not loop.last %} - {% endif %}*/
/* 									{% endfor %}*/
/* 								{% endif %}*/
/* 							</th>*/
/* 							{% if is_granted('ROLE_GESTIONNAIRE') %}*/
/* 								<td class="row">*/
/* 									<span class="col-xs-4">*/
/* 										<a href="{{ path('cicrtg_backend_agent_modif', {'id': agent.id}) }}" class="btn btn-xs btn-default">*/
/* 											<li class="fa fa-edit" ></li>Modifier*/
/* 										</a>*/
/* 									</span>*/
/* 									<span class="col-xs-4 col-xs-offset-2">*/
/* 										{% if(is_granted('ROLE_ADMIN')) %}*/
/* 											<a href="{{ path('cicrtg_backend_agent_supp', {'id': agent.id}) }}" target-data="#supp" class="btn btn-xs btn-danger">*/
/* 												<i class="fa fa-frown-o"></i>Supprimer*/
/* 											</a>*/
/* 										{% endif %}*/
/* 									</span>*/
/* 								</td>*/
/* 							{% endif %}*/
/* 						</tr>*/
/* 						{% endfor %}*/
/* 						*/
/* 					</tbody>*/
/* 				</table>*/
/* 			</div>*/
/* 		</div>*/
/*    </div>*/
/* </div>*/
/*        */
/*      */
/* <div class="modal fade" id="prefecture">*/
/*       <div class="modal-dialog">*/
/*           <div class="modal-content">*/
/*               <div class="modal-header" style="color: #000000;">*/
/*                   <button type="button" class="close" data-dismiss="modal">x</button>*/
/*                   <h4 class="modal-title">Ajout d'un agent</h4>*/
/*               </div>*/
/*               <div class="modal-body" style="color: #000000;">*/
/* */
/*                     <form  method="post" action="{{ path('cicrtg_backend_agent') }}">*/
/*                         <span class="help-block">*/
/*                             {{ form_errors(form) }}*/
/*                         </span>*/
/*                             */
/*                            <div class="form-group">*/
/*                              <div class="row">*/
/*                                  <div class="col-md-3">*/
/*                                      <label for="groupName" > {{ form_label(form.nom, 'Nom') }} :</label>*/
/*                                  </div>*/
/* 										*/
/*                                    <small class="text-danger"> {{ form_errors(form.nom) }} </small>*/
/*                                    <div class="col-md-7 ">*/
/* 											{{ form_widget(form.nom,  {'attr': {'class':'form-control', 'Placeholder': 'Le nom de l agent'}}) }}*/
/*                                     </div>*/
/*                                </div>*/
/*                            </div>*/
/* 							*/
/* 							<div class="form-group">*/
/*                              <div class="row">*/
/*                                  <div class="col-md-3">*/
/*                                      <label for="groupName" > {{ form_label(form.prenom, 'Prenom') }} :</label>*/
/*                                  </div>*/
/* 										*/
/*                                    <small class="text-danger"> {{ form_errors(form.prenom) }} </small>*/
/*                                    <div class="col-md-7 ">*/
/* 											{{ form_widget(form.prenom,  {'attr': {'class':'form-control', 'Placeholder': 'Le prénom'}} ) }}*/
/*                                     </div>*/
/*                                </div>*/
/*                            </div>*/
/* 							*/
/* 							<div class="form-group">*/
/*                              <div class="row">*/
/*                                  <div class="col-md-3">       */
/*                                      <label for="groupName" > {{ form_label(form.sexe, 'Sexe') }} :</label>*/
/*                                  </div>*/
/* 															 */
/*                                    <small class="text-danger"> {{ form_errors(form.sexe) }} </small>*/
/*                                    <div class="col-md-7 ">*/
/* 											{{ form_widget(form.sexe,  {'attr': {'class':'form-control', 'Placeholder': 'Veuillez choisir'}} ) }}*/
/*                                     </div>*/
/*                                </div>*/
/*                            </div>*/
/* 							<div class="form-group">*/
/*                              <div class="row">*/
/*                                  <div class="col-md-3">*/
/*                                      <label for="groupName" > {{ form_label(form.tel, 'Téléphone') }} :</label>*/
/*                                  </div>*/
/* 										*/
/*                                    <small class="text-danger"> {{ form_errors(form.tel) }} </small>*/
/*                                    <div class="col-md-7 ">*/
/* 											{{ form_widget(form.tel,  {'attr': {'class':'form-control', 'Placeholder': 'xx xx xx xx'}}) }}*/
/*                                     </div>*/
/*                                </div>*/
/*                            </div>*/
/* 							*/
/* 							<div class="form-group">*/
/*                              <div class="row">*/
/*                                  <div class="col-md-3">*/
/*                                      <label for="groupName" > {{ form_label(form.codeS, 'Code secret') }} :</label>*/
/*                                  </div>*/
/* 										*/
/*                                    <small class="text-danger"> {{ form_errors(form.codeS) }} </small>*/
/*                                    <div class="col-md-7 ">*/
/* 											{{ form_widget(form.codeS,  {'attr': {'class':'form-control', 'Placeholder': 'Le code secret de l agent'}}) }}*/
/*                                     </div>*/
/*                                </div>*/
/*                            </div>*/
/* 							*/
/* 							<div class="form-group">*/
/*                              <div class="row">*/
/*                                  <div class="col-md-3">*/
/*                                      <label for="groupName" > {{ form_label(form.email, 'email') }} :</label>*/
/*                                  </div>*/
/* 										*/
/*                                    <small class="text-danger"> {{ form_errors(form.email) }} </small>*/
/*                                    <div class="col-md-7 ">*/
/* 											{{ form_widget(form.email,  {'attr': {'class':'form-control', 'Placeholder': 'l email'}}) }}*/
/*                                     </div>*/
/*                                </div>*/
/*                            </div>*/
/* 							<div class="form-group">*/
/*                              <div class="row">*/
/*                                  <div class="col-md-3">*/
/*                                      <label for="groupName" > {{ form_label(form.residence, 'Résidence') }} :</label>*/
/*                                  </div>*/
/* 										*/
/*                                    <small class="text-danger"> {{ form_errors(form.residence) }} </small>*/
/*                                    <div class="col-md-7 ">*/
/* 											{{ form_widget(form.residence,  {'attr': {'class':'form-control', 'Placeholder': 'La résidence'}}) }}*/
/*                                     </div>*/
/*                                </div>*/
/*                            </div>*/
/* 							*/
/* 							<div class="form-group">*/
/*                              <div class="row">*/
/*                                  <div class="col-md-3">*/
/*                                      <label for="groupName" > {{ form_label(form.localites, 'Localité') }} :</label>*/
/*                                  </div>*/
/* 										*/
/*                                    <small class="text-danger"> {{ form_errors(form.localites) }} </small>*/
/*                                    <div class="col-md-7 ">*/
/* 											{{ form_widget(form.localites,  {'attr': {'class':'form-control'}}) }}*/
/*                                     </div>*/
/*                                </div>*/
/*                            </div>*/
/* */
/*                           <div class="modal-footer">*/
/*                                 */
/*                                 <input type="submit" class="btn btn-primary" />*/
/*                                &nbsp; <button class="btn btn-default pull-right" data-dismiss="modal">Fermer !</button>*/
/*                           </div>*/
/*                         {{ form_rest(form) }}*/
/*                     </form>*/
/* */
/*                        */
/*               </div>*/
/* */
/*           </div>*/
/*       </div>*/
/*     </div>*/
/* */

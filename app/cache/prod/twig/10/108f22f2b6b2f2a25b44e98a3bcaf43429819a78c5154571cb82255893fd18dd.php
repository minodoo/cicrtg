<?php

/* cicrtgFrontendBundle:Frontend:index.html.twig */
class __TwigTemplate_f22cc8061ceb38c8d237c68fb8cb52e805dca9b3908d1e0948f6589ff2fb6744 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        // line 1
        $this->parent = $this->loadTemplate("cicrtgFrontendBundle::layout.html.twig", "cicrtgFrontendBundle:Frontend:index.html.twig", 1);
        $this->blocks = array(
            'title' => array($this, 'block_title'),
            'fileDarian' => array($this, 'block_fileDarian'),
            'body_bundle' => array($this, 'block_body_bundle'),
            'addFunctionDocument' => array($this, 'block_addFunctionDocument'),
        );
    }

    protected function doGetParent(array $context)
    {
        return "cicrtgFrontendBundle::layout.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $this->parent->display($context, array_merge($this->blocks, $blocks));
    }

    // line 3
    public function block_title($context, array $blocks = array())
    {
        echo " ";
        $this->displayParentBlock("title", $context, $blocks);
        echo " Acceuil ";
    }

    // line 4
    public function block_fileDarian($context, array $blocks = array())
    {
        echo " ";
        $this->displayParentBlock("fileDarian", $context, $blocks);
        echo " ";
    }

    // line 5
    public function block_body_bundle($context, array $blocks = array())
    {
        // line 6
        echo "           ";
        $this->loadTemplate("cicrtgFrontendBundle:includes:indexbody.html.twig", "cicrtgFrontendBundle:Frontend:index.html.twig", 6)->display($context);
    }

    // line 10
    public function block_addFunctionDocument($context, array $blocks = array())
    {
        // line 11
        echo "    ";
        $this->displayParentBlock("addFunctionDocument", $context, $blocks);
        echo "
    // Area Chart
    Morris.Area({
        element: 'morris-area-chart',
        data: [{
            period: '2010 Q1',
            iphone: 2666,
            ipad: null,
            itouch: 2647
        }, {
            period: '2010 Q2',
            iphone: 2778,
            ipad: 2294,
            itouch: 2441
        }, {
            period: '2010 Q3',
            iphone: 4912,
            ipad: 1969,
            itouch: 2501
        }, {
            period: '2010 Q4',
            iphone: 3767,
            ipad: 3597,
            itouch: 5689
        }, {
            period: '2011 Q1',
            iphone: 6810,
            ipad: 1914,
            itouch: 2293
        }, {
            period: '2011 Q2',
            iphone: 5670,
            ipad: 4293,
            itouch: 1881
        }, {
            period: '2011 Q3',
            iphone: 4820,
            ipad: 3795,
            itouch: 1588
        }, {
            period: '2011 Q4',
            iphone: 15073,
            ipad: 5967,
            itouch: 5175
        }, {
            period: '2012 Q1',
            iphone: 10687,
            ipad: 4460,
            itouch: 2028
        }, {
            period: '2012 Q2',
            iphone: 8432,
            ipad: 5713,
            itouch: 1791
        }],
        xkey: 'period',
        ykeys: ['iphone', 'ipad', 'itouch'],
        labels: ['iPhone', 'iPad', 'iPod Touch'],
        pointSize: 2,
        hideHover: 'auto',
        resize: true
    });

    // Donut Chart
    Morris.Donut({
        element: 'morris-donut-chart',
        data: [{
            label: \"Download Sales\",
            value: 12
        }, {
            label: \"In-Store Sales\",
            value: 30
        }, {
            label: \"Mail-Order Sales\",
            value: 20
        }],
        resize: true
    });
";
    }

    public function getTemplateName()
    {
        return "cicrtgFrontendBundle:Frontend:index.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  58 => 11,  55 => 10,  50 => 6,  47 => 5,  39 => 4,  31 => 3,  11 => 1,);
    }
}
/* {% extends "cicrtgFrontendBundle::layout.html.twig" %}*/
/* */
/* {% block title %} {{ parent() }} Acceuil {% endblock %}*/
/* {% block fileDarian %} {{ parent() }} {% endblock %}*/
/* {% block body_bundle %}*/
/*            {% include "cicrtgFrontendBundle:includes:indexbody.html.twig" %}*/
/* {% endblock %}*/
/* */
/* */
/* {% block addFunctionDocument %}*/
/*     {{ parent() }}*/
/*     // Area Chart*/
/*     Morris.Area({*/
/*         element: 'morris-area-chart',*/
/*         data: [{*/
/*             period: '2010 Q1',*/
/*             iphone: 2666,*/
/*             ipad: null,*/
/*             itouch: 2647*/
/*         }, {*/
/*             period: '2010 Q2',*/
/*             iphone: 2778,*/
/*             ipad: 2294,*/
/*             itouch: 2441*/
/*         }, {*/
/*             period: '2010 Q3',*/
/*             iphone: 4912,*/
/*             ipad: 1969,*/
/*             itouch: 2501*/
/*         }, {*/
/*             period: '2010 Q4',*/
/*             iphone: 3767,*/
/*             ipad: 3597,*/
/*             itouch: 5689*/
/*         }, {*/
/*             period: '2011 Q1',*/
/*             iphone: 6810,*/
/*             ipad: 1914,*/
/*             itouch: 2293*/
/*         }, {*/
/*             period: '2011 Q2',*/
/*             iphone: 5670,*/
/*             ipad: 4293,*/
/*             itouch: 1881*/
/*         }, {*/
/*             period: '2011 Q3',*/
/*             iphone: 4820,*/
/*             ipad: 3795,*/
/*             itouch: 1588*/
/*         }, {*/
/*             period: '2011 Q4',*/
/*             iphone: 15073,*/
/*             ipad: 5967,*/
/*             itouch: 5175*/
/*         }, {*/
/*             period: '2012 Q1',*/
/*             iphone: 10687,*/
/*             ipad: 4460,*/
/*             itouch: 2028*/
/*         }, {*/
/*             period: '2012 Q2',*/
/*             iphone: 8432,*/
/*             ipad: 5713,*/
/*             itouch: 1791*/
/*         }],*/
/*         xkey: 'period',*/
/*         ykeys: ['iphone', 'ipad', 'itouch'],*/
/*         labels: ['iPhone', 'iPad', 'iPod Touch'],*/
/*         pointSize: 2,*/
/*         hideHover: 'auto',*/
/*         resize: true*/
/*     });*/
/* */
/*     // Donut Chart*/
/*     Morris.Donut({*/
/*         element: 'morris-donut-chart',*/
/*         data: [{*/
/*             label: "Download Sales",*/
/*             value: 12*/
/*         }, {*/
/*             label: "In-Store Sales",*/
/*             value: 30*/
/*         }, {*/
/*             label: "Mail-Order Sales",*/
/*             value: 20*/
/*         }],*/
/*         resize: true*/
/*     });*/
/* {% endblock %}*/

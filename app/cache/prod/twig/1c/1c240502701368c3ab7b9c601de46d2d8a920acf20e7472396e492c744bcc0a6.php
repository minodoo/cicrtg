<?php

/* cicrtgUserBundle:includes:loginBody.html.twig */
class __TwigTemplate_a236912f050cd1fe358c3b5bc7cf810194f4561e8cda518a7a0faf995674244c extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        // line 1
        echo "<div class=\"row\">
            <div class=\"col-md-4 col-md-offset-3\">

            \t";
        // line 4
        if ((isset($context["error"]) ? $context["error"] : null)) {
            // line 5
            echo "            \t\t<div class=\"alert alert-danger\">";
            echo twig_escape_filter($this->env, $this->getAttribute((isset($context["error"]) ? $context["error"] : null), "message", array()), "html", null, true);
            echo "</div>
            \t";
        }
        // line 7
        echo "
                <div class=\"login-panel panel panel-default\">
                    <div class=\"panel-heading\">
                        <h3 class=\"panel-title\">Connectez-vous Svp</h3>
                    </div>
                    <div class=\"panel-body\">
                        <form role=\"form\" action=\"";
        // line 13
        echo $this->env->getExtension('routing')->getPath("login_check");
        echo "\" method=\"post\">
                            <fieldset>
                                <div class=\"form-group\">
                                    <input class=\"form-control\" placeholder=\"Votre pseudo\" name=\"_username\" type=\"text\" value=\"";
        // line 16
        echo twig_escape_filter($this->env, (isset($context["last_username"]) ? $context["last_username"] : null), "html", null, true);
        echo "\" autofocus>
                                </div>
                                <div class=\"form-group\">
                                    <input class=\"form-control\" placeholder=\"Password\" name=\"_password\" type=\"password\" value=\"\">
                                </div>
                                <div class=\"checkbox\">
                                    <label>
                                        <input name=\"remember\" type=\"checkbox\" value=\"Remember Me\">Remember Me
                                    </label>
                                </div>
                                <!-- Change this to a button or input when using this as a form >
                                <a href=\"index.html\" class=\"btn btn-lg btn-success btn-block\">Login</a-->
                                <input type=\"submit\" value=\"Connexion\" class=\"btn btn-lg btn-success btn-block\"/>
                            </fieldset>
                        </form>
                    </div>
                </div>
            </div>
        </div>";
    }

    public function getTemplateName()
    {
        return "cicrtgUserBundle:includes:loginBody.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  46 => 16,  40 => 13,  32 => 7,  26 => 5,  24 => 4,  19 => 1,);
    }
}
/* <div class="row">*/
/*             <div class="col-md-4 col-md-offset-3">*/
/* */
/*             	{% if error %}*/
/*             		<div class="alert alert-danger">{{ error.message }}</div>*/
/*             	{% endif %}*/
/* */
/*                 <div class="login-panel panel panel-default">*/
/*                     <div class="panel-heading">*/
/*                         <h3 class="panel-title">Connectez-vous Svp</h3>*/
/*                     </div>*/
/*                     <div class="panel-body">*/
/*                         <form role="form" action="{{ path('login_check') }}" method="post">*/
/*                             <fieldset>*/
/*                                 <div class="form-group">*/
/*                                     <input class="form-control" placeholder="Votre pseudo" name="_username" type="text" value="{{last_username}}" autofocus>*/
/*                                 </div>*/
/*                                 <div class="form-group">*/
/*                                     <input class="form-control" placeholder="Password" name="_password" type="password" value="">*/
/*                                 </div>*/
/*                                 <div class="checkbox">*/
/*                                     <label>*/
/*                                         <input name="remember" type="checkbox" value="Remember Me">Remember Me*/
/*                                     </label>*/
/*                                 </div>*/
/*                                 <!-- Change this to a button or input when using this as a form >*/
/*                                 <a href="index.html" class="btn btn-lg btn-success btn-block">Login</a-->*/
/*                                 <input type="submit" value="Connexion" class="btn btn-lg btn-success btn-block"/>*/
/*                             </fieldset>*/
/*                         </form>*/
/*                     </div>*/
/*                 </div>*/
/*             </div>*/
/*         </div>*/

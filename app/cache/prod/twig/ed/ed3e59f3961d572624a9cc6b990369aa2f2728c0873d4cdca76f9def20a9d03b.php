<?php

/* cicrtgBackendBundle:includes:zoneBody.html.twig */
class __TwigTemplate_bd4c0a72413fbbef769c5555dd8b2221479dfa6d08bcf44a00c020b0f85870af extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        // line 1
        echo "﻿";
        $context['_parent'] = $context;
        $context['_seq'] = twig_ensure_traversable($this->getAttribute($this->getAttribute($this->getAttribute((isset($context["app"]) ? $context["app"] : null), "session", array()), "flashbag", array()), "get", array(0 => "notice"), "method"));
        foreach ($context['_seq'] as $context["_key"] => $context["message"]) {
            // line 2
            echo "\t\t\t\t
    <div class=\"alert alert-success\">
        <a class=\"close fade in\" data-dismiss=\"alert\">&times;</a>
        ";
            // line 5
            echo twig_escape_filter($this->env, $context["message"], "html", null, true);
            echo "
    </div>
";
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['message'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 8
        echo "

<div class=\"tabbable tabs-right\">
    <ul class=\"nav nav-tabs\">
        <li class =\"active\"><a  href=\"#tab1\" data-toggle=\"tab\">Préfectures</a></li>
        <li><a href=\"#tab2\" data-toggle=\"tab\">Localités</a></li>
        
    </ul>

    <div class=\"tab-content\">
        <div class=\"tab-pane fade active in\" id=\"tab1\">
            <br/>
            <div class=\"row\">
               <div class=\"col-sm-12\">
                    <div class=\"panel panel-default\">
                        <div class=\"panel-heading\">
                            <h2 class=\"panel-title\"> Liste des préfectures
\t\t\t\t\t\t\t";
        // line 25
        if ($this->env->getExtension('security')->isGranted("ROLE_GESTIONNAIRE")) {
            // line 26
            echo "                            <button data-toggle=\"modal\" data-backdrop=\"false\"  href=\"#prefecture\" class=\"btn btn-info btn-sm pull-right\" style=\"margin-top: -5px;\">
                                <i class=\"fa fa-plus\"></i> Ajouter </a>
                            </button></h2>
\t\t\t\t\t\t\t";
        }
        // line 30
        echo "                        </div>
                        <div class=\"panel-body\">
                            <table  class=\"zone hover table table-bordered table-striped table-condensed\" cellspacing=\"0\" width=\"100%\" >
                                <thead>
                                    <tr>
                                        
                                        <th>Numéro</th>
                                        <th>Libellé</th>
                                        <th>Région</th>
\t\t\t\t\t\t\t\t\t\t";
        // line 39
        if ($this->env->getExtension('security')->isGranted("ROLE_GESTIONNAIRE")) {
            // line 40
            echo "\t\t\t\t\t\t\t\t\t\t\t<th>Action</th>
\t\t\t\t\t\t\t\t\t\t";
        }
        // line 42
        echo "                                    </tr>
                                </thead>
                             
                                <tfoot>
                                    <tr>
                                        
                                        
                                    </tr>
                                </tfoot>
                             
                                <tbody>
\t\t\t\t\t\t\t\t\t";
        // line 53
        $context['_parent'] = $context;
        $context['_seq'] = twig_ensure_traversable((isset($context["prefectureListe"]) ? $context["prefectureListe"] : null));
        foreach ($context['_seq'] as $context["_key"] => $context["prefecture"]) {
            // line 54
            echo "                                    <tr>
                                        <td>";
            // line 55
            echo twig_escape_filter($this->env, $this->getAttribute($context["prefecture"], "id", array()), "html", null, true);
            echo "</td>
                                        <td>";
            // line 56
            echo twig_escape_filter($this->env, $this->getAttribute($context["prefecture"], "libP", array()), "html", null, true);
            echo "</td>
                                        <td>";
            // line 57
            echo twig_escape_filter($this->env, $this->getAttribute($this->getAttribute($context["prefecture"], "region", array()), "libR", array()), "html", null, true);
            echo "</td>
\t\t\t\t\t\t\t\t\t\t
\t\t\t\t\t\t\t\t\t\t";
            // line 59
            if ($this->env->getExtension('security')->isGranted("ROLE_GESTIONNAIRE")) {
                // line 60
                echo "\t\t\t\t\t\t\t\t\t\t\t<td class=\"row\">
\t\t\t\t\t\t\t\t\t\t\t\t<span class=\"col-xs-offset-3\">
\t\t\t\t\t\t\t\t\t\t\t\t\t<a href=\"";
                // line 62
                echo twig_escape_filter($this->env, $this->env->getExtension('routing')->getPath("cicrtg_backend_prefecture_modif", array("id" => $this->getAttribute($context["prefecture"], "id", array()))), "html", null, true);
                echo "\" class=\"btn btn-sm btn-default\">
\t\t\t\t\t\t\t\t\t\t\t\t\t\tModifier
\t\t\t\t\t\t\t\t\t\t\t\t\t</a>
\t\t\t\t\t\t\t\t\t\t\t\t\t";
                // line 65
                if ($this->env->getExtension('security')->isGranted("ROLE_ADMIN")) {
                    // line 66
                    echo "\t\t\t\t\t\t\t\t\t\t\t\t\t\t<a href=\"";
                    echo twig_escape_filter($this->env, $this->env->getExtension('routing')->getPath("cicrtg_backend_prefecture_supp", array("id" => $this->getAttribute($context["prefecture"], "id", array()))), "html", null, true);
                    echo "\" target-data=\"#supp\" class=\"btn btn-sm btn-danger\">
\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t<i class=\"fa fa-frown-o\"></i>Supprimer
\t\t\t\t\t\t\t\t\t\t\t\t\t\t</a>
\t\t\t\t\t\t\t\t\t\t\t\t\t";
                }
                // line 70
                echo "\t\t\t\t\t\t\t\t\t\t\t\t</span>
\t\t\t\t\t\t\t\t\t\t\t
\t\t\t\t\t\t\t\t\t\t\t</td>
\t\t\t\t\t\t\t\t\t\t";
            }
            // line 74
            echo "                                    </tr>
\t\t\t\t\t\t\t\t";
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['prefecture'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 76
        echo "                                </tbody>
                            </table>
                        </div>
                    </div>
               </div>
            </div>
        </div>
     
        <div class=\"tab-pane fade\" id=\"tab2\">
            <br/>
            <div class=\"row\">
                <div class=\"col-sm-12\">
                    <div class=\"panel panel-default\">
                        <div class=\"panel-heading\">
                            <h2 class=\"panel-title\"> Liste des localités
\t\t\t\t\t\t\t";
        // line 91
        if ($this->env->getExtension('security')->isGranted("ROLE_GESTIONNAIRE")) {
            // line 92
            echo "                                <button data-toggle=\"modal\" href=\"#localite\" class=\"btn btn-info btn-sm pull-right\" style=\"margin-top: -5px;\">
                                <i class=\"fa fa-plus\"></i> Ajouter </a>
\t\t\t\t\t\t\t\t</button>
\t\t\t\t\t\t\t";
        }
        // line 96
        echo "                            </h2>
                        </div>
                        <div class=\"panel-body\">
                            <table  class=\"zone hover table table-bordered table-striped table-condensed\" cellspacing=\"0\" width=\"100%\" >
                                <thead>
                                    <tr>
                                        
                                        <th></th>
                                        <th>Libellé</th>
                                        <th>Observation</th>
                                        <th>Prefecture</th>
                                        <th>Longitude</th>
                                        <th>Latitude</th>
                                        <th>Altitude</th>
\t\t\t\t\t\t\t\t\t\t";
        // line 110
        if ($this->env->getExtension('security')->isGranted("ROLE_GESTIONNAIRE")) {
            // line 111
            echo "\t\t\t\t\t\t\t\t\t\t\t<th>Action</th>
\t\t\t\t\t\t\t\t\t\t";
        }
        // line 113
        echo "                                    </tr>
                                </thead>
                             
                                <tfoot>
                                    <tr>
                                        
                                        
                                    </tr>
                                </tfoot>
                             
                                <tbody>
\t\t\t\t\t\t\t\t\t";
        // line 124
        $context['_parent'] = $context;
        $context['_seq'] = twig_ensure_traversable((isset($context["localiteListe"]) ? $context["localiteListe"] : null));
        foreach ($context['_seq'] as $context["_key"] => $context["localite"]) {
            // line 125
            echo "                                    <tr>
                                        <td></td>
                                        <td> ";
            // line 127
            echo twig_escape_filter($this->env, $this->getAttribute($context["localite"], "libL", array()), "html", null, true);
            echo "</td>
                                        <td>";
            // line 128
            echo twig_escape_filter($this->env, $this->getAttribute($context["localite"], "observation", array()), "html", null, true);
            echo "</td>
                                        <td>";
            // line 129
            echo twig_escape_filter($this->env, $this->getAttribute($this->getAttribute($context["localite"], "prefecture", array()), "libP", array()), "html", null, true);
            echo "</td>
                                        <td>";
            // line 130
            echo twig_escape_filter($this->env, $this->getAttribute($this->getAttribute($context["localite"], "position", array()), "longitude", array()), "html", null, true);
            echo "</td>
                                        <td>";
            // line 131
            echo twig_escape_filter($this->env, $this->getAttribute($this->getAttribute($context["localite"], "position", array()), "latitude", array()), "html", null, true);
            echo "</td>
                                        <td>";
            // line 132
            echo twig_escape_filter($this->env, $this->getAttribute($this->getAttribute($context["localite"], "position", array()), "altitude", array()), "html", null, true);
            echo "</td>
\t\t\t\t\t\t\t\t\t\t";
            // line 133
            if ($this->env->getExtension('security')->isGranted("ROLE_GESTIONNAIRE")) {
                // line 134
                echo "                                        <td class=\"row\">
\t\t\t\t\t\t\t\t\t\t\t<span class=\"col-xs-offset-3\">
\t\t\t\t\t\t\t\t\t\t\t\t<a href=\"";
                // line 136
                echo twig_escape_filter($this->env, $this->env->getExtension('routing')->getPath("cicrtg_backend_localite_modif", array("id" => $this->getAttribute($context["localite"], "id", array()))), "html", null, true);
                echo "\" class=\"btn btn-xs btn-default\">
\t\t\t\t\t\t\t\t\t\t\t\t\t<li class=\"fa fa-edit\" ></li>Modifier
\t\t\t\t\t\t\t\t\t\t\t\t</a>
\t\t\t\t\t\t\t\t\t\t\t\t";
                // line 139
                if ($this->env->getExtension('security')->isGranted("ROLE_ADMIN")) {
                    // line 140
                    echo "\t\t\t\t\t\t\t\t\t\t\t\t\t<a href=\"";
                    echo twig_escape_filter($this->env, $this->env->getExtension('routing')->getPath("cicrtg_backend_localite_supp", array("id" => $this->getAttribute($context["localite"], "id", array()))), "html", null, true);
                    echo "\" target-data=\"#supp\" class=\"btn btn-xs btn-danger\">
\t\t\t\t\t\t\t\t\t\t\t\t\t\t<i class=\"fa fa-frown-o\"></i>Supprimer
\t\t\t\t\t\t\t\t\t\t\t\t\t</a>
\t\t\t\t\t\t\t\t\t\t\t\t";
                }
                // line 144
                echo "\t\t\t\t\t\t\t\t\t\t\t</span>
                                        </td>
\t\t\t\t\t\t\t\t\t\t";
            }
            // line 147
            echo "                                    </tr>
\t\t\t\t\t\t\t\t\t";
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['localite'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 149
        echo "                                </tbody>
                            </table>
                            
                        </div>
                    </div>
               </div>
            </div>
        </div>
    </div>   

<div class=\"modal fade\" id=\"prefecture\">
      <div class=\"modal-dialog\">
          <div class=\"modal-content\">
              <div class=\"modal-header\" style=\"color: #000000;\">
                  <button type=\"button\" class=\"close\" data-dismiss=\"modal\">x</button>
                  <h4 class=\"modal-title\">Ajout de Préfecture</h4>
              </div>
              <div class=\"modal-body\" style=\"color: #000000;\">

                    <form  method=\"post\" action=\"";
        // line 168
        echo $this->env->getExtension('routing')->getPath("cicrtg_backend_zone");
        echo "\">
                        <span class=\"help-block\">
                            ";
        // line 170
        echo $this->env->getExtension('form')->renderer->searchAndRenderBlock((isset($context["formP"]) ? $context["formP"] : null), 'errors');
        echo "
                        </span>
                            
                           <div class=\"form-group\">
                             <div class=\"row\">
                                 <div class=\"col-md-3\">
                                     <label for=\"groupName\" >";
        // line 176
        echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute((isset($context["formP"]) ? $context["formP"] : null), "libP", array()), 'label', array("label" => "Le libellé"));
        echo " :</label>
                                 </div>

                                   <small class=\"text-danger\"> ";
        // line 179
        echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute((isset($context["formP"]) ? $context["formP"] : null), "libP", array()), 'errors');
        echo " </small>
                                   <div class=\"col-md-7 \">
                                        ";
        // line 181
        echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute((isset($context["formP"]) ? $context["formP"] : null), "libP", array()), 'widget', array("attr" => array("class" => "form-control", "placeholder" => "La préfecture")));
        echo "
                                    </div>
                               </div>
                           </div>

                          <div class=\"form-group\">
                              <div class=\"row\">
                                  <div class=\"col-sm-3\">
                                      <label for=\"id\" >";
        // line 189
        echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute((isset($context["formP"]) ? $context["formP"] : null), "region", array()), 'label', array("label" => "la région"));
        echo " : </label>
                                  </div>
                                    <small class=\"text-danger\"> ";
        // line 191
        echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute((isset($context["formP"]) ? $context["formP"] : null), "region", array()), 'errors');
        echo " </small>
                                    <div class=\"col-sm-7 form-group\">
                                        ";
        // line 193
        echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute((isset($context["formP"]) ? $context["formP"] : null), "region", array()), 'widget', array("attr" => array("class" => "form-control")));
        echo "
                                    </div>
                                </div>
                            </div>


                          <div class=\"modal-footer\">
                                
                                <input type=\"submit\" class=\"btn btn-primary\" />
                               &nbsp; <button class=\"btn btn-default pull-right\" data-dismiss=\"modal\">Fermer !</button>
                          </div>
                        ";
        // line 204
        echo $this->env->getExtension('form')->renderer->searchAndRenderBlock((isset($context["formP"]) ? $context["formP"] : null), 'rest');
        echo "
                    </form>

                       
              </div>

          </div>
      </div>
    </div>

<div class=\"modal fade\" id=\"localite\">
      <div class=\"modal-dialog\">
          <div class=\"modal-content\">
              <div class=\"modal-header\" style=\"color: #000000;\">
                  <button type=\"button\" class=\"close\" data-dismiss=\"modal\">x</button>
                  <h4 class=\"modal-title\">Ajout de localité</h4>
              </div>
              <div class=\"modal-body\" style=\"color: #000000;\">
                    <form  method=\"post\" action=\"";
        // line 222
        echo $this->env->getExtension('routing')->getPath("cicrtg_backend_zone");
        echo "\">
                       <lavel class=\"\">";
        // line 223
        echo $this->env->getExtension('form')->renderer->searchAndRenderBlock((isset($context["formL"]) ? $context["formL"] : null), 'errors');
        echo "</lavel>
\t\t
                           <div class=\"form-group\">
                             <div class=\"row\">
                                 <div class=\"col-md-3\">
                                     <label for=\"groupName\" >";
        // line 228
        echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute((isset($context["formL"]) ? $context["formL"] : null), "libL", array()), 'label', array("label" => "Le libelle"));
        echo " </label>
                                 </div>

                                   
\t\t\t\t\t\t\t\t   <small class=\"text-danger\"> ";
        // line 232
        echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute((isset($context["formL"]) ? $context["formL"] : null), "libL", array()), 'errors');
        echo " </small>
                                   <div class=\"col-md-7\">

                                        ";
        // line 235
        echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute((isset($context["formL"]) ? $context["formL"] : null), "libL", array()), 'widget', array("attr" => array("class" => "form-control", "Placeholder" => "le libellé de la localité")));
        echo "
                                   </div>
                               </div>
                           </div>
\t\t\t\t\t\t\t
\t\t\t\t\t\t\t<div class=\"form-group\">
                             <div class=\"row\">
                                 <div class=\"col-md-3\">
                                     <label for=\"groupName\" >";
        // line 243
        echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute((isset($context["formL"]) ? $context["formL"] : null), "observation", array()), 'label', array("label" => "Observation"));
        echo " </label>
                                 </div>

                                   <small class=\"text-danger\"> ";
        // line 246
        echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute((isset($context["formL"]) ? $context["formL"] : null), "observation", array()), 'errors');
        echo " </small>
                                   <div class=\"col-md-7\">

                                        ";
        // line 249
        echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute((isset($context["formL"]) ? $context["formL"] : null), "observation", array()), 'widget', array("attr" => array("class" => "form-control", "Placeholder" => "Votre observation")));
        echo "
                                   </div>
                               </div>
                           </div>
\t\t\t\t\t\t\t
\t\t\t\t\t\t\t
\t\t\t\t\t\t\t<div class=\"form-group\">
                             <div class=\"row\">
                                 <div class=\"col-md-3\">
                                     <label for=\"groupName\" >";
        // line 258
        echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute((isset($context["formL"]) ? $context["formL"] : null), "prefecture", array()), 'label', array("label" => "La préfecture"));
        echo " </label>
                                 </div>

                                   <small class=\"text-danger\"> ";
        // line 261
        echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute((isset($context["formL"]) ? $context["formL"] : null), "prefecture", array()), 'errors');
        echo " </small>
                                   <div class=\"col-md-7\">

                                        ";
        // line 264
        echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute((isset($context["formL"]) ? $context["formL"] : null), "prefecture", array()), 'widget', array("attr" => array("class" => "form-control")));
        echo "
                                   </div>
                               </div>
                           </div><br/>
\t\t\t\t\t\t\t
\t\t\t\t\t\t\t<fieldset>
\t\t\t\t\t\t\t\t<legend>Position</legend>
\t\t\t\t\t\t\t\t<div class=\"form-group\">
                             <div class=\"row\">
                                 <div class=\"col-md-3\">
                                     <label for=\"groupName\" >";
        // line 274
        echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute($this->getAttribute((isset($context["formL"]) ? $context["formL"] : null), "position", array()), "longitude", array()), 'label', array("label" => "Le longitude"));
        echo " </label>
                                 </div>

                                   <small class=\"text-danger\"> ";
        // line 277
        echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute($this->getAttribute((isset($context["formL"]) ? $context["formL"] : null), "position", array()), "longitude", array()), 'errors');
        echo " </small>
                                   <div class=\"col-md-7\">

                                        ";
        // line 280
        echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute($this->getAttribute((isset($context["formL"]) ? $context["formL"] : null), "position", array()), "longitude", array()), 'widget', array("attr" => array("class" => "form-control", "Placeholder" => "Le longitude")));
        echo "
                                   </div>
                               </div>
                           </div>
\t\t\t\t\t\t\t
\t\t\t\t\t\t\t<div class=\"form-group\">
                             <div class=\"row\">
                                 <div class=\"col-md-3\">
                                     <label for=\"groupName\" >";
        // line 288
        echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute($this->getAttribute((isset($context["formL"]) ? $context["formL"] : null), "position", array()), "latitude", array()), 'label', array("label" => "La latitude"));
        echo " </label>
                                 </div>

                                   <small class=\"text-danger\"> ";
        // line 291
        echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute($this->getAttribute((isset($context["formL"]) ? $context["formL"] : null), "position", array()), "latitude", array()), 'errors');
        echo " </small>
                                   <div class=\"col-md-7\">

                                        ";
        // line 294
        echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute($this->getAttribute((isset($context["formL"]) ? $context["formL"] : null), "position", array()), "latitude", array()), 'widget', array("attr" => array("class" => "form-control", "Placeholder" => "La latitude")));
        echo "
                                   </div>
                               </div>
                           </div>
\t\t\t\t\t\t\t
\t\t\t\t\t\t\t<div class=\"form-group\">
                             <div class=\"row\">
                                 <div class=\"col-md-3\">
                                     <label for=\"groupName\" >";
        // line 302
        echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute($this->getAttribute((isset($context["formL"]) ? $context["formL"] : null), "position", array()), "altitude", array()), 'label', array("label" => "L altitude "));
        echo " </label>
                                 </div>

                                   <small class=\"text-danger\"> ";
        // line 305
        echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute($this->getAttribute((isset($context["formL"]) ? $context["formL"] : null), "position", array()), "altitude", array()), 'errors');
        echo " </small>
                                   <div class=\"col-md-7\">

                                        ";
        // line 308
        echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute($this->getAttribute((isset($context["formL"]) ? $context["formL"] : null), "position", array()), "altitude", array()), 'widget', array("attr" => array("class" => "form-control", "Placeholder" => "l altitude")));
        echo "
                                   </div>
                               </div>
                           </div>
\t\t\t\t\t\t\t</fieldset>
\t\t\t\t\t\t\t
                          <div class=\"modal-footer\">
                            <input type=\"submit\" class=\"btn btn-primary\" />
                            &nbsp; <button class=\"btn btn-default pull-right\" data-dismiss=\"modal\">Fermer !</button>
                          </div>

                    
\t\t\t\t\t";
        // line 320
        echo $this->env->getExtension('form')->renderer->searchAndRenderBlock((isset($context["formL"]) ? $context["formL"] : null), 'rest');
        echo "
                    </form>
                       
              </div>

          </div>
      </div>
    </div>

<div class=\"row\">

</div>";
    }

    public function getTemplateName()
    {
        return "cicrtgBackendBundle:includes:zoneBody.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  536 => 320,  521 => 308,  515 => 305,  509 => 302,  498 => 294,  492 => 291,  486 => 288,  475 => 280,  469 => 277,  463 => 274,  450 => 264,  444 => 261,  438 => 258,  426 => 249,  420 => 246,  414 => 243,  403 => 235,  397 => 232,  390 => 228,  382 => 223,  378 => 222,  357 => 204,  343 => 193,  338 => 191,  333 => 189,  322 => 181,  317 => 179,  311 => 176,  302 => 170,  297 => 168,  276 => 149,  269 => 147,  264 => 144,  256 => 140,  254 => 139,  248 => 136,  244 => 134,  242 => 133,  238 => 132,  234 => 131,  230 => 130,  226 => 129,  222 => 128,  218 => 127,  214 => 125,  210 => 124,  197 => 113,  193 => 111,  191 => 110,  175 => 96,  169 => 92,  167 => 91,  150 => 76,  143 => 74,  137 => 70,  129 => 66,  127 => 65,  121 => 62,  117 => 60,  115 => 59,  110 => 57,  106 => 56,  102 => 55,  99 => 54,  95 => 53,  82 => 42,  78 => 40,  76 => 39,  65 => 30,  59 => 26,  57 => 25,  38 => 8,  29 => 5,  24 => 2,  19 => 1,);
    }
}
/* ﻿{% for message in app.session.flashbag.get('notice') %}*/
/* 				*/
/*     <div class="alert alert-success">*/
/*         <a class="close fade in" data-dismiss="alert">&times;</a>*/
/*         {{ message }}*/
/*     </div>*/
/* {% endfor %}*/
/* */
/* */
/* <div class="tabbable tabs-right">*/
/*     <ul class="nav nav-tabs">*/
/*         <li class ="active"><a  href="#tab1" data-toggle="tab">Préfectures</a></li>*/
/*         <li><a href="#tab2" data-toggle="tab">Localités</a></li>*/
/*         */
/*     </ul>*/
/* */
/*     <div class="tab-content">*/
/*         <div class="tab-pane fade active in" id="tab1">*/
/*             <br/>*/
/*             <div class="row">*/
/*                <div class="col-sm-12">*/
/*                     <div class="panel panel-default">*/
/*                         <div class="panel-heading">*/
/*                             <h2 class="panel-title"> Liste des préfectures*/
/* 							{% if(is_granted('ROLE_GESTIONNAIRE')) %}*/
/*                             <button data-toggle="modal" data-backdrop="false"  href="#prefecture" class="btn btn-info btn-sm pull-right" style="margin-top: -5px;">*/
/*                                 <i class="fa fa-plus"></i> Ajouter </a>*/
/*                             </button></h2>*/
/* 							{% endif %}*/
/*                         </div>*/
/*                         <div class="panel-body">*/
/*                             <table  class="zone hover table table-bordered table-striped table-condensed" cellspacing="0" width="100%" >*/
/*                                 <thead>*/
/*                                     <tr>*/
/*                                         */
/*                                         <th>Numéro</th>*/
/*                                         <th>Libellé</th>*/
/*                                         <th>Région</th>*/
/* 										{% if(is_granted('ROLE_GESTIONNAIRE')) %}*/
/* 											<th>Action</th>*/
/* 										{% endif %}*/
/*                                     </tr>*/
/*                                 </thead>*/
/*                              */
/*                                 <tfoot>*/
/*                                     <tr>*/
/*                                         */
/*                                         */
/*                                     </tr>*/
/*                                 </tfoot>*/
/*                              */
/*                                 <tbody>*/
/* 									{% for prefecture in prefectureListe  %}*/
/*                                     <tr>*/
/*                                         <td>{{ prefecture.id }}</td>*/
/*                                         <td>{{ prefecture.libP }}</td>*/
/*                                         <td>{{ prefecture.region.libR }}</td>*/
/* 										*/
/* 										{% if(is_granted('ROLE_GESTIONNAIRE')) %}*/
/* 											<td class="row">*/
/* 												<span class="col-xs-offset-3">*/
/* 													<a href="{{ path('cicrtg_backend_prefecture_modif', {'id': prefecture.id}) }}" class="btn btn-sm btn-default">*/
/* 														Modifier*/
/* 													</a>*/
/* 													{% if(is_granted('ROLE_ADMIN')) %}*/
/* 														<a href="{{ path('cicrtg_backend_prefecture_supp', {'id': prefecture.id}) }}" target-data="#supp" class="btn btn-sm btn-danger">*/
/* 															<i class="fa fa-frown-o"></i>Supprimer*/
/* 														</a>*/
/* 													{% endif %}*/
/* 												</span>*/
/* 											*/
/* 											</td>*/
/* 										{% endif %}*/
/*                                     </tr>*/
/* 								{% endfor %}*/
/*                                 </tbody>*/
/*                             </table>*/
/*                         </div>*/
/*                     </div>*/
/*                </div>*/
/*             </div>*/
/*         </div>*/
/*      */
/*         <div class="tab-pane fade" id="tab2">*/
/*             <br/>*/
/*             <div class="row">*/
/*                 <div class="col-sm-12">*/
/*                     <div class="panel panel-default">*/
/*                         <div class="panel-heading">*/
/*                             <h2 class="panel-title"> Liste des localités*/
/* 							{% if(is_granted('ROLE_GESTIONNAIRE')) %}*/
/*                                 <button data-toggle="modal" href="#localite" class="btn btn-info btn-sm pull-right" style="margin-top: -5px;">*/
/*                                 <i class="fa fa-plus"></i> Ajouter </a>*/
/* 								</button>*/
/* 							{% endif %}*/
/*                             </h2>*/
/*                         </div>*/
/*                         <div class="panel-body">*/
/*                             <table  class="zone hover table table-bordered table-striped table-condensed" cellspacing="0" width="100%" >*/
/*                                 <thead>*/
/*                                     <tr>*/
/*                                         */
/*                                         <th></th>*/
/*                                         <th>Libellé</th>*/
/*                                         <th>Observation</th>*/
/*                                         <th>Prefecture</th>*/
/*                                         <th>Longitude</th>*/
/*                                         <th>Latitude</th>*/
/*                                         <th>Altitude</th>*/
/* 										{% if(is_granted('ROLE_GESTIONNAIRE')) %}*/
/* 											<th>Action</th>*/
/* 										{% endif %}*/
/*                                     </tr>*/
/*                                 </thead>*/
/*                              */
/*                                 <tfoot>*/
/*                                     <tr>*/
/*                                         */
/*                                         */
/*                                     </tr>*/
/*                                 </tfoot>*/
/*                              */
/*                                 <tbody>*/
/* 									{% for localite in  localiteListe %}*/
/*                                     <tr>*/
/*                                         <td></td>*/
/*                                         <td> {{ localite.libL }}</td>*/
/*                                         <td>{{ localite.observation }}</td>*/
/*                                         <td>{{ localite.prefecture.libP}}</td>*/
/*                                         <td>{{ localite.position.longitude}}</td>*/
/*                                         <td>{{ localite.position.latitude}}</td>*/
/*                                         <td>{{ localite.position.altitude}}</td>*/
/* 										{% if(is_granted('ROLE_GESTIONNAIRE')) %}*/
/*                                         <td class="row">*/
/* 											<span class="col-xs-offset-3">*/
/* 												<a href="{{ path('cicrtg_backend_localite_modif', {'id': localite.id}) }}" class="btn btn-xs btn-default">*/
/* 													<li class="fa fa-edit" ></li>Modifier*/
/* 												</a>*/
/* 												{% if(is_granted('ROLE_ADMIN')) %}*/
/* 													<a href="{{ path('cicrtg_backend_localite_supp', {'id': localite.id}) }}" target-data="#supp" class="btn btn-xs btn-danger">*/
/* 														<i class="fa fa-frown-o"></i>Supprimer*/
/* 													</a>*/
/* 												{% endif %}*/
/* 											</span>*/
/*                                         </td>*/
/* 										{% endif %}*/
/*                                     </tr>*/
/* 									{% endfor %}*/
/*                                 </tbody>*/
/*                             </table>*/
/*                             */
/*                         </div>*/
/*                     </div>*/
/*                </div>*/
/*             </div>*/
/*         </div>*/
/*     </div>   */
/* */
/* <div class="modal fade" id="prefecture">*/
/*       <div class="modal-dialog">*/
/*           <div class="modal-content">*/
/*               <div class="modal-header" style="color: #000000;">*/
/*                   <button type="button" class="close" data-dismiss="modal">x</button>*/
/*                   <h4 class="modal-title">Ajout de Préfecture</h4>*/
/*               </div>*/
/*               <div class="modal-body" style="color: #000000;">*/
/* */
/*                     <form  method="post" action="{{ path('cicrtg_backend_zone') }}">*/
/*                         <span class="help-block">*/
/*                             {{ form_errors(formP) }}*/
/*                         </span>*/
/*                             */
/*                            <div class="form-group">*/
/*                              <div class="row">*/
/*                                  <div class="col-md-3">*/
/*                                      <label for="groupName" >{{ form_label(formP.libP, "Le libellé") }} :</label>*/
/*                                  </div>*/
/* */
/*                                    <small class="text-danger"> {{ form_errors(formP.libP) }} </small>*/
/*                                    <div class="col-md-7 ">*/
/*                                         {{ form_widget(formP.libP, { 'attr': {'class':'form-control', 'placeholder':'La préfecture'} }) }}*/
/*                                     </div>*/
/*                                </div>*/
/*                            </div>*/
/* */
/*                           <div class="form-group">*/
/*                               <div class="row">*/
/*                                   <div class="col-sm-3">*/
/*                                       <label for="id" >{{ form_label(formP.region, "la région") }} : </label>*/
/*                                   </div>*/
/*                                     <small class="text-danger"> {{ form_errors(formP.region) }} </small>*/
/*                                     <div class="col-sm-7 form-group">*/
/*                                         {{ form_widget(formP.region, { 'attr': {'class':'form-control'} }) }}*/
/*                                     </div>*/
/*                                 </div>*/
/*                             </div>*/
/* */
/* */
/*                           <div class="modal-footer">*/
/*                                 */
/*                                 <input type="submit" class="btn btn-primary" />*/
/*                                &nbsp; <button class="btn btn-default pull-right" data-dismiss="modal">Fermer !</button>*/
/*                           </div>*/
/*                         {{ form_rest(formP) }}*/
/*                     </form>*/
/* */
/*                        */
/*               </div>*/
/* */
/*           </div>*/
/*       </div>*/
/*     </div>*/
/* */
/* <div class="modal fade" id="localite">*/
/*       <div class="modal-dialog">*/
/*           <div class="modal-content">*/
/*               <div class="modal-header" style="color: #000000;">*/
/*                   <button type="button" class="close" data-dismiss="modal">x</button>*/
/*                   <h4 class="modal-title">Ajout de localité</h4>*/
/*               </div>*/
/*               <div class="modal-body" style="color: #000000;">*/
/*                     <form  method="post" action="{{ path('cicrtg_backend_zone') }}">*/
/*                        <lavel class="">{{ form_errors(formL) }}</lavel>*/
/* 		*/
/*                            <div class="form-group">*/
/*                              <div class="row">*/
/*                                  <div class="col-md-3">*/
/*                                      <label for="groupName" >{{ form_label(formL.libL, 'Le libelle') }} </label>*/
/*                                  </div>*/
/* */
/*                                    */
/* 								   <small class="text-danger"> {{ form_errors(formL.libL) }} </small>*/
/*                                    <div class="col-md-7">*/
/* */
/*                                         {{ form_widget(formL.libL, {'attr': {'class':'form-control', 'Placeholder': 'le libellé de la localité'}}) }}*/
/*                                    </div>*/
/*                                </div>*/
/*                            </div>*/
/* 							*/
/* 							<div class="form-group">*/
/*                              <div class="row">*/
/*                                  <div class="col-md-3">*/
/*                                      <label for="groupName" >{{ form_label(formL.observation, 'Observation') }} </label>*/
/*                                  </div>*/
/* */
/*                                    <small class="text-danger"> {{ form_errors(formL.observation) }} </small>*/
/*                                    <div class="col-md-7">*/
/* */
/*                                         {{ form_widget(formL.observation, {'attr': {'class':'form-control', 'Placeholder': 'Votre observation'}}) }}*/
/*                                    </div>*/
/*                                </div>*/
/*                            </div>*/
/* 							*/
/* 							*/
/* 							<div class="form-group">*/
/*                              <div class="row">*/
/*                                  <div class="col-md-3">*/
/*                                      <label for="groupName" >{{ form_label(formL.prefecture, 'La préfecture') }} </label>*/
/*                                  </div>*/
/* */
/*                                    <small class="text-danger"> {{ form_errors(formL.prefecture) }} </small>*/
/*                                    <div class="col-md-7">*/
/* */
/*                                         {{ form_widget(formL.prefecture, {'attr': {'class':'form-control'}}) }}*/
/*                                    </div>*/
/*                                </div>*/
/*                            </div><br/>*/
/* 							*/
/* 							<fieldset>*/
/* 								<legend>Position</legend>*/
/* 								<div class="form-group">*/
/*                              <div class="row">*/
/*                                  <div class="col-md-3">*/
/*                                      <label for="groupName" >{{ form_label(formL.position.longitude, 'Le longitude') }} </label>*/
/*                                  </div>*/
/* */
/*                                    <small class="text-danger"> {{ form_errors(formL.position.longitude) }} </small>*/
/*                                    <div class="col-md-7">*/
/* */
/*                                         {{ form_widget(formL.position.longitude, {'attr': {'class':'form-control', 'Placeholder': 'Le longitude'}}) }}*/
/*                                    </div>*/
/*                                </div>*/
/*                            </div>*/
/* 							*/
/* 							<div class="form-group">*/
/*                              <div class="row">*/
/*                                  <div class="col-md-3">*/
/*                                      <label for="groupName" >{{ form_label(formL.position.latitude, 'La latitude') }} </label>*/
/*                                  </div>*/
/* */
/*                                    <small class="text-danger"> {{ form_errors(formL.position.latitude) }} </small>*/
/*                                    <div class="col-md-7">*/
/* */
/*                                         {{ form_widget(formL.position.latitude, {'attr': {'class':'form-control', 'Placeholder': 'La latitude'}}) }}*/
/*                                    </div>*/
/*                                </div>*/
/*                            </div>*/
/* 							*/
/* 							<div class="form-group">*/
/*                              <div class="row">*/
/*                                  <div class="col-md-3">*/
/*                                      <label for="groupName" >{{ form_label(formL.position.altitude, 'L altitude ') }} </label>*/
/*                                  </div>*/
/* */
/*                                    <small class="text-danger"> {{ form_errors(formL.position.altitude) }} </small>*/
/*                                    <div class="col-md-7">*/
/* */
/*                                         {{ form_widget(formL.position.altitude, {'attr': {'class':'form-control', 'Placeholder': 'l altitude'}}) }}*/
/*                                    </div>*/
/*                                </div>*/
/*                            </div>*/
/* 							</fieldset>*/
/* 							*/
/*                           <div class="modal-footer">*/
/*                             <input type="submit" class="btn btn-primary" />*/
/*                             &nbsp; <button class="btn btn-default pull-right" data-dismiss="modal">Fermer !</button>*/
/*                           </div>*/
/* */
/*                     */
/* 					{{ form_rest(formL) }}*/
/*                     </form>*/
/*                        */
/*               </div>*/
/* */
/*           </div>*/
/*       </div>*/
/*     </div>*/
/* */
/* <div class="row">*/
/* */
/* </div>*/

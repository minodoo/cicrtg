<?php

/* cicrtgBackendBundle:backend:supPrefecture.html.twig */
class __TwigTemplate_dfbeee50d42eff41d17fc3a15964ce8849be6e7199e9e2084f6b70b28eb41fbd extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        // line 1
        $this->parent = $this->loadTemplate("cicrtgBackendBundle::layout.html.twig", "cicrtgBackendBundle:backend:supPrefecture.html.twig", 1);
        $this->blocks = array(
            'title' => array($this, 'block_title'),
            'fileDarian' => array($this, 'block_fileDarian'),
            'body_bundle' => array($this, 'block_body_bundle'),
        );
    }

    protected function doGetParent(array $context)
    {
        return "cicrtgBackendBundle::layout.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $this->parent->display($context, array_merge($this->blocks, $blocks));
    }

    // line 3
    public function block_title($context, array $blocks = array())
    {
        echo " ";
        $this->displayParentBlock("title", $context, $blocks);
        echo " zones ";
    }

    // line 4
    public function block_fileDarian($context, array $blocks = array())
    {
        echo " ";
        $this->displayParentBlock("fileDarian", $context, $blocks);
        echo " zones / Prefecture / Suppression ";
    }

    // line 5
    public function block_body_bundle($context, array $blocks = array())
    {
        // line 6
        echo "
<div class=\"row\">
<div class=\"col-xs-offset-3 col-sm-6\">
    <div class=\"panel panel-warning\">
        <div class=\"panel-heading\">
            <h2 class=\"panel-title\"> Suppression
            </h2>
        </div>
        <div class=\"panel-body\">
            Souhaitez-vous vraiment supprimer la préfecture de <span class=\"label label-warning\">";
        // line 15
        echo twig_escape_filter($this->env, $this->getAttribute((isset($context["prefecture"]) ? $context["prefecture"] : null), "libP", array()), "html", null, true);
        echo " ??</span>
        </div>
        
        <div class=\"panel-footer\">
            <form action=\"";
        // line 19
        echo twig_escape_filter($this->env, $this->env->getExtension('routing')->getPath("cicrtg_backend_prefecture_supp", array("id" => $this->getAttribute((isset($context["prefecture"]) ? $context["prefecture"] : null), "id", array()))), "html", null, true);
        echo "\" method=\"POST\">
                <span class=\"col-xs-offset-3\">
                    
                    <input type=\"submit\" class=\"btn btn-danger \" value=\"Supprimer\" />
                    &nbsp; <a href=\"";
        // line 23
        echo $this->env->getExtension('routing')->getPath("cicrtg_backend_zone");
        echo "\" class=\"btn btn-default\"><i class=\"fa fa-long-arrow-left\"></i>Retour</a>
\t\t\t\t\t\t\t 
                </span>
                ";
        // line 26
        echo $this->env->getExtension('form')->renderer->searchAndRenderBlock((isset($context["formSupp"]) ? $context["formSupp"] : null), 'rest');
        echo "
            </form>
        </div>
    </div>
</div>
</div>
";
    }

    public function getTemplateName()
    {
        return "cicrtgBackendBundle:backend:supPrefecture.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  80 => 26,  74 => 23,  67 => 19,  60 => 15,  49 => 6,  46 => 5,  38 => 4,  30 => 3,  11 => 1,);
    }
}
/* {% extends "cicrtgBackendBundle::layout.html.twig" %}*/
/* */
/* {% block title %} {{ parent() }} zones {% endblock %}*/
/* {% block fileDarian %} {{ parent() }} zones / Prefecture / Suppression {% endblock %}*/
/* {% block body_bundle %}*/
/* */
/* <div class="row">*/
/* <div class="col-xs-offset-3 col-sm-6">*/
/*     <div class="panel panel-warning">*/
/*         <div class="panel-heading">*/
/*             <h2 class="panel-title"> Suppression*/
/*             </h2>*/
/*         </div>*/
/*         <div class="panel-body">*/
/*             Souhaitez-vous vraiment supprimer la préfecture de <span class="label label-warning">{{ prefecture.libP }} ??</span>*/
/*         </div>*/
/*         */
/*         <div class="panel-footer">*/
/*             <form action="{{ path('cicrtg_backend_prefecture_supp', {'id': prefecture.id}) }}" method="POST">*/
/*                 <span class="col-xs-offset-3">*/
/*                     */
/*                     <input type="submit" class="btn btn-danger " value="Supprimer" />*/
/*                     &nbsp; <a href="{{ path('cicrtg_backend_zone') }}" class="btn btn-default"><i class="fa fa-long-arrow-left"></i>Retour</a>*/
/* 							 */
/*                 </span>*/
/*                 {{ form_rest(formSupp) }}*/
/*             </form>*/
/*         </div>*/
/*     </div>*/
/* </div>*/
/* </div>*/
/* {% endblock %}*/

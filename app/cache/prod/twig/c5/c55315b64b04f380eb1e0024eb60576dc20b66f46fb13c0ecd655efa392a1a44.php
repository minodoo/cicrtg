<?php

/* cicrtgBackendBundle:backend:agentModif.html.twig */
class __TwigTemplate_3bb8976a24336ae1ec18ec47dba9976e68659afac29de204146cb058447b8029 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        // line 1
        $this->parent = $this->loadTemplate("cicrtgBackendBundle::layout.html.twig", "cicrtgBackendBundle:backend:agentModif.html.twig", 1);
        $this->blocks = array(
            'title' => array($this, 'block_title'),
            'fileDarian' => array($this, 'block_fileDarian'),
            'body_bundle' => array($this, 'block_body_bundle'),
        );
    }

    protected function doGetParent(array $context)
    {
        return "cicrtgBackendBundle::layout.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $this->parent->display($context, array_merge($this->blocks, $blocks));
    }

    // line 3
    public function block_title($context, array $blocks = array())
    {
        echo " ";
        $this->displayParentBlock("title", $context, $blocks);
        echo " Agent ";
    }

    // line 4
    public function block_fileDarian($context, array $blocks = array())
    {
        echo " ";
        $this->displayParentBlock("fileDarian", $context, $blocks);
        echo " Agent / Modification ";
    }

    // line 5
    public function block_body_bundle($context, array $blocks = array())
    {
        // line 6
        echo "           ";
        $this->loadTemplate("cicrtgBackendBundle:includes:agentModifBody.html.twig", "cicrtgBackendBundle:backend:agentModif.html.twig", 6)->display($context);
    }

    public function getTemplateName()
    {
        return "cicrtgBackendBundle:backend:agentModif.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  49 => 6,  46 => 5,  38 => 4,  30 => 3,  11 => 1,);
    }
}
/* {% extends "cicrtgBackendBundle::layout.html.twig" %}*/
/* */
/* {% block title %} {{ parent() }} Agent {% endblock %}*/
/* {% block fileDarian %} {{ parent() }} Agent / Modification {% endblock %}*/
/* {% block body_bundle %}*/
/*            {% include "cicrtgBackendBundle:includes:agentModifBody.html.twig" %}*/
/* {% endblock %}*/
/* */

<?php

namespace Proxies\__CG__\cicrtg\BackendBundle\Entity;

/**
 * DO NOT EDIT THIS FILE - IT WAS CREATED BY DOCTRINE'S PROXY GENERATOR
 */
class Localite extends \cicrtg\BackendBundle\Entity\Localite implements \Doctrine\ORM\Proxy\Proxy
{
    /**
     * @var \Closure the callback responsible for loading properties in the proxy object. This callback is called with
     *      three parameters, being respectively the proxy object to be initialized, the method that triggered the
     *      initialization process and an array of ordered parameters that were passed to that method.
     *
     * @see \Doctrine\Common\Persistence\Proxy::__setInitializer
     */
    public $__initializer__;

    /**
     * @var \Closure the callback responsible of loading properties that need to be copied in the cloned object
     *
     * @see \Doctrine\Common\Persistence\Proxy::__setCloner
     */
    public $__cloner__;

    /**
     * @var boolean flag indicating if this object was already initialized
     *
     * @see \Doctrine\Common\Persistence\Proxy::__isInitialized
     */
    public $__isInitialized__ = false;

    /**
     * @var array properties to be lazy loaded, with keys being the property
     *            names and values being their default values
     *
     * @see \Doctrine\Common\Persistence\Proxy::__getLazyProperties
     */
    public static $lazyPropertiesDefaults = array();



    /**
     * @param \Closure $initializer
     * @param \Closure $cloner
     */
    public function __construct($initializer = null, $cloner = null)
    {

        $this->__initializer__ = $initializer;
        $this->__cloner__      = $cloner;
    }







    /**
     * 
     * @return array
     */
    public function __sleep()
    {
        if ($this->__isInitialized__) {
            return array('__isInitialized__', '' . "\0" . 'cicrtg\\BackendBundle\\Entity\\Localite' . "\0" . 'id', '' . "\0" . 'cicrtg\\BackendBundle\\Entity\\Localite' . "\0" . 'libL', '' . "\0" . 'cicrtg\\BackendBundle\\Entity\\Localite' . "\0" . 'observation', '' . "\0" . 'cicrtg\\BackendBundle\\Entity\\Localite' . "\0" . 'deplacement', '' . "\0" . 'cicrtg\\BackendBundle\\Entity\\Localite' . "\0" . 'position', '' . "\0" . 'cicrtg\\BackendBundle\\Entity\\Localite' . "\0" . 'prefecture', '' . "\0" . 'cicrtg\\BackendBundle\\Entity\\Localite' . "\0" . 'agents');
        }

        return array('__isInitialized__', '' . "\0" . 'cicrtg\\BackendBundle\\Entity\\Localite' . "\0" . 'id', '' . "\0" . 'cicrtg\\BackendBundle\\Entity\\Localite' . "\0" . 'libL', '' . "\0" . 'cicrtg\\BackendBundle\\Entity\\Localite' . "\0" . 'observation', '' . "\0" . 'cicrtg\\BackendBundle\\Entity\\Localite' . "\0" . 'deplacement', '' . "\0" . 'cicrtg\\BackendBundle\\Entity\\Localite' . "\0" . 'position', '' . "\0" . 'cicrtg\\BackendBundle\\Entity\\Localite' . "\0" . 'prefecture', '' . "\0" . 'cicrtg\\BackendBundle\\Entity\\Localite' . "\0" . 'agents');
    }

    /**
     * 
     */
    public function __wakeup()
    {
        if ( ! $this->__isInitialized__) {
            $this->__initializer__ = function (Localite $proxy) {
                $proxy->__setInitializer(null);
                $proxy->__setCloner(null);

                $existingProperties = get_object_vars($proxy);

                foreach ($proxy->__getLazyProperties() as $property => $defaultValue) {
                    if ( ! array_key_exists($property, $existingProperties)) {
                        $proxy->$property = $defaultValue;
                    }
                }
            };

        }
    }

    /**
     * 
     */
    public function __clone()
    {
        $this->__cloner__ && $this->__cloner__->__invoke($this, '__clone', array());
    }

    /**
     * Forces initialization of the proxy
     */
    public function __load()
    {
        $this->__initializer__ && $this->__initializer__->__invoke($this, '__load', array());
    }

    /**
     * {@inheritDoc}
     * @internal generated method: use only when explicitly handling proxy specific loading logic
     */
    public function __isInitialized()
    {
        return $this->__isInitialized__;
    }

    /**
     * {@inheritDoc}
     * @internal generated method: use only when explicitly handling proxy specific loading logic
     */
    public function __setInitialized($initialized)
    {
        $this->__isInitialized__ = $initialized;
    }

    /**
     * {@inheritDoc}
     * @internal generated method: use only when explicitly handling proxy specific loading logic
     */
    public function __setInitializer(\Closure $initializer = null)
    {
        $this->__initializer__ = $initializer;
    }

    /**
     * {@inheritDoc}
     * @internal generated method: use only when explicitly handling proxy specific loading logic
     */
    public function __getInitializer()
    {
        return $this->__initializer__;
    }

    /**
     * {@inheritDoc}
     * @internal generated method: use only when explicitly handling proxy specific loading logic
     */
    public function __setCloner(\Closure $cloner = null)
    {
        $this->__cloner__ = $cloner;
    }

    /**
     * {@inheritDoc}
     * @internal generated method: use only when explicitly handling proxy specific cloning logic
     */
    public function __getCloner()
    {
        return $this->__cloner__;
    }

    /**
     * {@inheritDoc}
     * @internal generated method: use only when explicitly handling proxy specific loading logic
     * @static
     */
    public function __getLazyProperties()
    {
        return self::$lazyPropertiesDefaults;
    }

    
    /**
     * {@inheritDoc}
     */
    public function getId()
    {
        if ($this->__isInitialized__ === false) {
            return (int)  parent::getId();
        }


        $this->__initializer__ && $this->__initializer__->__invoke($this, 'getId', array());

        return parent::getId();
    }

    /**
     * {@inheritDoc}
     */
    public function setLibL($libL)
    {

        $this->__initializer__ && $this->__initializer__->__invoke($this, 'setLibL', array($libL));

        return parent::setLibL($libL);
    }

    /**
     * {@inheritDoc}
     */
    public function getLibL()
    {

        $this->__initializer__ && $this->__initializer__->__invoke($this, 'getLibL', array());

        return parent::getLibL();
    }

    /**
     * {@inheritDoc}
     */
    public function setObservation($observation)
    {

        $this->__initializer__ && $this->__initializer__->__invoke($this, 'setObservation', array($observation));

        return parent::setObservation($observation);
    }

    /**
     * {@inheritDoc}
     */
    public function getObservation()
    {

        $this->__initializer__ && $this->__initializer__->__invoke($this, 'getObservation', array());

        return parent::getObservation();
    }

    /**
     * {@inheritDoc}
     */
    public function setDeplacement($deplacement)
    {

        $this->__initializer__ && $this->__initializer__->__invoke($this, 'setDeplacement', array($deplacement));

        return parent::setDeplacement($deplacement);
    }

    /**
     * {@inheritDoc}
     */
    public function getDeplacement()
    {

        $this->__initializer__ && $this->__initializer__->__invoke($this, 'getDeplacement', array());

        return parent::getDeplacement();
    }

    /**
     * {@inheritDoc}
     */
    public function setPosition(\cicrtg\BackendBundle\Entity\Position $position = NULL)
    {

        $this->__initializer__ && $this->__initializer__->__invoke($this, 'setPosition', array($position));

        return parent::setPosition($position);
    }

    /**
     * {@inheritDoc}
     */
    public function getPosition()
    {

        $this->__initializer__ && $this->__initializer__->__invoke($this, 'getPosition', array());

        return parent::getPosition();
    }

    /**
     * {@inheritDoc}
     */
    public function addAgent(\cicrtg\BackendBundle\Entity\Agent $agent)
    {

        $this->__initializer__ && $this->__initializer__->__invoke($this, 'addAgent', array($agent));

        return parent::addAgent($agent);
    }

    /**
     * {@inheritDoc}
     */
    public function removeAgent(\cicrtg\BackendBundle\Entity\Agent $agent)
    {

        $this->__initializer__ && $this->__initializer__->__invoke($this, 'removeAgent', array($agent));

        return parent::removeAgent($agent);
    }

    /**
     * {@inheritDoc}
     */
    public function getAgents()
    {

        $this->__initializer__ && $this->__initializer__->__invoke($this, 'getAgents', array());

        return parent::getAgents();
    }

    /**
     * {@inheritDoc}
     */
    public function setPrefecture(\cicrtg\BackendBundle\Entity\Prefecture $prefecture)
    {

        $this->__initializer__ && $this->__initializer__->__invoke($this, 'setPrefecture', array($prefecture));

        return parent::setPrefecture($prefecture);
    }

    /**
     * {@inheritDoc}
     */
    public function getPrefecture()
    {

        $this->__initializer__ && $this->__initializer__->__invoke($this, 'getPrefecture', array());

        return parent::getPrefecture();
    }

}

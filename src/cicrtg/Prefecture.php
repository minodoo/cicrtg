<?php

namespace cicrtg\FrontendBundle\DataFixtures\ORM;

use Doctrine\Common\DataFixtures\FixtureInterface;
use Doctrine\Common\Persistence\ObjectManager;
use cicrtg\FrontendBundle\Entity\Prefecture;

class Donnees implements FixtureInterface
{
	public function load(ObjectManager $manager)
	{
		

		$ligne[1]	  = array('libelle' => '', 'id' => );
		$ligne[2]	  = array('libelle' => '', 'id' => );
		$ligne[3]	  = array('libelle' => '', 'id' => );
		$ligne[4]	  = array('libelle' => '', 'id' => );
		$ligne[5]	  = array('libelle' => '', 'id' => );
		$ligne[6]	  = array('libelle' => '', 'id' => );
		$ligne[7]	  = array('libelle' => '', 'id' => );
		$ligne[8]	  = array('libelle' => '', 'id' => );
		$ligne[9]	  = array('libelle' => '', 'id' => );
		$ligne[10]	  = array('libelle' => '', 'id' => );
		$ligne[11]	  = array('libelle' => '', 'id' => );
		$ligne[12]	  = array('libelle' => '', 'id' => );
		$ligne[13]	  = array('libelle' => '', 'id' => );
		$ligne[14]	  = array('libelle' => '', 'id' => );
		$ligne[15]	  = array('libelle' => '', 'id' => );
		

			foreach($ligne as $i => $valeur) 
		{
				$prefecture[$i] = new Prefecture();
				$prefecture[$i]->libP($valeur);
				$prefecture[$i]->region_id($valeur);
				$manager->persist($prefecture[$i]);
		}
			$manager->flush();
		

		
	}
}
<?php

namespace cicrtg\FrontendBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use cicrtg\FrontendBundle\Entity\Contact;
use cicrtg\FrontendBundle\Form\ContactType;

class FrontendController extends Controller
{
    public function indexAction()
    {
    	//$manager = $this->getDoctrine()->getManager();
    	//$Donnee = $manager->getRepository('cicrtgFrontendBundle:Donnee');
    	//$listeDesDonnees = $Donnee->findAll();
		
//		$form = $this->get('tuto_welcom.contact.form');
//		$formHandler = $this->get('tuto_welcom.contact.form.handler') ;
//		if($formHandler->prossess())
//		{
//			$this->get('session')->getFlashBag()->add('notice', 'Votre demande a �t� bien envoy� � l\' admin du site merci.');
//			return $this->redirect($this->generateUrl('frontend_homepage'));
//		}
//        return $this->render('cicrtgFrontendBundle:Frontend:onePage.html.twig', array(
//																					  //'listeDesDonnees' => $listeDesDonnees,
//																					  'form' => $form->createView()));
		$lesDonnees = array(
			 'period' =>'2010 Q1',
            'iphone' => 1500,
            'ipad' => null,
            'itouch' => 2647
             
        );
		$lesDonnees = '['.json_encode($lesDonnees).']';
		//var_dump($lesDonnees); exit;
		$charte = 15073;
		return $this->render('cicrtgFrontendBundle:Frontend:index.html.twig', array('lesDonnees' => $lesDonnees,
																					'chart' => $charte));
    }
	

    public function donneesAction($id = null)
    {
    	$manager = $this->getDoctrine()->getManager();
    	$Donnee = $manager->getRepository('cicrtgFrontendBundle:Donnee');
    	$listeDesDonnees = $Donnee->findAll();
        return $this->render('cicrtgFrontendBundle:Frontend:donnees.html.twig',  array('listeDesDonnees' => $listeDesDonnees));
    }

    public function donneeszoneAction($id = null)
    {
    	$manager = $this->getDoctrine()->getManager();
    	$Donnee = $manager->getRepository('cicrtgFrontendBundle:Donnee');
    	$listeDesDonnees = $Donnee->findAll();
        return $this->render('cicrtgFrontendBundle:Frontend:donnees.html.twig',  array('listeDesDonnees' => $listeDesDonnees));
    }

    public function modeAction()
    {
        return $this->render('cicrtgFrontendBundle:Frontend:index.html.twig');
    }
    public function planAction()
    {
        return $this->render('cicrtgFrontendBundle:Frontend:index.html.twig');
    }
    public function contactAction()
    {
		$form = $this->get('tuto_welcom.contact.form');
	
		
		
        $formHandler = $this->get('tuto_welcom.contact.form.handler') ;
		if($formHandler->prossess())
		{
			$this->get('session')->getFlashBag()->add('notice', 'Votre demande a �t� bien envoy� � l\' admin du site merci.');
			return $this->redirect($this->generateUrl('frontend_homepage'));
		}
		
        return $this->render('cicrtgFrontendBundle:Frontend:onePage.html.twig', array('form' => $form->createView()));
    }
    



     public function navigationAction()
    {
    	return $this->render('cicrtgFrontendBundle:Frontend:navigation.html.twig');
    }


    public function carouselAction()
    {
      
    	return $this->render('cicrtgFrontendBundle:Frontend:carousel.html.twig');
    }
}

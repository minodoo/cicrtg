<?php

namespace cicrtg\FrontendBundle\Form\Handler;


use Symfony\Component\Form\Form;
use Symfony\Component\HttpFoundation\Request;
use Doctrine\ORM\EntityManager;
//use tuto\WelcomBundle\Entity\Contact;

class ContactHandler
{
    protected $request;
    protected $form ;
    protected $em;
    //protected $contact;
    public function __construct(Form $form, Request $request, EntityManager $em)
    {
        $this->form = $form;
        $this->request = $request;
        $this->em = $em;
        //$this->contact = $contact;

    }
    public function prossess()
    {
        if($this->request->getMethod() === "POST")
        {
            $this->form->bind($this->request);
            //echo "en dessou de bind<br/>";  var_dump($this->form->getData()); exit;
            if($this->form->isValid())
            {
                $this->em->persist($this->form->getData());

                    if ($this->onSuccess())

                        return true;


            }
        }

    }

    public function onSuccess()
    {
        $this->em->flush();
        return true;
    }
}
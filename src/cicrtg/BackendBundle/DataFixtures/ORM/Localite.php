<?php

namespace cicrtg\FrontendBundle\DataFixtures\ORM;

use Doctrine\Common\DataFixtures\FixtureInterface;
use Doctrine\Common\Persistence\ObjectManager;
use cicrtg\FrontendBundle\Entity\Region;

class Donnees implements FixtureInterface
{
	public function load(ObjectManager $manager)
	{
		

		$ligne	  = array('SAVANES', 'KARA', 'CENTRALE' ,'PLATEAUX' ,'MARITIME');
		

			foreach($ligne as $i => $valeur) 
		{
				$region[$i] = new Region();
				$region[$i]->libR($valeur);
				$manager->persist($region[$i]);
		}
			$manager->flush();
		

		
	}
}
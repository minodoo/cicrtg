<?php

namespace cicrtg\BackendBundle\DataFixtures\ORM;

use Doctrine\Common\DataFixtures\FixtureInterface;
use Doctrine\Common\Persistence\ObjectManager;
use cicrtg\BackendBundle\Entity\Prefecture;

class Prefectures implements FixtureInterface
{
	public function load(ObjectManager $manager)
	{
		

		$ligne[1]	  = array('libP' => 'OTI', 'id' => 1);
		$ligne[2]	  = array('libP' => 'KPENDJAL', 'id' => 1);
		$ligne[3]	  = array('libP' => 'TANDJOARE', 'id' => 1);
		$ligne[4]	  = array('libP' => 'TONE/CINKASSE', 'id' => 1);
		$ligne[5]	  = array('libP' => 'DANKPEN', 'id' => 2);
		$ligne[6]	  = array('libP' => 'KERAN', 'id' => 2);
		$ligne[7]	  = array('libP' => 'BLITTA', 'id' => 3);
		$ligne[8]	  = array('libP' => 'SOTOUBOUA', 'id' => 3);
		$ligne[9]	  = array('libP' => 'MOYEN-MONO', 'id' => 4);
		$ligne[10]	  = array('libP' => 'HAHO', 'id' => 4);
		$ligne[11]	  = array('libP' => 'OGOU', 'id' => 4);
		$ligne[12]	  = array('libP' => 'AGOU', 'id' => 4);
		$ligne[13]	  = array('libP' => 'Préfecture de BAS-MONO', 'id' => 5);
		$ligne[14]	  = array('libP' => 'Préfecture des LACS', 'id' => 5);
		$ligne[15]	  = array('libP' => 'Préfecture de VO', 'id' => 5);
		$ligne[15]	  = array('libP' => 'Préfecture de YOTO (Zone 1)', 'id' => 5);
		$ligne[15]	  = array('libP' => 'Préfecture de YOTO (Zone 2)', 'id' => 5);
		$ligne[15]	  = array('libP' => 'Préfecture de ZIO', 'id' => 5);
		$ligne[15]	  = array('libP' => 'Préfecture de l\'AVE', 'id' => 5);
		$ligne[15]	  = array('libP' => 'Préfecture du GOLFE', 'id' => 5);
		$ligne[15]	  = array('libP' => 'LOME COMMUNE', 'id' => 5);
		
		
		

			foreach($ligne as $i => $valeur) 
		{
				$prefecture[$i] = new Prefecture();
				$prefecture[$i]->setLibP($valeur);
				$prefecture[$i]->region_id($valeur);
				$manager->persist($prefecture[$i]);
		}
			$manager->flush();
		

		
	}
}
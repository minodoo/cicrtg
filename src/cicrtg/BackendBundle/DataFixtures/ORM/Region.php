<?php

namespace cicrtg\BackendBundle\DataFixtures\ORM;

use Doctrine\Common\DataFixtures\FixtureInterface;
use Doctrine\Common\Persistence\ObjectManager;
use cicrtg\BackendBundle\Entity\Region;

class Regions implements FixtureInterface
{
	public function load(ObjectManager $manager)
	{
		

		$ligne	  = array('SAVANES', 'KARA', 'CENTRALE' ,'PLATEAUX' ,'MARITIME');
		

			foreach($ligne as $i => $valeur) 
		{
				$region[$i] = new Region();
				$region[$i]->setLibR($valeur);
				$manager->persist($region[$i]);
		}
			$manager->flush();
		

		
	}
}
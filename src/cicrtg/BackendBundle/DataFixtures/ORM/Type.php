<?php

namespace cicrtg\FrontendBundle\DataFixtures\ORM;

use Doctrine\Common\DataFixtures\FixtureInterface;
use Doctrine\Common\Persistence\ObjectManager;
use cicrtg\FrontendBundle\Entity\Type;

class Donnees implements FixtureInterface
{
	public function load(ObjectManager $manager)
	{
		

		$ligne	  = array('Hydrographie', 'Pluviométrie');
		

			foreach($ligne as $i => $valeur) 
		{
				$type[$i] = new Type();
				$type[$i]->libT($valeur);
				$manager->persist($type[$i]);
		}
			$manager->flush();
		

		
	}
}
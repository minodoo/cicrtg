<?php

namespace cicrtg\FrontendBundle\DataFixtures\ORM;

use Doctrine\Common\DataFixtures\FixtureInterface;
use Doctrine\Common\Persistence\ObjectManager;
use cicrtg\FrontendBundle\Entity\Donnee;

class Donnees implements FixtureInterface
{
	public function load(ObjectManager $manager)
	{
		

		$ligne[1]	  = array(
								'sendtime'=> '2015-05-06 16:54:25', 
								'sendernumber'=> 2147483647, 
								'message' => 'nangbeto lomé 32 28 passe', 
								'zone'=> 'lomé', 
								'hydro' => 32, 
								'pluvio' => 28, 
								'commentaire' => '');
		$ligne[2]	  = array(
								'sendtime'=> '2015-05-08 09:57:44', 
								'sendernumber'=> 2147483647, 
								'message' => 'nangbeto vogan 23 44 ok', 
								'zone'=> 'vogan', 
								'hydro' => 23, 
								'pluvio' => 44, 
								'commentaire' => '');
		$ligne[3]	  = array(
								'sendtime'=> '2015-05-08 10:09:31', 
								'sendernumber'=> 2147483647, 
								'message' => 'nangbeto tabligbo 30 70', 
								'zone'=> 'tabligbo', 
								'hydro' => 30, 
								'pluvio' => 70, 
								'commentaire' => '');
		$ligne[4]	  = array(
								'sendtime'=> '2015-05-10 08:46:13', 
								'sendernumber'=> 2147483647, 
								'message' => 'Nangbeto Lome 25 65', 
								'zone'=> 'Lome', 
								'hydro' => 25, 
								'pluvio' => 65, 
								'commentaire' => '');

			for ($i=1; $i<5; $i++) 
		{
			
				$liste_donnees[$i] = new Donnee();

				$liste_donnees[$i]->setSendtime(new \Datetime());
				$liste_donnees[$i]->setSendernumber($ligne[$i]['sendernumber']);
				$liste_donnees[$i]->setMessage($ligne[$i]['message']);
				$liste_donnees[$i]->setZone($ligne[$i]['zone']);
				$liste_donnees[$i]->setHydro($ligne[$i]['hydro']);
				$liste_donnees[$i]->setPluvio($ligne[$i]['pluvio']);
				$liste_donnees[$i]->setCommentaire($ligne[$i]['commentaire']);

				$manager->persist($liste_donnees[$i]);
			
		}
			$manager->flush();
		

		
	}
}
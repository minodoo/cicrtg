<?php

namespace cicrtg\BackendBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolverInterface;

class PhenomeneType extends AbstractType
{
    private $class ;
    public function __construct($class)
    {
        $this->class = $class   ;     
    }
    /**
     * @param FormBuilderInterface $builder
     * @param array $options
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('lib')
        ;
    }
    
    /**
     * @param OptionsResolverInterface $resolver
     */
    public function setDefaultOptions(OptionsResolverInterface $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => $this->class
            //'cicrtg\BackendBundle\Entity\Phenomene'
        ));
    }

    /**
     * @return string
     */
    public function getName()
    {
        return 'cicrtg_backendbundle_phenomene';
    }
}

<?php 
namespace cicrtg\BackendBundle\Form\Handler;

use Symfony\Component\Form\Form;
use Symfony\Component\HttpFoundation\Request;
use Doctrine\ORM\EntityManager;

class FormHandler
{
    protected $request;
    protected $em;
    protected $form;
    
    public function __construct(Form $form, Request $request, EntityManager $em)
    {
        $this->form = $form;
        $this->request = $request;
        $this->em  = $em;
        
    }
    #je fais le traitement du formulaire et � la fin je retourne les donn�es pour l'utilis� dans la vue au niveau de $formData
    public function process()
    {
         if($this->request->getMethod() === 'POST')
        {
            
            $this->form->bind($this->request); 
            if($this->form->isValid())
            {
                //echo 'je suis ici'; exit;
                $formData = $this->form;
                $this->em->persist($this->form->getData());
                 if ($this->onSuccess())
                        return $formData;;
                    
            }
        }
        
    }
    
    
    public function processSupp($entity)
    {
        if($this->request->getMethod() === 'POST')
        { 
            $this->form->bind($this->request);
            if($this->form->isValid())
            {echo 'je suis ici'; exit;
                $this->em->remove($entity);
                if($this->onSuccess())
                    return true;
            }
        }
    }
    
    public function processSuppAutre($entity, $form)
    {
        if($this->request->getMethod() === 'POST')
        { 
            $form->bind($this->request);
            if($form->isValid())
            {
                $this->em->remove($entity);
                if($this->onSuccess())
                    return true;
            }
        }
    }
    
    public function onSuccess()
    {
         $this->em->flush();
         return true;
    }
    
    
}
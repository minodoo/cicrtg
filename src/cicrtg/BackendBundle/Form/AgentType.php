<?php

namespace cicrtg\BackendBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolverInterface;

class AgentType extends AbstractType
{
    /**
     * @param FormBuilderInterface $builder
     * @param array $options
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('nom', 'text')
            ->add('prenom', 'text')
            ->add('sexe', new GenderType())
            ->add('tel', 'text')
            ->add('codeS', 'text')
            ->add('email', 'email')
            ->add('residence', 'text')
            ->add('localites', 'entity', array('class' => 'cicrtgBackendBundle:Localite',
                                                'property' => 'libL',
                                                'multiple' => true))
        ;
    }
    
    /**
     * @param OptionsResolverInterface $resolver
     */
    public function setDefaultOptions(OptionsResolverInterface $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => 'cicrtg\BackendBundle\Entity\Agent'
        ));
    }

    /**
     * @return string
     */
    public function getName()
    {
        return 'cicrtg_backendbundle_agent';
    }
}

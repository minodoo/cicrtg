<?php

namespace cicrtg\BackendBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolverInterface;

class LocaliteType extends AbstractType
{
    private $class;
    
    public function __construct($class)
    {
        $this->class = $class;        
    }
    /**
     * @param FormBuilderInterface $builder
     * @param array $options
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('libL')
            ->add('observation')
            //->add('deplacement', 'choice', array('1' => 'Oui', '0' => 'Non'))
            ->add('position', new PositionType())
            ->add('prefecture', 'entity',  array('class' => 'cicrtgBackendBundle:Prefecture',
                                             'property' => 'libP',
                                             'multiple' => false))
        ;
    }
    
    /**
     * @param OptionsResolverInterface $resolver
     */
    public function setDefaultOptions(OptionsResolverInterface $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => $this->class
        ));
    }

    /**
     * @return string
     */
    public function getName()
    {
        return 'cicrtg_backendbundle_localite';
    }
}

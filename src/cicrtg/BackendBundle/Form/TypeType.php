<?php

namespace cicrtg\BackendBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolverInterface;

class TypeType extends AbstractType
{
    private $class ;
    public function __construct($class)
    {
        $this->class = $class   ;     
    }
    /**
     * @param FormBuilderInterface $builder
     * @param array $options
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('libT', 'text')
            ->add('postphenomene', 'entity', array('class' => 'cicrtgBackendBundle:Phenomene',
                                                   'property' => 'lib',
                                                   'multiple' => false))
        ;
    }
    
    /**
     * @param OptionsResolverInterface $resolver
     */
    public function setDefaultOptions(OptionsResolverInterface $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => $this->class
            //'cicrtg\BackendBundle\Entity\Type'
        ));
    }

    /**
     * @return string
     */
    public function getName()
    {
        return 'cicrtg_backendbundle_type';
    }
}

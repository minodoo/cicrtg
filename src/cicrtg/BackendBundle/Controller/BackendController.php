<?php
namespace cicrtg\BackendBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Response;

use cicrtg\BackendBundle\Entity\Prefecture;
use cicrtg\BackendBundle\Form\PrefectureType;

use cicrtg\BackendBundle\Entity\Localite;
use cicrtg\BackendBundle\Form\LocaliteType;

use cicrtg\BackendBundle\Entity\Agent;
use cicrtg\BackendBundle\Form\AgentType;

use cicrtg\BackendBundle\Entity\Phenomene;
use cicrtg\BackendBundle\Entity\Type;
use cicrtg\BackendBundle\Form\Handler\FormHandler;

use APY\DataGridBundle\Grid\Source\Entity;

use JMS\SecurityExtraBundle\Annotation\Secure;



class BackendController extends Controller
{
    public function indexAction()
    {
        return $this->render('cicrtgBackendBundle:backend:index.html.twig');
    }
    public function navAction()
    {
        $em = $this->getDoctrine()->getManager();
        $Phenomene = $em->getRepository('cicrtgBackendBundle:Phenomene');
        $phenomene = $Phenomene->findAll();
        return $this->render('cicrtgBackendBundle:includes:nav.html.twig', array('phenomenes' => $phenomene));
    }
    
    public function donneesAction(Phenomene $phenomene)
    {
        $em = $this->getDoctrine()->getManager();
        $Donnee = $em->getRepository('cicrtgBackendBundle:Donnee');
        $donnees = $Donnee->getDonneesByType($phenomene->getId());
        $donneesCount =  $Donnee->getCountDonneeByType($phenomene->getId());       
         return $this->render('cicrtgBackendBundle:backend:donnees.html.twig', array('phenomene' => $phenomene,
                                                                                     'donnees' => $donnees,
                                                                                     'donneesCount' => $donneesCount));
    }
    
    public function agentsAction()
    {
        
        $form = $this->get('cicrtg_backend.agent.form');
        $formHandel = $this->get('cicrtg_backend.agent.form.handler');
        $formData = $formHandel->process();
        $em = $this->getDoctrine()->getManager();
        $Agent = $em->getRepository('cicrtgBackendBundle:Agent');
        $agents = $Agent->findAll();
        if($this->retourAgent($formData, 1))
        {
            
            return $this->redirect($this->generateUrl('cicrtg_backend_agent'));
        }
         return $this->render('cicrtgBackendBundle:backend:agents.html.twig', array('form' => $form->createView(), 'agents' => $agents));
    }
    
   /**
    * @Secure(roles="ROLE_GESTIONNAIRE")
    */
    public function agentmodifAction(Agent $agent)
    {
        $form = $this->createForm('cicrtg_backendbundle_agent', $agent);
        $formhandel = new FormHandler($form, $this->getRequest(), $this->getDoctrine()->getManager());
        $formData = $formhandel->process();
        if($this->retourAgent($formData, 2))
        {
            return $this->redirect($this->generateUrl('cicrtg_backend_agent'));
        }
        return $this->render('cicrtgBackendBundle:backend:agentModif.html.twig', array('form' => $form->createView(),
                                                                                       'agent' => $agent));
   
    }
    
    /**
    * @Secure(roles="ROLE_GESTIONNAIRE")
    */
    public function suppagentAction(Agent $agent)
    {
        $form = $this->createFormBuilder()->getForm();
        $request = $this->getRequest();
        if($request->getMethod() === 'POST')
        {
            $form->bind($request);
            if($form->isValid())
            {
                $em = $this->getDoctrine()->getManager();
                $em->remove($agent);
                $em->flush();
                if($this->retourAgent($form, 3))
                {
                    return $this->redirect($this->generateUrl('cicrtg_backend_agent'));
                }
            }
        }
        return $this->render('cicrtgBackendBundle:backend:supAgent.html.twig', array('form' => $form->createView(),
                                                                                     'agent' => $agent));

    }
    
    public function zoneAction()
    {
        $em = $this->getDoctrine()->getManager();
        $Prefecture = $em->getRepository('cicrtgBackendBundle:Prefecture');
        $prefectures = $Prefecture->getPrefectureByRegion();
        
        $Localite = $em->getRepository('cicrtgBackendBundle:Localite');
        $localites = $Localite->getLocaliteByPosition();
        $formP = $this->get('cicrtg_backend.prefecture.form');
        $formL = $this->get('cicrtg_backend.localite.form');
        
        $formPhandel = $this->get('cicrtg_backend.prefecture.form.handler');
        $formLhandel = $this->get('cicrtg_backend.localite.form.handler');
        
        $formData = $formPhandel->process();
        $formLData = $formLhandel->process();
        
        //var_dump($formData);
        
            if($this->retourPrefecture($formData, 1) || $this->retourLocalite($formLData, 1))
            return $this->redirect($this->generateUrl('cicrtg_backend_zone'));
        
        return $this->render('cicrtgBackendBundle:backend:zone.html.twig', array('formP' => $formP->createView(),
                                                                                  'formL' => $formL->createView(),
                                                                                   'prefectureListe' => $prefectures,
                                                                                   'localiteListe' => $localites
                                                                                   
                                                                                   ));
    }
    
    
    /**
    * @Secure(roles="ROLE_GESTIONNAIRE")
    */
    public function modifprefectureAction(Prefecture $prefecture)
    {
        $formP = $this->createForm('cicrtg_backendbundle_prefecture', $prefecture);
        //$formP = $this->get('cicrtg_backend.prefecture.form');
        //echo 'je suis la'; exit;
        
        $formPhandel =  new FormHandler($formP, $this->getRequest(), $this->getDoctrine()->getManager());
        $formData = $formPhandel->process();
        
        if($this->retourPrefecture($formData, 2))
            return $this->redirect($this->generateUrl('cicrtg_backend_zone'));
        
         return $this->render('cicrtgBackendBundle:backend:prefectureModif.html.twig', array('formPmodif' => $formP->createView(),
                                                                                              'prefecture' => $prefecture));
        
    }
    
    /**
    * @Secure(roles="ROLE_GESTIONNAIRE")
    */
    public function modiflocaliteAction(Localite $localite)
    {
        $formL = $this->createForm('cicrtg_backendbundle_localite', $localite);
        //$formP = $this->get('cicrtg_backend.prefecture.form');
        //echo 'je suis la'; exit;
        
        $formLhandel =  new FormHandler($formL, $this->getRequest(), $this->getDoctrine()->getManager());
        $formData = $formLhandel->process();
        
        if($this->retourLocalite($formData, 2))
            return $this->redirect($this->generateUrl('cicrtg_backend_zone'));
        
         return $this->render('cicrtgBackendBundle:backend:localiteModif.html.twig', array('formLmodif' => $formL->createView(),
                                                                                              'localite' => $localite));
        
    }
    
    /**
    * @Secure(roles="ROLE_GESTIONNAIRE")
    */
    public function supprefectureAction(Prefecture $prefecture)
    {
        $formSupp = $this->createFormBuilder()->getForm();
        //$formPhandel =  new FormHandler($formSupp, $this->getRequest(), $this->getDoctrine()->getManager());
        //$supp = $formPhandel->processSupp($prefecture);
        
        $request = $this->getRequest();
        if($request->getMethod() === 'POST')
        {
            
            $formSupp->bind($request);
            if($formSupp->isValid())
            {
                $em = $this->getDoctrine()->getManager();
                $em->remove($prefecture);
                $em->flush();
                $this->retourPrefecture($formSupp, 3);
                 return $this->redirect($this->generateUrl('cicrtg_backend_zone'));
            }
        }
    
        
        return $this->render('cicrtgBackendBundle:backend:supPrefecture.html.twig', array('formSupp' => $formSupp->createView(),
                                                                                           'prefecture' => $prefecture));
        
    }
    
    
    /**
    * @Secure(roles="ROLE_GESTIONNAIRE")
    */
    public function suplocaliteAction(Localite $localite)
    {
        $formSupp = $this->createFormBuilder()->getForm();
        //$formPhandel =  new FormHandler($formSupp, $this->getRequest(), $this->getDoctrine()->getManager());
        //$supp = $formPhandel->processSupp($prefecture);
        
        $request = $this->getRequest();
        if($request->getMethod() === 'POST')
        {
            
            $formSupp->bind($request);
            if($formSupp->isValid())
            {
                $em = $this->getDoctrine()->getManager();
                $em->remove($localite);
                $em->flush();
                $this->retourLocalite($formSupp, 3);
                 return $this->redirect($this->generateUrl('cicrtg_backend_zone'));
            }
        }
    
        
        return $this->render('cicrtgBackendBundle:backend:supLocalite.html.twig', array('formSupp' => $formSupp->createView(),
                                                                                           'localite' => $localite));
        
    }
    
    public function localiteAction()
    {
         return $this->render('cicrtgBackendBundle:backend:index.html.twig');
    }
    
    public function usersAction()
    {
       return $this->render('cicrtgBackendBundle:backend:index.html.twig'); 
    }
    
    
    # type de données
    public function typeDonneesAction()
    {
        $em = $this->getDoctrine()->getManager();
        $PostPhenome = $em->getRepository('cicrtgBackendBundle:Phenomene');
        $postPhenomes = $PostPhenome->findAll();
        
        $Type = $em->getRepository('cicrtgBackendBundle:Type');
        $types = $Type->findAll();
        
        $formPheno = $this->get('cicrtg_backend.postphenome.form');
        $formType = $this->get('cicrtg_backend.type.form');
        return $this->render('cicrtgBackendBundle:backend:typesdonnees.html.twig', array('formPheno' => $formPheno->createView(),
                                                                                        'formType' => $formType->createView(),
                                                                                        'types' => $types,
                                                                                        'postPhenomes' => $postPhenomes));
    }
    
    public function phenoDelAction(Phenomene $pheno)
    {
        
        
        $form = $this->createFormBuilder()->getForm();
        $handler = $this->get('cicrtg_backend.postphenome.form.handler');
        $formData = $handler->processSuppAutre($pheno, $form);
        //echo 'je suis ici'; exit;
        if($this->retourPheno($formData, 3))
        {
            return $this->redirect($this->generateUrl('cicrtg_backend_type'));
        }
        return $this->render('cicrtgBackendBundle:backend:supPheno.html.twig', array('form' => $form->createView(),
                                                                                          'pheno' => $pheno));
    }
    
    public function phenoPostAction()
    {
       // echo 'ici'; exit;
        $handler = $this->get('cicrtg_backend.postphenome.form.handler');
        $formData = $handler->process();
        //var_dump($formData); exit;
        if($this->retourPheno($formData, 1))
        {
            return $this->redirect($this->generateUrl('cicrtg_backend_type'));
        }
        
    }
    
    
    public function typePostAction()
    {
         $handler = $this->get('cicrtg_backend.type.form.handler');
        $formData = $handler->process();
        //var_dump($formData); exit;
        if($this->retourType($formData, 1))
        {
            return $this->redirect($this->generateUrl('cicrtg_backend_type'));
        }
    }
    
     public function typeDelAction(Type $type)
    {
        $form = $this->createFormBuilder()->getForm();
        $handler = $this->get('cicrtg_backend.type.form.handler');
        $formData = $handler->processSuppAutre($type, $form);
        //echo 'je suis ici'; exit;
        if($this->retourType($formData, 3))
        {
            return $this->redirect($this->generateUrl('cicrtg_backend_type'));
        }
        return $this->render('cicrtgBackendBundle:backend:supType.html.twig', array('form' => $form->createView(),
                                                                                          'type' => $type));
    }
    
# ======================================   les fonction privé de la classe ==============================================
private function retourPrefecture($formData, $etat)
{
    if($formData)
    {
        if($etat == 1)
        {
            $this->get('session')->getFlashBag()->add('notice', 'La préfecture '.$formData->get('libP')->getData().' a été enrégistré avec succès');
            return true;
        }elseif($etat == 2)
            {
                $this->get('session')->getFlashBag()->add('notice', 'La préfecture '.$formData->get('libP')->getData().' a apporté une modification avec succès');
                return true;
            }
            else
            {
                    $this->get('session')->getFlashBag()->add('notice', 'Cette préfecture a été supprimer avec succès');
                    return true;
            }
            
    }
    return false;
}
   
   
   
private function retourPheno($formData, $etat)
{
    if($formData)
    {
        if($etat == 1)
        {
            $this->get('session')->getFlashBag()->add('notice', 'Le poste phénomène : '.$formData->get('lib')->getData().' a été enrégistré avec succès');
            return true;
        }elseif($etat == 2)
            {
                $this->get('session')->getFlashBag()->add('notice', 'Le poste phénomène : '.$formData->get('lib')->getData().' a apporté une modification avec succès');
                return true;
            }
            else
            {
                    $this->get('session')->getFlashBag()->add('notice', 'Cet poste phénomène a été supprimer avec succès');
                    return true;
            }
            
    }
    return false;
}


private function retourType($formData, $etat)
{
    if($formData)
    {
        if($etat == 1)
        {
            $this->get('session')->getFlashBag()->add('notice', 'Le type de donées : '.$formData->get('libT')->getData().' a été enrégistré avec succès');
            return true;
        }elseif($etat == 2)
            {
                $this->get('session')->getFlashBag()->add('notice', 'Le type de donées : '.$formData->get('libT')->getData().' a apporté une modification avec succès');
                return true;
            }
            else
            {
                    $this->get('session')->getFlashBag()->add('notice', 'Cet type de donées a été supprimer avec succès');
                    return true;
            }
            
    }
    return false;
}
   


private function retourLocalite($formData, $etat)
{
    if($formData)
    {
        if($etat == 1)
        {
            $this->get('session')->getFlashBag()->add('notice', 'La localité '.$formData->get('libL')->getData().' a été enrégistré avec succès');
            return true;
        }elseif($etat == 2)
            {
                $this->get('session')->getFlashBag()->add('notice', 'La localité '.$formData->get('libL')->getData().' a apporté une modification avec succès');
                return true;
            }
            else
            {
                    $this->get('session')->getFlashBag()->add('notice', 'Cette préfecture a été supprimer avec succès');
                    return true;
            }
            
    }
    return false;
}

private function retourAgent($formData, $etat)
{
    if($formData)
    {
        if($etat == 1)
        {
            $this->get('session')->getFlashBag()->add('notice', 'L\'agent '.$formData->get('nom')->getData().' a été enrégistré avec succès');
            return true;
        }elseif($etat == 2)
            {
                $this->get('session')->getFlashBag()->add('notice', 'L\'agent '.$formData->get('nom')->getData().' a apporté une modification avec succès');
                return true;
            }
            else
            {
                    $this->get('session')->getFlashBag()->add('notice', 'Cet agent a été supprimer avec succès');
                    return true;
            }
            
    }
    return false;
}
   
}
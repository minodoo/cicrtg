<?php

namespace cicrtg\FrontendBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use cicrtg\FrontendBundle\Entity\Contact;
use cicrtg\FrontendBundle\Form\ContactType;

class FrontendController extends Controller
{
    public function indexAction()
    {
        return $this->render('cicrtgBackendBundle:backend:index.html.twig');
    }
}

<?php

namespace cicrtg\BackendBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Region
 *
 * @ORM\Table()
 * @ORM\Entity(repositoryClass="cicrtg\BackendBundle\Entity\RegionRepository")
 */
class Region
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="libR", type="string", length=50)
     */
    private $libR;
    
     /**
     *@ORM\OneToMany(targetEntity="cicrtg\BackendBundle\Entity\Prefecture", mappedBy="region")
     */
    private $prefectures;


    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set libR
     *
     * @param string $libR
     *
     * @return Region
     */
    public function setLibR($libR)
    {
        $this->libR = $libR;
    
        return $this;
    }

    /**
     * Get libR
     *
     * @return string
     */
    public function getLibR()
    {
        return $this->libR;
    }
    
   
    
    /**
     * Constructor
     */
    public function __construct()
    {
        $this->prefectures = new \Doctrine\Common\Collections\ArrayCollection();
    }

    /**
     * Add prefecture
     *
     * @param \cicrtg\BackendBundle\Entity\Prefecture $prefecture
     *
     * @return Region
     */
    public function addPrefecture(\cicrtg\BackendBundle\Entity\Prefecture $prefecture)
    {
        $this->prefectures[] = $prefecture;
        $prefecture->setRegion($this);
    
        return $this;
    }

    /**
     * Remove prefecture
     *
     * @param \cicrtg\BackendBundle\Entity\Prefecture $prefecture
     */
    public function removePrefecture(\cicrtg\BackendBundle\Entity\Prefecture $prefecture)
    {
        $this->prefectures->removeElement($prefecture);
    }

    /**
     * Get prefectures
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getPrefectures()
    {
        return $this->prefectures;
    }
}

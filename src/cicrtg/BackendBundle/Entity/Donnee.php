<?php

namespace cicrtg\BackendBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Donnee
 *
 * @ORM\Table()
 * @ORM\Entity(repositoryClass="cicrtg\BackendBundle\Entity\DonneeRepository")
 */
class Donnee
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var float
     *
     * @ORM\Column(name="notation", type="float")
     */
    private $notation;

    /**
     * @var string
     *
     * @ORM\Column(name="commentaire", type="text")
     */
    private $commentaire;
    
    /**
     *@ORM\ManyToOne(targetEntity="cicrtg\BackendBundle\Entity\Agent", inversedBy="donnees")
     *@ORM\JoinColumn(nullable=false)
     */
    private $agent;
    
    /**
     *@ORM\ManyToOne(targetEntity="cicrtg\BackendBundle\Entity\Type", inversedBy="donnees")
     *@ORM\JoinColumn(nullable=false)
     */
    private $type;
    
    /**
     * @var \DateTime
     *
     * @ORM\Column(name="date", type="datetime")
     */
    private $date;
    
    public function __construct()
    {
        $this->date = new \datetime();
    }


    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set notation
     *
     * @param float $notation
     *
     * @return Donnee
     */
    public function setNotation($notation)
    {
        $this->notation = $notation;
    
        return $this;
    }

    /**
     * Get notation
     *
     * @return float
     */
    public function getNotation()
    {
        return $this->notation;
    }

    /**
     * Set commentaire
     *
     * @param string $commentaire
     *
     * @return Donnee
     */
    public function setCommentaire($commentaire)
    {
        $this->commentaire = $commentaire;
    
        return $this;
    }

    /**
     * Get commentaire
     *
     * @return string
     */
    public function getCommentaire()
    {
        return $this->commentaire;
    }

    /**
     * Set agent
     *
     * @param \cicrtg\BackendBundle\Entity\Agent $agent
     *
     * @return Donnee
     */
    public function setAgent(\cicrtg\BackendBundle\Entity\Agent $agent)
    {
        $this->agent = $agent;
    
        return $this;
    }

    /**
     * Get agent
     *
     * @return \cicrtg\BackendBundle\Entity\Agent
     */
    public function getAgent()
    {
        return $this->agent;
    }

    /**
     * Set type
     *
     * @param \cicrtg\BackendBundle\Entity\Type $type
     *
     * @return Donnee
     */
    public function setType(\cicrtg\BackendBundle\Entity\Type $type)
    {
        $this->type = $type;
    
        return $this;
    }

    /**
     * Get type
     *
     * @return \cicrtg\BackendBundle\Entity\Type
     */
    public function getType()
    {
        return $this->type;
    }

    /**
     * Set date
     *
     * @param \DateTime $date
     *
     * @return Donnee
     */
    public function setDate($date)
    {
        $this->date = $date;
    
        return $this;
    }

    /**
     * Get date
     *
     * @return \DateTime
     */
    public function getDate()
    {
        return $this->date;
    }
}

<?php

namespace cicrtg\BackendBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;
use Symfony\Bridge\Doctrine\Validator\Constraints as DoctrineAssert;
use Symfony\Bridge\Doctrine\Validator\Constraints\UniqueEntity;


/**
 * Localite
 *
 * @ORM\Table()
 * @ORM\Entity(repositoryClass="cicrtg\BackendBundle\Entity\LocaliteRepository")
 * @UniqueEntity(fields="libL", message="Cette localité exite déjà, veuillez entrer une autre")
 */
class Localite
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="libL", type="string", length=50)
     * @Assert\NotBlank
     * @Assert\Length(min=2)
     */
    private $libL;

    /**
     * @var string
     *
     * @ORM\Column(name="observation", type="text")
     * @Assert\Length(min=2)
     */
    private $observation;

    /**
     * @var binary
     *
     * @ORM\Column(name="deplacement", type="string", length=1)
     */
    private $deplacement;
    
    /**
     *@ORM\OneToOne(targetEntity="cicrtg\BackendBundle\Entity\Position", cascade={"persist"})
    */
    private $position;
    
    /**
     *@ORM\ManyToOne(targetEntity="cicrtg\BackendBundle\Entity\Prefecture", inversedBy="localites")
     *@ORM\JoinColumn(nullable=false)
    */
    private $prefecture;
    
    /**
     *@ORM\ManyToMany(targetEntity="cicrtg\BackendBundle\Entity\Agent", inversedBy="localites")
    */
    private $agents;
    
    

    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set libL
     *
     * @param string $libL
     *
     * @return Localite
     */
    public function setLibL($libL)
    {
        $this->libL = $libL;
    
        return $this;
    }

    /**
     * Get libL
     *
     * @return string
     */
    public function getLibL()
    {
        return $this->libL;
    }

    /**
     * Set observation
     *
     * @param string $observation
     *
     * @return Localite
     */
    public function setObservation($observation)
    {
        $this->observation = $observation;
    
        return $this;
    }

    /**
     * Get observation
     *
     * @return string
     */
    public function getObservation()
    {
        return $this->observation;
    }

    /**
     * Set deplacement
     *
     * @param binary $deplacement
     *
     * @return Localite
     */
    public function setDeplacement($deplacement)
    {
        $this->deplacement = $deplacement;
    
        return $this;
    }

    /**
     * Get deplacement
     *
     * @return binary
     */
    public function getDeplacement()
    {
        return $this->deplacement;
    }

    /**
     * Set position
     *
     * @param \cicrtg\BackendBundle\Entity\Position $position
     *
     * @return Localite
     */
    public function setPosition(\cicrtg\BackendBundle\Entity\Position $position = null)
    {
        $this->position = $position;
    
        return $this;
    }

    /**
     * Get position
     *
     * @return \cicrtg\BackendBundle\Entity\Position
     */
    public function getPosition()
    {
        return $this->position;
    }

   
    /**
     * Constructor
     */
    public function __construct()
    {
        $this->agents = new \Doctrine\Common\Collections\ArrayCollection();
    }


    /**
     * Add agent
     *
     * @param \cicrtg\BackendBundle\Entity\Agent $agent
     *
     * @return Localite
     */
    public function addAgent(\cicrtg\BackendBundle\Entity\Agent $agent)
    {
        $this->agents[] = $agent;
    
        return $this;
    }

    /**
     * Remove agent
     *
     * @param \cicrtg\BackendBundle\Entity\Agent $agent
     */
    public function removeAgent(\cicrtg\BackendBundle\Entity\Agent $agent)
    {
        $this->agents->removeElement($agent);
    }

    /**
     * Get agents
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getAgents()
    {
        return $this->agents;
    }

    /**
     * Set prefecture
     *
     * @param \cicrtg\BackendBundle\Entity\Prefecture $prefecture
     *
     * @return Localite
     */
    public function setPrefecture(\cicrtg\BackendBundle\Entity\Prefecture $prefecture)
    {
        $this->prefecture = $prefecture;
    
        return $this;
    }

    /**
     * Get prefecture
     *
     * @return \cicrtg\BackendBundle\Entity\Prefecture
     */
    public function getPrefecture()
    {
        return $this->prefecture;
    }
}

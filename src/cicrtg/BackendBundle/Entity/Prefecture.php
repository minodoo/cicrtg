<?php

namespace cicrtg\BackendBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;
use Symfony\Bridge\Doctrine\Validator\Constraints as DoctrineAssert;
use Symfony\Bridge\Doctrine\Validator\Constraints\UniqueEntity;

/**
 * Prefecture
 *
 * @ORM\Table()
 * @ORM\Entity(repositoryClass="cicrtg\BackendBundle\Entity\PrefectureRepository")
 * @UniqueEntity(fields="libP", message="Cette pr�fecture exite d�j�, veuillez entrer une autre")
 */
class Prefecture
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="libP", type="string", length=50)
     * @Assert\NotBlank
     * @Assert\Length(min=2)
     */
    private $libP;
    
    /**
     *@ORM\ManyToOne(targetEntity="cicrtg\BackendBundle\Entity\Region", inversedBy="prefectures")
     *@ORM\JoinColumn(nullable = false)
     */
    private $region;
    
    /**
     *@ORM\OneToMany(targetEntity="cicrtg\BackendBundle\Entity\Localite", mappedBy="prefecture")
    */
    private $localites;

    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set libP
     *
     * @param string $libP
     *
     * @return Prefecture
     */
    public function setLibP($libP)
    {
        $this->libP = $libP;
    
        return $this;
    }

    /**
     * Get libP
     *
     * @return string
     */
    public function getLibP()
    {
        return $this->libP;
    }
  
    /**
     * Add localite
     *
     * @param \cicrtg\BackendBundle\Entity\Localite $localite
     *
     * @return Prefecture
     */
    public function addLocalite(\cicrtg\BackendBundle\Entity\Localite $localite)
    {
        $this->localite[] = $localite;
        $localite->setPrefectures($this);
        return $this;
    }

    /**
     * Remove localite
     *
     * @param \cicrtg\BackendBundle\Entity\Localite $localite
     */
    public function removeLocalite(\cicrtg\BackendBundle\Entity\Localite $localite)
    {
        $this->localite->removeElement($localite);
        
    }

    /**
     * Get localite
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getLocalite()
    {
        return $this->localite;
    }
    /**
     * Constructor
     */
    public function __construct()
    {
        $this->localite = new \Doctrine\Common\Collections\ArrayCollection();
    }

   

    /**
     * Set region
     *
     * @param \cicrtg\BackendBundle\Entity\Region $region
     *
     * @return Prefecture
     */
    public function setRegion(\cicrtg\BackendBundle\Entity\Region $region)
    {
        $this->region = $region;
    
        return $this;
    }

    /**
     * Get region
     *
     * @return \cicrtg\BackendBundle\Entity\Region
     */
    public function getRegion()
    {
        return $this->region;
    }

    /**
     * Get localites
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getLocalites()
    {
        return $this->localites;
    }
}

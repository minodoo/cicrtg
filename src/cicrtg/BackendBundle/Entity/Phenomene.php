<?php

namespace cicrtg\BackendBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Phenomene
 *
 * @ORM\Table()
 * @ORM\Entity(repositoryClass="cicrtg\BackendBundle\Entity\PhenomeneRepository")
 */
class Phenomene
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="lib", type="string", length=50)
     */
    private $lib;
    
     /**
     *@ORM\OneToMany(targetEntity="cicrtg\BackendBundle\Entity\Type", mappedBy="postphenomene")
     */
    private $typeDonnees;


    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set lib
     *
     * @param string $lib
     *
     * @return Phenomene
     */
    public function setLib($lib)
    {
        $this->lib = $lib;
    
        return $this;
    }

    /**
     * Get lib
     *
     * @return string
     */
    public function getLib()
    {
        return $this->lib;
    }
    /**
     * Constructor
     */
    public function __construct()
    {
        $this->typeDonnees = new \Doctrine\Common\Collections\ArrayCollection();
    }

    /**
     * Add typeDonnee
     *
     * @param \cicrtg\BackendBundle\Entity\Type $typeDonnee
     *
     * @return Phenomene
     */
    public function addTypeDonnee(\cicrtg\BackendBundle\Entity\Type $typeDonnee)
    {
        $this->typeDonnees[] = $typeDonnee;
    
        return $this;
    }

    /**
     * Remove typeDonnee
     *
     * @param \cicrtg\BackendBundle\Entity\Type $typeDonnee
     */
    public function removeTypeDonnee(\cicrtg\BackendBundle\Entity\Type $typeDonnee)
    {
        $this->typeDonnees->removeElement($typeDonnee);
    }

    /**
     * Get typeDonnees
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getTypeDonnees()
    {
        return $this->typeDonnees;
    }
}

<?php

namespace cicrtg\BackendBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;
use Symfony\Bridge\Doctrine\Validator\Constraints as DoctrineAssert;
use Symfony\Bridge\Doctrine\Validator\Constraints\UniqueEntity;
/**
 * Type
 *
 * @ORM\Table()
 * @ORM\Entity(repositoryClass="cicrtg\BackendBundle\Entity\TypeRepository")
 */
class Type
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="libT", type="string", length=20)
     */
    private $libT;
    
     /**
     *@ORM\OneToMany(targetEntity="cicrtg\BackendBundle\Entity\Donnee", mappedBy="type")
     */
    private $donnees;
    
    /**
     *@ORM\ManyToOne(targetEntity="cicrtg\BackendBundle\Entity\Phenomene", inversedBy="typeDonnees")
     */
    private $postphenomene;
    


    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set libT
     *
     * @param string $libT
     *
     * @return Type
     */
    public function setLibT($libT)
    {
        $this->libT = $libT;
    
        return $this;
    }

    /**
     * Get libT
     *
     * @return string
     */
    public function getLibT()
    {
        return $this->libT;
    }
    /**
     * Constructor
     */
    public function __construct()
    {
        $this->donnees = new \Doctrine\Common\Collections\ArrayCollection();
    }

    /**
     * Add donnee
     *
     * @param \cicrtg\BackendBundle\Entity\Donnee $donnee
     *
     * @return Type
     */
    public function addDonnee(\cicrtg\BackendBundle\Entity\Donnee $donnee)
    {
        $this->donnees[] = $donnee;
        $donnee->setType($this);
    
        return $this;
    }

    /**
     * Remove donnee
     *
     * @param \cicrtg\BackendBundle\Entity\Donnee $donnee
     */
    public function removeDonnee(\cicrtg\BackendBundle\Entity\Donnee $donnee)
    {
        $this->donnees->removeElement($donnee);
    }

    /**
     * Get donnees
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getDonnees()
    {
        return $this->donnees;
    }

    /**
     * Set postphenomene
     *
     * @param \cicrtg\BackendBundle\Entity\Phenomene $postphenomene
     *
     * @return Type
     */
    public function setPostphenomene(\cicrtg\BackendBundle\Entity\Phenomene $postphenomene = null)
    {
        $this->postphenomene = $postphenomene;
        $postphenomene->addTypeDonnee($this);
        return $this;
    }

    /**
     * Get postphenomene
     *
     * @return \cicrtg\BackendBundle\Entity\Phenomene
     */
    public function getPostphenomene()
    {
        return $this->postphenomene;
    }
}

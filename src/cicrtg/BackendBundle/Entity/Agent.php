<?php

namespace cicrtg\BackendBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;
use Symfony\Bridge\Doctrine\Validator\Constraints as DoctrineAssert;
use Symfony\Bridge\Doctrine\Validator\Constraints\UniqueEntity;

/**
 * Agent
 *
 * @ORM\Table()
 * @ORM\Entity(repositoryClass="cicrtg\BackendBundle\Entity\AgentRepository")
 * @UniqueEntity(fields="email", message="Cet email est déjà utilisé par un autre agent ")
 * @UniqueEntity(fields="codeS", message="Cet code existe déja alors veuillez inscrire un autre")
 */
class Agent
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="nom", type="string", length=50)
     * @Assert\NotBlank
     * @Assert\Length(min=2)
     */
    private $nom;

    /**
     * @var string
     *
     * @ORM\Column(name="prenom", type="string", length=70)
     * @Assert\NotBlank
     * @Assert\Length(min=2, minMessage="Cette valeur est trop courte")
     */
    private $prenom;

    /**
     * @var binary
     *
     * @ORM\Column(name="sexe", type="string", length=1)
     */
    private $sexe;

    /**
     * @var string
     *
     * @ORM\Column(name="tel", type="string", length=15)
     * @Assert\NotBlank
     * @Assert\Length(min=8, minMessage="cette valeur doit contenire au moins {{ limit }} caractères")
     * @Assert\Length(max=15, maxMessage="cette valeur ne doit pas dépacer {{ limit }} caractères")
     */
    private $tel;

    /**
     * @var string
     *
     * @ORM\Column(name="codeS", type="string", length=3)
     * @Assert\NotBlank
     *
     */
    private $codeS;

    /**
     * @var string
     *
     * @ORM\Column(name="email", type="string", length=50)
     * @Assert\NotBlank
     * @Assert\Email(message="{{ value }} n'est pas un email valide ")
     */
    private $email;

    /**
     * @var string
     *
     * @ORM\Column(name="residence", type="string", length=50)
     * @Assert\NotBlank
     * @Assert\Length(min=4, minMessage="Veuillez entrer une valeur > {{ limit }} caractères svp")
     */
    private $residence;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="date", type="datetime")
     */
    private $date;
    
    /**
     *@ORM\OneToMany(targetEntity="cicrtg\BackendBundle\Entity\Donnee", mappedBy="agent")
     */
    private $donnees;
    
     /**
     *@ORM\ManyToMany(targetEntity="cicrtg\BackendBundle\Entity\Localite", inversedBy="agents")
     *
     */
    private $localites;


    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set nom
     *
     * @param string $nom
     *
     * @return Agent
     */
    public function setNom($nom)
    {
        $this->nom = $nom;
    
        return $this;
    }

    /**
     * Get nom
     *
     * @return string
     */
    public function getNom()
    {
        return $this->nom;
    }

    /**
     * Set prenom
     *
     * @param string $prenom
     *
     * @return Agent
     */
    public function setPrenom($prenom)
    {
        $this->prenom = $prenom;
    
        return $this;
    }

    /**
     * Get prenom
     *
     * @return string
     */
    public function getPrenom()
    {
        return $this->prenom;
    }

    /**
     * Set sexe
     *
     * @param binary $sexe
     *
     * @return Agent
     */
    public function setSexe($sexe)
    {
        $this->sexe = $sexe;
    
        return $this;
    }

    /**
     * Get sexe
     *
     * @return binary
     */
    public function getSexe()
    {
        return $this->sexe;
    }

    /**
     * Set tel
     *
     * @param string $tel
     *
     * @return Agent
     */
    public function setTel($tel)
    {
        $this->tel = $tel;
    
        return $this;
    }

    /**
     * Get tel
     *
     * @return string
     */
    public function getTel()
    {
        return $this->tel;
    }

    /**
     * Set codeS
     *
     * @param string $codeS
     *
     * @return Agent
     */
    public function setCodeS($codeS)
    {
        $this->codeS = $codeS;
    
        return $this;
    }

    /**
     * Get codeS
     *
     * @return string
     */
    public function getCodeS()
    {
        return $this->codeS;
    }

    /**
     * Set email
     *
     * @param string $email
     *
     * @return Agent
     */
    public function setEmail($email)
    {
        $this->email = $email;
    
        return $this;
    }

    /**
     * Get email
     *
     * @return string
     */
    public function getEmail()
    {
        return $this->email;
    }

    /**
     * Set residence
     *
     * @param string $residence
     *
     * @return Agent
     */
    public function setResidence($residence)
    {
        $this->residence = $residence;
    
        return $this;
    }

    /**
     * Get residence
     *
     * @return string
     */
    public function getResidence()
    {
        return $this->residence;
    }

    /**
     * Set date
     *
     * @param \DateTime $date
     *
     * @return Agent
     */
    public function setDate($date)
    {
        $this->date = $date;
    
        return $this;
    }

    /**
     * Get date
     *
     * @return \DateTime
     */
    public function getDate()
    {
        return $this->date;
    }
   
    /**
     * Constructor
     */
    public function __construct()
    {
        $this->date = new \Datetime;
        $this->donnees = new \Doctrine\Common\Collections\ArrayCollection();
    }

    /**
     * Add donnee
     *
     * @param \cicrtg\BackendBundle\Entity\Donnee $donnee
     *
     * @return Agent
     */
    public function addDonnee(\cicrtg\BackendBundle\Entity\Donnee $donnee)
    {
        $this->donnees[] = $donnee;
        $donnee->setAgent($this);
    
        return $this;
    }

    /**
     * Remove donnee
     *
     * @param \cicrtg\BackendBundle\Entity\Donnee $donnee
     */
    public function removeDonnee(\cicrtg\BackendBundle\Entity\Donnee $donnee)
    {
        $this->donnees->removeElement($donnee);
    }

    /**
     * Get donnees
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getDonnees()
    {
        return $this->donnees;
    }

    /**
     * Add localite
     *
     * @param \cicrtg\BackendBundle\Entity\Localite $localite
     *
     * @return Agent
     */
    public function addLocalite(\cicrtg\BackendBundle\Entity\Localite $localite)
    {
        $this->localites[] = $localite;
        $localite->addAgent($this);
    
        return $this;
    }

    /**
     * Remove localite
     *
     * @param \cicrtg\BackendBundle\Entity\Localite $localite
     */
    public function removeLocalite(\cicrtg\BackendBundle\Entity\Localite $localite)
    {
        $this->localites->removeElement($localite);
        $localite->removeAgent($this);
    }

    /**
     * Get localites
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getLocalites()
    {
        return $this->localites;
    }
}

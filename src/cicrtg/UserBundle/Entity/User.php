<?php

namespace cicrtg\UserBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use FOS\UserBundle\Model\User as BaseUser;
use Symfony\Component\Validator\Constraints as Assert;
use Symfony\Bridge\Doctrine\Validator\Constraints as DoctrineAssert;
use Symfony\Bridge\Doctrine\Validator\Constraints\UniqueEntity;

/**
 * User
 *
 * @ORM\Table()
 * @ORM\Entity(repositoryClass="cicrtg\UserBundle\Entity\UserRepository")
 */
class User extends BaseUser
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    protected $id;
    
    /**
     * @var string
     *
     * @ORM\Column(name="nom", type="string", length=50)
     * @Assert\NotBlank
     * @Assert\Length(min=2)
     */
    protected $nom;

    /**
     * @var string
     *
     * @ORM\Column(name="prenom", type="string", length=70)
     * @Assert\NotBlank
     * @Assert\Length(min=2, minMessage="Cette valeur est trop courte")
     */
    protected $prenom;

    /**
     * @var binary
     *
     * @ORM\Column(name="sexe", type="string", length=1)
     */
    protected $sexe;

    /**
     * @var string
     *
     * @ORM\Column(name="tel", type="string", length=15)
     * @Assert\NotBlank
     */
    protected $tel;

}

